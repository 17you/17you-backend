const pgConnection = require("../../../config/database");

module.exports = {
  getStates: (req, res, next) => {
    pgConnection.query("SELECT * FROM tlstate", (error, results) => {

      if (error) {
        console.log(error)
        res.status(401).json({
          status: "false",
          message: "Error in retrieving states details",
        });
        return;
      }
      
      res.status(201).json({
        status: "true",
        message: "Successfully retrieved states details",
        states_results: results.rows
      });
    });
  },
};
