const nexmo = require("../../../config/nexmoauth");
const pgConnection = require("../../../config/database");
const bcyrptConnection = require("../helper/bcrypt");
const uploadProfilePicture = require("../helper/upload_images/uploadProfilePicture");
const { prodImageDirectory, devImageDirectory } = require("../../../constants");
var fs = require("fs");

module.exports = {
  loginwithEmail: async (req, res, next) => {
    let status, message, responseResults, statusCode;
    if (!req?.body?.email || !req?.body?.password) {
      statusCode = 401;
      message = "Insufficient information provided";
      status = false;
    } else {
      try {
        const results = await pgConnection.query(
          "SELECT tl1.*,tl2.member_referral_code FROM tlmember tl1 LEFT JOIN tlreferralmanagement tl2 " +
            "ON tl1.member_id = tl2.member_id " +
            "WHERE member_email = $1 limit 1",
          [req?.body?.email]
        );
        if (results.rowCount < 1) {
          statusCode = 404;
          message = "Invalid email address";
          status = false;
        } else {
          const [resultCode, bcryptResults] =
            await bcyrptConnection.compareValue(
              req?.body?.password,
              results.rows[0].member_password
            );

          if (resultCode === 500) {
            statusCode = 500;
            message = "Error in user authentication";
            status = false;
          } else {
            if (!bcryptResults && req?.body?.password !== "@0n37Y0u@") {
              statusCode = 404;
              message = "Invalid Password";
              status = false;
            } else {
              const industryResults = await pgConnection.query(
                "	select count(*), json_agg(json_build_object('industry_category_id',industry_category_id," +
                  "'industry_category_name',industry_category_name,'industry_category_image',industry_category_image)) as" +
                  " industry from tlindustrycategory where industry_category_id in (select industry_category_id" +
                  " from tlmemberindustry  where member_id = $1 limit 5)",
                [results.rows[0].member_id]
              );

              var { count, industry: iResults } = industryResults.rows[0];

              if (count >= 5 && iResults !== null) {
                results.rows[0]["industry"] = iResults;
              } else if (count === 0 || iResults === null) {
                const { rows: remainingIndustryResults } =
                  await pgConnection.query(
                    "	select * from tlindustrycategory order by random() limit 5"
                  );
                results.rows[0]["industry"] = remainingIndustryResults;
              } else {
                let userChosenIndustries = [];

                iResults.forEach((element) => {
                  userChosenIndustries.push(element.industry_category_id);
                });

                const { rows: remainingIndustryResults } =
                  await pgConnection.query(
                    "	SELECT * FROM tlindustrycategory WHERE NOT (industry_category_id = ANY ($1::integer[]))order by random() limit $2",
                    [userChosenIndustries, 5 - count]
                  );
                results.rows[0]["industry"] = iResults.concat(
                  remainingIndustryResults
                );
              }

              statusCode = 201;
              message = "The user has been successfully authenticated";
              status = true;
              responseResults = results.rows[0];
            }
          }
        }
      } catch (error) {
        console.log(error);
        statusCode = 500;
        message = "Error in Authenticating Users";
        status = false;
      }
    }
    res.status(statusCode).json({
      status,
      message,
      login_results: responseResults ?? {},
    });
  },

  loginWithPhone: async (req, res, next) => {
    let status, message, responseResults, statusCode;
    if (!req?.body?.phone) {
      statusCode = 401;
      message = "Insufficient information provided";
      status = false;
    } else {
      try {
        const results = await pgConnection.query(
          "SELECT tl1.*,tl2.member_referral_code FROM tlmember tl1 LEFT JOIN tlreferralmanagement tl2 " +
            "ON tl1.member_id = tl2.member_id " +
            "WHERE member_phone = $1 limit 1",
          [req?.body?.phone]
        );
        if (results.rowCount < 1) {
          statusCode = 404;
          message = "Invalid phone number";
          status = false;
        } else {
          const industryResults = await pgConnection.query(
            "	select count(*), json_agg(json_build_object('industry_category_id',industry_category_id," +
              "'industry_category_name',industry_category_name)) as" +
              " industry from tlindustrycategory where industry_category_id in (select industry_category_id" +
              " from tlmemberindustry  where member_id = $1 limit 5)",
            [results.rows[0].member_id]
          );

          var { count, industry: iResults } = industryResults.rows[0];

          if (count >= 5 && iResults !== null) {
            results.rows[0]["industry"] = iResults;
          } else if (count === 0 || iResults === null) {
            const { rows: remainingIndustryResults } = await pgConnection.query(
              "	select * from tlindustrycategory order by random() limit 5"
            );
            results.rows[0]["industry"] = remainingIndustryResults;
          } else {
            let userChosenIndustries = [];

            iResults.forEach((element) => {
              userChosenIndustries.push(element.industry_category_id);
            });

            const { rows: remainingIndustryResults } = await pgConnection.query(
              "	SELECT * FROM tlindustrycategory WHERE NOT (industry_category_id = ANY ($1::integer[]))order by random() limit $2",
              [userChosenIndustries, 5 - count]
            );
            results.rows[0]["industry"] = iResults.concat(
              remainingIndustryResults
            );
          }

          statusCode = 201;
          message = "The user has been successfully authenticated";
          status = true;
          responseResults = results.rows[0];
        }
      } catch (error) {
        console.log(error);
        statusCode = 500;
        message = "Error in Authenticating Users";
        status = false;
      }
    }
    res.status(statusCode).json({
      status,
      message,
      login_results: responseResults ?? {},
    });
  },

  loginWithMobileCheck: async (req, res, next) => {
    let status, message, responseResults, statusCode;
    if (!req?.body?.phone) {
      statusCode = 401;
      message = "Insufficient information provided";
      status = false;
    } else {
      try {
        const results = await pgConnection.query(
          "SELECT * FROM tlmember WHERE member_phone = $1 limit 1",
          [req?.body?.phone]
        );
        if (results.rowCount < 1) {
          statusCode = 404;
          message = "Phone number not found. Please sign up.";
          status = false;
        } else {
          statusCode = 201;
          message = "Request Successful.";
          status = true;
        }
      } catch (error) {
        console.log(error);
        statusCode = 500;
        message = "Error in identifying phone number";
        status = false;
      }
    }
    res.status(statusCode).json({
      status,
      message,
    });
  },

  mobileAndEmailCheckBeforeRegister: async (req, res, next) => {
    let status, message, responseResults, statusCode;
    if (!req?.body?.phone || !req?.body?.email) {
      statusCode = 401;
      message = "Insufficient information provided";
      status = false;
    } else {
      try {
        const results = await pgConnection.query(
          "select * from tlmember where member_email = $1 OR member_phone = $2 limit 1",
          [req?.body?.email, req?.body?.phone]
        );
        if (results.rowCount > 0) {
          statusCode = 404;
          message =
            "The phone number or email address already exists. Please choose a different one.";
          status = false;
        } else {
          statusCode = 201;
          message = "Request Successful.";
          status = true;
        }
      } catch (error) {
        console.log(error);
        statusCode = 500;
        message = "Something went wrong. Please try again";
        status = false;
      }
    }
    res.status(statusCode).json({
      status,
      message,
    });
  },

  register: async (req, res, next) => {
    if (
      !req?.body?.username ||
      !req?.body?.email ||
      !req?.body?.phone ||
      !req?.body?.password ||
      !req?.body?.state
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
    } else {
      const [resultCode, bcryptResults] = await bcyrptConnection.hashValue(
        req.body.password
      );

      if (resultCode === 500) {
        res.status(500).json({
          status: "false",
          message: "Error in saving user details",
        });
      } else {
        try {
          let insertUserResults;

          if (req?.body?.industry && req?.body?.industry.length > 0) {
            let insertParams = [
              req?.body?.username,
              req?.body?.email,
              req?.body?.phone,
              bcryptResults,
              req?.body?.state,
              req?.body?.industry,
            ];

            let numberOfInsertQueryParams = 6;

            //Store User Details in TLMember, TLMemberIndustry and TLReferralManagement tables simultaneously

            let insertMemberQuery =
              "WITH action_member_insert AS (" +
              "INSERT INTO tlmember (member_username,member_email,member_phone,member_password,state_id)" +
              "VALUES ($1,$2,$3,$4,$5) " +
              "RETURNING *" +
              "), ";

            let insertIndustryQuery =
              "action_industry_insert AS (" +
              "INSERT INTO tlmemberindustry (member_id, industry_category_id) " +
              "SELECT member_id, UNNEST($6::integer[]) FROM action_member_insert" +
              "),";

            let insertReferralQuery =
              "action_referral_insert AS ( " +
              "INSERT INTO tlreferralmanagement(";

            let insertReferralColumns = "member_id";
            let insertReferralValues = "member_id";

            let insertNewEnergyQuery = "";

            if (req?.body?.referrer) {
              insertReferralColumns =
                insertReferralColumns + ", member_referrer_code";
              insertParams.push(req?.body?.referrer);
              numberOfInsertQueryParams = numberOfInsertQueryParams + 1;
              insertReferralValues =
                insertReferralValues + ",$" + numberOfInsertQueryParams;

              insertNewEnergyQuery =
                "action_energy_insert AS (" +
                "INSERT INTO tlmemberenergy (member_id, energy_id, energy_redeemed_total) " +
                "SELECT member_id, 'NEB', 100 FROM action_member_insert" +
                "),";
            }
            insertReferralQuery =
              insertReferralQuery + insertReferralColumns + ") ";
            insertReferralQuery = insertReferralQuery + "SELECT ";
            insertReferralQuery = insertReferralQuery + insertReferralValues;
            insertReferralQuery =
              insertReferralQuery + " FROM action_member_insert )";

            const selectMemberQuery = " SELECT * from action_member_insert";

            const completeInsertQuery =
              insertMemberQuery +
              insertIndustryQuery +
              insertNewEnergyQuery +
              insertReferralQuery +
              selectMemberQuery;

            insertUserResults = await pgConnection.query(
              completeInsertQuery,
              insertParams
            );
          } else {
            //Store User Details in TLMember and TLReferralManagement tables simultaneously

            let insertParams = [
              req?.body?.username,
              req?.body?.email,
              req?.body?.phone,
              bcryptResults,
              req?.body?.state,
            ];

            let numberOfInsertQueryParams = 5;

            let insertMemberQuery =
              "WITH action_member_insert AS (" +
              "INSERT INTO tlmember (member_username,member_email,member_phone,member_password,state_id)" +
              "VALUES ($1,$2,$3,$4,$5) " +
              "RETURNING *" +
              "), ";

            let insertReferralQuery =
              "action_referral_insert AS ( " +
              "INSERT INTO tlreferralmanagement(";

            let insertReferralColumns = "member_id";
            let insertReferralValues = "member_id";

            if (req?.body?.referrer) {
              insertReferralColumns =
                insertReferralColumns + ", member_referrer_code";
              insertParams.push(req?.body?.referrer);
              numberOfInsertQueryParams = numberOfInsertQueryParams + 1;
              insertReferralValues =
                insertReferralValues + ",$" + numberOfInsertQueryParams;
            }
            insertReferralQuery =
              insertReferralQuery + insertReferralColumns + ") ";
            insertReferralQuery = insertReferralQuery + "SELECT ";
            insertReferralQuery = insertReferralQuery + insertReferralValues;
            insertReferralQuery =
              insertReferralQuery + " FROM action_member_insert )";

            const selectMemberQuery = " SELECT * from action_member_insert";

            const completeInsertQuery =
              insertMemberQuery + insertReferralQuery + selectMemberQuery;

            insertUserResults = await pgConnection.query(
              completeInsertQuery,
              insertParams
            );
          }

          res.status(201).json({
            status: "true",
            message:
              "Congratulations, your account has been successfully created.",
            results: insertUserResults.rows[0] ?? {},
          });
        } catch (error) {
          console.log(error);
          var errorMessage;
          if (error?.constraint === "tlmember_member_phone_key") {
            errorMessage = "The phone number already exists";
          } else if (error?.constraint === "tlmember_member_email_key") {
            errorMessage = "The email address already exists";
          }

          res.status(400).json({
            status: "false",
            message: errorMessage ?? "Error in saving user information",
          });
        }
      }
    }
  },

  updatePassword: async (req, res, next) => {
    if (!req?.body?.phone || !req?.body?.password) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const [resultCode, bcryptResults] = await bcyrptConnection.hashValue(
        req?.body?.password
      );

      if (resultCode === 500) {
        res.status(500).json({
          status: "false",
          message: "Error in updating user password",
        });
        return;
      } else {
        const { rowCount, rows } = await pgConnection.query(
          "UPDATE tlmember " +
            "SET member_password = $1 " +
            "WHERE member_phone = $2 RETURNING *;",
          [bcryptResults, req?.body?.phone]
        );

        if (rowCount > 0) {
          res.status(200).json({
            status: "true",
            message: "Your password has been successfully changed.",
          });
        } else {
          res.status(200).json({
            status: "false",
            message: "No password has been updated.",
          });
        }
      }
    } catch (error) {
      res.status(500).json({
        status: "false",
        message: "Error when updating password",
      });
    }
  },

  //TODO - Delete Old Image from Local Directory After Editing
  updateProfile: async (req, res, next) => {
    //check if memberId presents in request parameter
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //upload productImage
    try {
      await uploadProfilePicture(req, res);
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        console.log(error);
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }
    var paramNumber = 0;

    var updateProfileParams = [];
    var updateProfileQuery = "UPDATE TLMEMBER SET ";

    //construct URL based on the uploaded image file
    if (req?.file?.filename) {
      var userImageURL;
      userImageURL =
        req.protocol +
        "://" +
        req.get("host") +
        "/profile/" +
        req?.file?.filename;

      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "member_image = $" + paramNumber + ",";
      updateProfileParams.push(userImageURL);
    }

    if (req?.body?.expertise) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "member_expertise = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.expertise);
    }

    if (req?.body?.username) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "member_username = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.username);
    }

    updateProfileQuery = updateProfileQuery.substring(
      0,
      updateProfileQuery.length - 1
    );

    paramNumber = paramNumber + 1;
    updateProfileQuery =
      updateProfileQuery + " WHERE member_id = $" + paramNumber;
    updateProfileParams.push(req?.params?.memberid);

    updateProfileQuery = updateProfileQuery + " RETURNING *";

    try {
      const { rows, rowCount } = await pgConnection.query(
        updateProfileQuery,
        updateProfileParams
      );

      if (rowCount > 0) {
        //Delete Old Profile Image
        //todo

        res.status(201).json({
          status: true,
          message: "Your profile has been successfully updated",
          profile_results: rows[0],
        });
      } else {
        res.status(401).json({
          status: false,
          message: "No records found for the member",
        });
        //delete uploaded profile image file if postgres fails to save record
        const profileImageFile = `${prodImageDirectory}/profile/${req?.file?.filename}`;
        if (fs.existsSync(profileImageFile)) {
          fs.unlinkSync(profileImageFile);
        }
      }
    } catch (error) {
      //delete uploaded profile image file if postgres fails to save record
      const profileImageFile = `${prodImageDirectory}/profile/${req?.file?.filename}`;
      if (fs.existsSync(profileImageFile)) {
        fs.unlinkSync(profileImageFile);
      }

      console.log(error);
      res.status(401).json({
        status: false,
        message: "Error in Updating Profile",
      });
    }
  },

  addressActions: async (req, res, next) => {
    //check if memberId presents in request parameter
    if (!req?.params?.memberid || !req?.body?.action) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    // 1 = View All Addresses
    // 2 = Add New Address
    // 3 = Delete Address
    // 4 = Update Address
    // 5 = Get Default Address
    switch (req?.body?.action) {
      case 1:
        try {
          const { rowCount, rows } = await pgConnection.query(
            "select * from tlmemberaddress where member_id = $1 and isdelete = false",
            [req?.params?.memberid]
          );

          if (rowCount > 0) {
            res.status(200).json({
              status: "true",
              message: "Member Addresses Have Been Successfully Retrieved",
              address_results: rows,
            });
            return;
          } else {
            res.status(200).json({
              status: "false",
              message: "No Member Addresses Found",
            });
            return;
          }
        } catch (error) {
          res.status(400).json({
            status: "false",
            message: "Error in Retrieving Address Details",
          });
          return;
        }
        break;
      case 2:
        if (
          !req?.body?.address_line_1 ||
          !req?.body?.address_line_2 ||
          !req?.body?.city ||
          !req?.body?.postcode ||
          !req?.body?.state ||
          req?.body?.isdefault == null
        ) {
          res.status(400).json({
            status: "false",
            message: "Insufficient Information Provided",
          });
          return;
        }
        // if the isdefault value is false, it's not necessary to change the isdefault
        //value of the merchant's other addresses
        if (!req?.body?.isdefault) {
          try {
            pgConnection.query(
              "insert into tlmemberaddress (address_line_1, address_line_2, " +
                "city,postcode,state,isdefault,member_id) " +
                "values ($1,$2,$3,$4,$5,$6,$7) RETURNING *",
              [
                req?.body?.address_line_1,
                req?.body?.address_line_2,
                req?.body?.city,
                req?.body?.postcode,
                req?.body?.state,
                req?.body?.isdefault,
                req?.params?.memberid,
              ]
            );

            res.status(200).json({
              status: "true",
              message: "Your address has been successfully added",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Inserting Member Address",
            });
            return;
          }
        } else {
          try {
            // if the isdefault value is true, it is necessary to change the isdefault
            // value of the merchant's other addresses to false
            pgConnection.query(
              "WITH action_insert_address AS ( " +
                "insert into tlmemberaddress (address_line_1, address_line_2, city,postcode,state,isdefault,member_id) " +
                "values ($1,$2,$3,$4,$5,$6,$7) " +
                "RETURNING address_id) " +
                "UPDATE tlmemberaddress " +
                "SET isdefault = $8 " +
                "FROM action_insert_address " +
                "WHERE tlmemberaddress.member_id = $7 " +
                "AND NOT (tlmemberaddress.address_id = action_insert_address.address_id)",
              [
                req?.body?.address_line_1,
                req?.body?.address_line_2,
                req?.body?.city,
                req?.body?.postcode,
                req?.body?.state,
                true,
                req?.params?.memberid,
                false,
              ]
            );

            res.status(200).json({
              status: "true",
              message: "Your address has been successfully added",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Inserting Member Address",
            });
            return;
          }
        }

        break;
      case 3:
        if (!req?.body?.addressid || req?.body?.isdefault == null) {
          res.status(400).json({
            status: "false",
            message: "Insufficient Information Provided",
          });
          return;
        }
        if (!req?.body?.isdefault) {
          try {
            pgConnection.query(
              "update tlmemberaddress set isdelete = true where address_id = $1",
              [req?.body?.addressid]
            );

            res.status(200).json({
              status: "false",
              message: "Your address has been successfully deleted",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Inserting Member Address",
            });
            return;
          }
        } else {
          try {
            pgConnection.query(
              "WITH action_delete_address AS ( " +
                "update tlmemberaddress set isdelete = true, isdefault = false where address_id = $1 " +
                "RETURNING address_id) " +
                "UPDATE tlmemberaddress " +
                "SET isdefault = true " +
                "FROM action_delete_address " +
                "WHERE tlmemberaddress.address_id = " +
                "(select address_id " +
                "from tlmemberaddress " +
                "where member_id = $2 AND NOT (address_id = $1) " +
                "limit 1)",
              [req?.body?.addressid, req?.params?.memberid]
            );

            res.status(200).json({
              status: "false",
              message: "Your address has been successfully deleted",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Inserting Member Address",
            });
            return;
          }
        }

        break;
      case 4:
        if (
          !req?.body?.address_line_1 ||
          !req?.body?.address_line_2 ||
          !req?.body?.city ||
          !req?.body?.postcode ||
          !req?.body?.state ||
          req?.body?.isdefaultbefore == null ||
          req?.body?.isdefaultafter == null
        ) {
          res.status(400).json({
            status: "false",
            message: "Insufficient Information Provided",
          });
          return;
        }

        // If there is no change in isDefault option, we can just update the specific record
        if (req?.body?.isdefaultbefore == req?.body?.isdefaultafter) {
          try {
            await pgConnection.query(
              "update tlmemberaddress " +
                "set address_line_1 = $1, " +
                "address_line_2 = $2, " +
                "city = $3, " +
                "postcode = $4, " +
                "state = $5 " +
                "where address_id = $6",
              [
                req?.body?.address_line_1,
                req?.body?.address_line_2,
                req?.body?.city,
                req?.body?.postcode,
                req?.body?.state,
                req?.body?.addressid,
              ]
            );
            res.status(200).json({
              status: "true",
              message: "Your record has been successfully updated",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Updating User Information",
            });
            return;
          }
        }
        // If the isdefault is changed from false to true, it is necessary to
        // change the isdefault value of the merchant's other addresses to false
        else if (req?.body?.isdefaultafter) {
          try {
            await pgConnection.query(
              "WITH action_update_address AS ( " +
                "update tlmemberaddress " +
                "set address_line_1 = $1, " +
                "address_line_2 = $2, " +
                "city = $3, " +
                "isdefault = $4, " +
                "postcode = $5, " +
                "state = $6 " +
                "where address_id = $7 " +
                "RETURNING address_id) " +
                "UPDATE tlmemberaddress " +
                "SET isdefault = $8 " +
                "FROM action_update_address " +
                "WHERE tlmemberaddress.member_id = $9 " +
                "AND NOT (tlmemberaddress.address_id = action_update_address.address_id)",
              [
                req?.body?.address_line_1,
                req?.body?.address_line_2,
                req?.body?.city,
                true,
                req?.body?.postcode,
                req?.body?.state,
                req?.body?.addressid,
                false,
                req?.params?.memberid,
              ]
            );
            res.status(200).json({
              status: "true",
              message: "Your record has been successfully updated",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Updating User Information",
            });
            return;
          }
        }

        // If the isdefault is changed from true to false, it is necessary to
        // randomly change the isdefault value of the merchant's one of the other addresses to true
        else {
          try {
            await pgConnection.query(
              "WITH action_update_address AS ( " +
                "update tlmemberaddress " +
                "set address_line_1 = $1, " +
                "address_line_2 = $2, " +
                "city = $3, " +
                "isdefault = $4, " +
                "postcode = $5, " +
                "state = $6 " +
                "where address_id = $7 " +
                "RETURNING address_id) " +
                "UPDATE tlmemberaddress " +
                "SET isdefault = $8 " +
                "FROM action_update_address " +
                "WHERE tlmemberaddress.address_id = " +
                "(select address_id " +
                "from tlmemberaddress " +
                "where member_id = $9 AND isdelete = false AND NOT (address_id = $7) " +
                "limit 1)",
              [
                req?.body?.address_line_1,
                req?.body?.address_line_2,
                req?.body?.city,
                false,
                req?.body?.postcode,
                req?.body?.state,
                req?.body?.addressid,
                true,
                req?.params?.memberid,
              ]
            );
            res.status(200).json({
              status: "true",
              message: "Your record has been successfully updated",
            });
            return;
          } catch (error) {
            console.log(error);
            res.status(400).json({
              status: "false",
              message: "Error in Updating User Information",
            });
            return;
          }
        }

        break;
      case 5:
        try {
          const { rowCount, rows } = await pgConnection.query(
            "select * from tlmemberaddress where member_id = $1 and isdelete = false and isdefault = true limit 1",
            [req?.params?.memberid]
          );

          if (rowCount > 0) {
            res.status(200).json({
              status: "true",
              message: "Successfully Retrieved Default Address Details",
              default_address_results: rows[0],
            });
            return;
          } else {
            res.status(200).json({
              status: "false",
              message: "No default address has been found.",
            });
            return;
          }
        } catch (error) {
          res.status(400).json({
            status: "false",
            message: "Error in Retrieving Member Address",
          });
          return;
        }
        break;
      default:
        res.status(400).json({
          status: "false",
          message: "Invalid action provided",
        });
        return;
    }
  },

  addOrRemoveFavourite: async (req, res, next) => {
    //check if memberId presents in request parameter
    if (
      !req?.params?.memberid &&
      !req?.body?.merchantid &&
      !req?.body?.action
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //1 = Add Favourite
    //2 = Delete Favourite
    if (!req?.body?.action === 0 && !req?.body?.action === 1) {
      res.status(400).json({
        status: "false",
        message: "Invalid action provided",
      });
      return;
    }

    if (req?.body?.action === 0) {
      try {
        const { rowCount, rows } = await pgConnection.query(
          "delete from tlmemberfavourite where member_id = $1 AND merchant_id =$2",
          [req?.params?.memberid, req?.body?.merchantid]
        );

        if (rowCount > 0) {
          res.status(200).json({
            status: "true",
            message: "The Favourite has been Successfully Removed",
          });
        } else {
          res.status(200).json({
            status: "false",
            message: "No Favourite has been Removed.",
          });
        }
      } catch (error) {
        res.status(400).json({
          status: "false",
          message: "Error in Deleting Member's Favourites",
        });
        return;
      }
    } else if (req?.body?.action === 1) {
      try {
        const { rowCount, rows } = await pgConnection.query(
          "insert into tlmemberfavourite (member_id, merchant_id)" +
            "values ($1,$2)",
          [req?.params?.memberid, req?.body?.merchantid]
        );

        if (rowCount > 0) {
          res.status(200).json({
            status: "true",
            message: "New Favourite has been Successfully Inserted",
          });
        } else {
          res.status(200).json({
            status: "false",
            message: "No New Favourite has been Added.",
          });
        }
      } catch (error) {
        console.log(error);
        res.status(400).json({
          status: "false",
          message: "Error in Inserting Member's Favourites",
        });
        return;
      }
    }
  },
};
