const { removeAllListeners } = require("../../../config/database");
const pgConnection = require("../../../config/database");

module.exports = {
  addMemberShared: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    try {
      const { rowCount } = await pgConnection.query(
        "INSERT INTO TLMemberShared (member_id) " + "VALUES " + "($1)",
        [req?.params?.memberid]
      );

      if (rowCount > 0) {
        res.status(201).json({
          status: "true",
          message: "Record has been successfully added",
        });
      } else {
        res.status(201).json({
          status: "false",
          message: "No records have been added",
        });
      }
    } catch (error) {
      res.status(401).json({
        status: "false",
        message: "Error in adding records.",
      });
    }
  },

  getReferralCounts: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount: referralCount, rows: referralCountResults } =
        await pgConnection.query(
          "select (select count(*)::int  " +
          "as total_share from tlmembershared  " +
          " where member_id = $1), " +
          "(select count(*)::int as  " +
          " total_sign_up from tlreferralmanagement  " +
          " where member_referrer_code =(select member_referral_code from  " +
          " tlreferralmanagement where member_id = $1)), " +
          " (select coalesce(sum(energy_redeemed_total),0)  " +
          " as total_energy from tlmemberenergy  " +
          " where member_id = $1) :: int,  " +
          " (select count(*)::int as " +
          " today_sign_up from tlreferralmanagement  " +
          " where member_referrer_code = (select member_referral_code from  " +
          " tlreferralmanagement where member_id = $1)  " +
          " AND member_signed_up_date_and_time :: date = CURRENT_DATE)",
          [req?.params?.memberid]
        );

      if (referralCount > 0) {
        const { rows: energyLevelResults } = await pgConnection.query(
          "select * from tlenergylevel"
        );

        res.status(201).json({
          status: "true",
          message: "Referral Records have been successfully retrieved",
          referral_count_results: referralCountResults[0],
          energy_level_results: energyLevelResults,
        });
      } else {
        res.status(201).json({
          status: "false",
          message: "No referral counts are found the user",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }
  },

  getTotalReferralCountBasedOnStates: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {

      // Get Total Sign Up based on States
     const {rows : totalSignUpBasedOnStates} =  await pgConnection.query(
     "select tl1.*, COUNT(tl3.*) :: int from tlstate tl1 "+
     "left join tlmember tl2 "+
     "on tl2.state_id = tl1.state_id "+
     "left join tlreferralmanagement tl3 "+
     "on tl2.member_id = tl3.member_id "+
     "AND tl3.member_referrer_code = (select member_referral_code "+ 
     "from tlreferralmanagement "+ 
     "where member_id = $1 limit 1)"+
     "group by tl1.state_id",
     [req?.params?.memberid ]);

     //Get Total Sign Up and Total Share
     const {rows : totalSignUpAndShare} =  await pgConnection.query(
      "select  (select COUNT (tl3.*) :: int from tlmembershared tl3 "+
      "where "+
      "tl3.member_id = $1) AS total_share,  COUNT(tl1.*) :: int AS total_sign_up from tlreferralmanagement tl1 "+
      "where "+
      "tl1.member_referrer_code = "+
      "(select tl2.member_referral_code from tlreferralmanagement tl2 where tl2.member_id =$1 limit 1) ",
      [req?.params?.memberid ]
     )

     if (totalSignUpAndShare && totalSignUpAndShare?.length > 0 && totalSignUpBasedOnStates && totalSignUpBasedOnStates?.length >0) {
      let overallReferralResults = {};

      overallReferralResults['total_sign_up'] = totalSignUpAndShare[0].total_sign_up;
      overallReferralResults['total_share'] = totalSignUpAndShare[0].total_share;
      overallReferralResults['total_sign_up_states'] = totalSignUpBasedOnStates;
    
 
      res.status(201).json({
       status: "true",
       message: "Referral Records have been successfully retrieved",
       overall_referral_results: overallReferralResults
     });

     }
     else{
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });

     }
      
    } 
    

    catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }
  },

  getTodayReferralCountBasedOnStates: async (req,res,next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {

      // Get Total Sign Up based on States
     const {rows : totalSignUpBasedOnStates} =  await pgConnection.query(
     "select tl1.*, COUNT(tl3.*) :: int from tlstate tl1 "+
     "left join tlmember tl2 "+
     "on tl2.state_id = tl1.state_id "+
     "left join tlreferralmanagement tl3 "+
     "on tl2.member_id = tl3.member_id "+
     "AND tl3.member_referrer_code = (select tl4.member_referral_code "+ 
     "from tlreferralmanagement tl4 "+ 
     "where tl4.member_id = $1 limit 1) "+
     "AND tl3.member_signed_up_date_and_time :: date = CURRENT_DATE "+
     "group by tl1.state_id",
     [req?.params?.memberid ]);

     //Get Total Sign Up and Total Share
     const {rows : totalSignUpAndShare} =  await pgConnection.query(
      "select  (select COUNT (tl3.*) :: int from tlmembershared tl3 "+
      "where "+
      "tl3.member_id = $1 AND tl3.member_shared_date = CURRENT_DATE) "+
      "AS total_share,  COUNT(tl1.*) :: int AS total_sign_up from tlreferralmanagement tl1 "+
      "where "+
      "tl1.member_referrer_code = "+
      "(select tl2.member_referral_code from tlreferralmanagement tl2 where tl2.member_id =$1 limit 1) AND "+
      "tl1.member_signed_up_date_and_time :: DATE = CURRENT_DATE",
      [req?.params?.memberid ]
     )

     if (totalSignUpAndShare && totalSignUpAndShare?.length > 0 && totalSignUpBasedOnStates && totalSignUpBasedOnStates?.length >0) {
      let overallReferralResults = {};

      overallReferralResults['total_sign_up'] = totalSignUpAndShare[0].total_sign_up;
      overallReferralResults['total_share'] = totalSignUpAndShare[0].total_share;
      overallReferralResults['total_sign_up_states'] = totalSignUpBasedOnStates;
    
 
      res.status(201).json({
       status: "true",
       message: "Referral Records have been successfully retrieved",
       overall_referral_results: overallReferralResults
     });

     }
     else{
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });

     }
      
    } 
    

    catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }
  },

  getCurrentWeekReferralCountBasedOnStates: async (req,res,next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {

      // Get Total Sign Up based on States
     const {rows : totalSignUpBasedOnStates} =  await pgConnection.query(
     "select tl1.*, COUNT(tl3.*) :: int from tlstate tl1 "+
     "left join tlmember tl2 "+
     "on tl2.state_id = tl1.state_id "+
     "left join tlreferralmanagement tl3 "+
     "on tl2.member_id = tl3.member_id "+
     "AND tl3.member_referrer_code = (select tl4.member_referral_code "+ 
     "from tlreferralmanagement tl4 "+ 
     "where tl4.member_id = $1 limit 1) "+
     "AND date_trunc('week',tl3.member_signed_up_date_and_time :: date) = date_trunc('week',CURRENT_DATE) "+
     "group by tl1.state_id",
     [req?.params?.memberid ]);

     console.log("Entering");
     //Get Total Sign Up and Total Share
     const {rows : totalSignUpAndShare} =  await pgConnection.query(
      "select  (select COUNT (tl3.*) :: int from tlmembershared tl3 "+
      "where "+
      "tl3.member_id = $1 AND   date_trunc('week',tl3.member_shared_date) = date_trunc('week',CURRENT_DATE)) "+
      "AS total_share,  COUNT(tl1.*) :: int AS total_sign_up from tlreferralmanagement tl1 "+
      "where "+
      "tl1.member_referrer_code = "+
      "(select tl2.member_referral_code from tlreferralmanagement tl2 where tl2.member_id =$1 limit 1) AND "+
      "date_trunc('week',tl1.member_signed_up_date_and_time :: date) = date_trunc('week',CURRENT_DATE)",
      [req?.params?.memberid ]
     )

     if (totalSignUpAndShare && totalSignUpAndShare?.length > 0 && totalSignUpBasedOnStates && totalSignUpBasedOnStates?.length >0) {
      let overallReferralResults = {};

      overallReferralResults['total_sign_up'] = totalSignUpAndShare[0].total_sign_up;
      overallReferralResults['total_share'] = totalSignUpAndShare[0].total_share;
      overallReferralResults['total_sign_up_states'] = totalSignUpBasedOnStates;
    
 
      res.status(201).json({
       status: "true",
       message: "Referral Records have been successfully retrieved",
       overall_referral_results: overallReferralResults
     });

     }
     else{
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });

     }
      
    } 
    

    catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }

  },
  
  getCurrentMonthReferralCountBasedOnStates: async (req,res,next) => {

    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {

      // Get Total Sign Up based on States
     const {rows : totalSignUpBasedOnStates} =  await pgConnection.query(
     "select tl1.*, COUNT(tl3.*) :: int from tlstate tl1 "+
     "left join tlmember tl2 "+
     "on tl2.state_id = tl1.state_id "+
     "left join tlreferralmanagement tl3 "+
     "on tl2.member_id = tl3.member_id "+
     "AND tl3.member_referrer_code = (select tl4.member_referral_code "+ 
     "from tlreferralmanagement tl4 "+ 
     "where tl4.member_id = $1 limit 1) "+
     "AND date_trunc('month',tl3.member_signed_up_date_and_time :: date) = date_trunc('month',CURRENT_DATE) "+
     "group by tl1.state_id",
     [req?.params?.memberid ]);

     //Get Total Sign Up and Total Share
     const {rows : totalSignUpAndShare} =  await pgConnection.query(
      "select  (select COUNT (tl3.*) :: int from tlmembershared tl3 "+
      "where "+
      "tl3.member_id = $1 AND   date_trunc('month',tl3.member_shared_date) = date_trunc('month',CURRENT_DATE)) "+
      "AS total_share,  COUNT(tl1.*) :: int AS total_sign_up from tlreferralmanagement tl1 "+
      "where "+
      "tl1.member_referrer_code = "+
      "(select tl2.member_referral_code from tlreferralmanagement tl2 where tl2.member_id =$1 limit 1) AND "+
      "date_trunc('month',tl1.member_signed_up_date_and_time :: date) = date_trunc('month',CURRENT_DATE)",
      [req?.params?.memberid ]
     )

     if (totalSignUpAndShare && totalSignUpAndShare?.length > 0 && totalSignUpBasedOnStates && totalSignUpBasedOnStates?.length >0) {
      let overallReferralResults = {};

      overallReferralResults['total_sign_up'] = totalSignUpAndShare[0].total_sign_up;
      overallReferralResults['total_share'] = totalSignUpAndShare[0].total_share;
      overallReferralResults['total_sign_up_states'] = totalSignUpBasedOnStates;
    
 
      res.status(201).json({
       status: "true",
       message: "Referral Records have been successfully retrieved",
       overall_referral_results: overallReferralResults
     });

     }
     else{
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });

     }
      
    } 
    

    catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }
  },

  getTotalReferralEmailAddress:async(req,res,next)=>{

    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try{
      const{rows:referrerEmailResults} =  await pgConnection.query(
        "select mask_email(tl2.member_email),tl1.member_signed_up_date_and_time, "+
        "(select count(*) from tlreferralmanagement tl3 "+
        "where tl3.member_referrer_code = tl1.member_referral_code) ::int "+
        "as total_referred, 150 as total_booster "+
        "from tlreferralmanagement tl1 "+
        "inner join tlmember tl2 "+
        "on tl2.member_id = tl1.member_id "+
        "where member_referrer_code = "+
        "(select tl3.member_referral_code from tlreferralmanagement tl3 "+
        "where tl3.member_id = $1)",[req?.params?.memberid])
    

    if(referrerEmailResults && referrerEmailResults.length>0){
      res.status(201).json({
        status: "true",
        message: "Referral Records have been successfully retrieved",
        referral_email_results: referrerEmailResults
      });
    }
    else
    {
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });
    }
  }
    catch(error){
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });

    }

  },

  getTodayReferralEmailAddress:async(req,res,next)=>{

    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try{
      const{rows:referrerEmailResults} =  await pgConnection.query(
        "select mask_email(tl2.member_email),tl1.member_signed_up_date_and_time, "+
        "(select count(*) from tlreferralmanagement tl3 "+
        "where tl3.member_referrer_code = tl1.member_referral_code) ::int "+
        "as total_referred, 150 as total_booster "+
        "from tlreferralmanagement tl1 "+
        "inner join tlmember tl2 "+
        "on tl2.member_id = tl1.member_id "+
        "where member_referrer_code = "+
        "(select tl3.member_referral_code from tlreferralmanagement tl3 "+
        "where tl3.member_id = $1) "+
        "AND tl1.member_signed_up_date_and_time :: date = CURRENT_DATE",[req?.params?.memberid])
    

    if(referrerEmailResults && referrerEmailResults.length>0){
      res.status(201).json({
        status: "true",
        message: "Referral Records have been successfully retrieved",
        referral_email_results: referrerEmailResults
      });
    }
    else
    {
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });
    }
  }
    catch(error){
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });

    }

  },

  getCurrentMonthReferralEmailAddress:async(req,res,next)=>{

    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try{
      const{rows:referrerEmailResults} =  await pgConnection.query(
        "select mask_email(tl2.member_email),tl1.member_signed_up_date_and_time, "+
        "(select count(*) from tlreferralmanagement tl3 "+
        "where tl3.member_referrer_code = tl1.member_referral_code) ::int "+
        "as total_referred, 150 as total_booster "+
        "from tlreferralmanagement tl1 "+
        "inner join tlmember tl2 "+
        "on tl2.member_id = tl1.member_id "+
        "where member_referrer_code = "+
        "(select tl3.member_referral_code from tlreferralmanagement tl3 "+
        "where tl3.member_id = $1) "+
        "AND date_trunc('month',tl1.member_signed_up_date_and_time :: date) = date_trunc('month',CURRENT_DATE)",[req?.params?.memberid])
    

    if(referrerEmailResults && referrerEmailResults.length>0){
      res.status(201).json({
        status: "true",
        message: "Referral Records have been successfully retrieved",
        referral_email_results: referrerEmailResults
      });
    }
    else
    {
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });
    }
  }
    catch(error){
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });

    }

  },
  
  getCurrentWeekReferralEmailAddress:async(req,res,next)=>{
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try{
      const{rows:referrerEmailResults} =  await pgConnection.query(
        "select mask_email(tl2.member_email),tl1.member_signed_up_date_and_time, "+
        "(select count(*) from tlreferralmanagement tl3 "+
        "where tl3.member_referrer_code = tl1.member_referral_code) ::int "+
        "as total_referred, 150 as total_booster "+
        "from tlreferralmanagement tl1 "+
        "inner join tlmember tl2 "+
        "on tl2.member_id = tl1.member_id "+
        "where member_referrer_code = "+
        "(select tl3.member_referral_code from tlreferralmanagement tl3 "+
        "where tl3.member_id = $1) "+
        "AND date_trunc('week',tl1.member_signed_up_date_and_time :: date) = date_trunc('week',CURRENT_DATE)",[req?.params?.memberid])
    

    if(referrerEmailResults && referrerEmailResults.length>0){
      res.status(201).json({
        status: "true",
        message: "Referral Records have been successfully retrieved",
        referral_email_results: referrerEmailResults
      });
    }
    else
    {
      res.status(201).json({
        status: "false",
        message: "No referral records have been found",
      });
    }
  }
    catch(error){
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving referral information.",
      });
    }
  },
};
