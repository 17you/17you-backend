const pgConnection = require("../../../config/database");
const moment = require("moment");

module.exports = {
  getRankingDetails: async (req, res, next) => {
    //Get Default Ranking Images
    try {
      const { rows: defaultRankingImages } = await pgConnection.query(
        "select ranking_image from tlrankinginfo limit 10"
      );

      //Get Daily Ranking Records
      const { rows: dailyWithoutImage } = await pgConnection.query(
        "select tl1.member_id, tl2.member_username, " +
          "(select count(*) from tlreferralmanagement tl3  " +
          " where tl3.member_referrer_code = tl1.member_referral_code " +
          "and tl3.member_signed_up_date_and_time :: date = current_date " +
          ") :: int as total_sign_up " +
          "from tlreferralmanagement tl1 " +
          "inner join tlmember tl2 " +
          "on tl2.member_id = tl1.member_id " +
          "order by total_sign_up DESC, tl1.member_id ASC " +
          "limit 10"
      );

      //Get Weekly Ranking Records
      const { rows: weeklyWithoutImage } = await pgConnection.query(
        "select tl1.member_id, tl2.member_username, " +
          "(select count(*) from tlreferralmanagement tl3  " +
          " where tl3.member_referrer_code = tl1.member_referral_code " +
          " AND date_trunc('week',tl3.member_signed_up_date_and_time :: date) = date_trunc('week',CURRENT_DATE) " +
          ") :: int as total_sign_up " +
          "from tlreferralmanagement tl1 " +
          "inner join tlmember tl2 " +
          "on tl2.member_id = tl1.member_id " +
          "order by total_sign_up DESC, tl1.member_id ASC " +
          "limit 10"
      );

      //Get Monthly Ranking Records
      const { rows: monthlyWithoutImage } = await pgConnection.query(
        "select tl1.member_id, tl2.member_username, " +
          "(select count(*) from tlreferralmanagement tl3  " +
          " where tl3.member_referrer_code = tl1.member_referral_code " +
          "and date_trunc('month',tl3.member_signed_up_date_and_time :: date) = date_trunc('month',CURRENT_DATE) " +
          ") :: int as total_sign_up " +
          "from tlreferralmanagement tl1 " +
          "inner join tlmember tl2 " +
          "on tl2.member_id = tl1.member_id " +
          "order by total_sign_up DESC, tl1.member_id ASC " +
          "limit 10"
      );

      // Map the default image with the fetched ranking records (weekly,daily,monthly)
      const daily = dailyWithoutImage.map((item, index) => {
        item["default_ranking_image"] =
          defaultRankingImages[index].ranking_image;
        return item;
      });

      const weekly = weeklyWithoutImage.map((item, index) => {
        item["default_ranking_image"] =
          defaultRankingImages[index].ranking_image;
        return item;
      });

      const monthly = monthlyWithoutImage.map((item, index) => {
        item["default_ranking_image"] =
          defaultRankingImages[index].ranking_image;
        return item;
      });

      // Separate top Three Records from other Records

      const topDailyRecords = daily.splice(0, 3);
      const otherDailyRecords = daily;

      const topWeeklyRecords = weekly.splice(0, 3);
      const otherWeeklyRecords = weekly;

      const topMonthlyRecords = monthly.splice(0, 3);
      const otherMonthlyRecords = monthly;

      res.status(200).json({
        status: true,
        message: "Ranking Details have been successfully retrieved.",

        ranking_results: {
          daily: {
            top: topDailyRecords,
            others: otherDailyRecords,
          },
          weekly: {
            top: topWeeklyRecords,
            others: otherWeeklyRecords,
          },
          monthly: {
            top: topMonthlyRecords,
            others: otherMonthlyRecords,
          },
        },
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        status: false,
        message: "Error in retrieving weekly information.",
      });
    }
  },
  checkRewards: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    if (moment().local().format("dddd") !== "Tuesday") {
      res.status(201).json({
        status: false,
        isredeemable: false,
        message: "You can only redeem your rewards on Sunday.",
      });
      return;
    }

    try {
      //Get Ranking Records (for the Month)
      const { rows: monthlyRecords, rowCount: monthlyRecordsCount } =
        await pgConnection.query(
          "select tl1.member_id, tl2.member_username, " +
            "(select count(*) from tlreferralmanagement tl3  " +
            " where tl3.member_referrer_code = tl1.member_referral_code " +
            "and date_trunc('month',tl3.member_signed_up_date_and_time :: date) = date_trunc('month',CURRENT_DATE) " +
            ") :: int as total_sign_up " +
            "from tlreferralmanagement tl1 " +
            "inner join tlmember tl2 " +
            "on tl2.member_id = tl1.member_id " +
            "order by total_sign_up DESC, tl1.member_id ASC " +
            "limit 10"
        );

      if (monthlyRecordsCount <= 0) {
        res.status(201).json({
          status: false,
          isredeemable: false,
          message: "No records found",
        });
        return;
      }
      var isWon = false;
      var rank = "";
      monthlyRecords.forEach((element, index) => {
        if (element.member_id.toString() === req?.params?.memberid.toString()) {
          rank = index + 1;
          isWon = true;
        }
      });

      if (isWon === false) {
        res.status(201).json({
          status: false,
          isredeemable: false,
          message:
            "Sorry, you are not qualified for the reward. Please try again next week.",
        });
        return;
      }

      //Check if user has already claimed the points for the current week reward
      const { rowCount: alreadyAvailablePointRowCount } =
        await pgConnection.query(
          "SELECT * FROM TLMEMBERENERGY WHERE ENERGY_ID = $1 AND MEMBER_ID = $2 AND ENERGY_REDEEMED_DATE = CURRENT_DATE",
          ["REB", req?.params?.memberid]
        );

      if (alreadyAvailablePointRowCount > 0) {
        res.status(201).json({
          status: false,
          isredeemable: false,
          message:
            "Sorry, you have already claimed the points for the week. Please try again next week. ",
        });
        return;
      }

      const { rows: rewardPoints, rowCount: rewardPointsRowCount } =
        await pgConnection.query(
          "select reward_points from tlrankinginfo where ranking_id = $1 limit 1",
          [rank]
        );
      if (rewardPointsRowCount <= 0) {
        throw "Not able to retrieve the reward points";
      }

      res.status(201).json({
        status: true,
        rank: rank,
        reward_points: rewardPoints[0]?.reward_points,
        isredeemable: true,
        message: `Congrats, you have maintained no ${rank} position for this week.`,
      });
      return;
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: false,
        message: "Something went wrong. Please try again",
      });
    }
  },

  claimRewards: async (req, res, next) => {
    if (!req?.params?.memberid || !req?.body?.points || !req?.body?.rank) {
      res.status(401).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Check if user has already claimed the points for the current week reward
      const { rowCount: alreadyAvailablePointRowCount } =
        await pgConnection.query(
          "SELECT * FROM TLMEMBERENERGY WHERE ENERGY_ID = $1 AND MEMBER_ID = $2 AND ENERGY_REDEEMED_DATE = CURRENT_DATE",
          ["REB", req?.params?.memberid]
        );

      if (alreadyAvailablePointRowCount > 0) {
        res.status(201).json({
          status: true,
          message:
            "Sorry, you have already claimed the points for the week. Please try again next week. ",
        });
        return;
      }

      //update the claimed points in db
      const { rowCount: insertedRowCount } = await pgConnection.query(
        "INSERT INTO TLMEMBERENERGY(MEMBER_ID, ENERGY_ID,ENERGY_REDEEMED_TOTAL,MEMBER_RANK) " +
          "VALUES ($1,$2,$3,$4)",
        [req?.params?.memberid, "REB", req?.body?.points, req?.body?.rank]
      );

      if (insertedRowCount <= 0) {
        res.status(500).json({
          status: false,
          message: "No records have been updated",
        });
        return;
      }

      res.status(201).json({
        status: true,
        message:
          "Congrats. You have successfully claimed the points for your rank.",
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: false,
        message: "Something went wrong. Please try again",
      });
    }
  },

  getRewardDetails: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        `
      SELECT DATE_TRUNC('month', energy_redeemed_date) :: TEXT as month_and_year,
      JSON_AGG(JSON_BUILD_OBJECT('date',t.energy_redeemed_date :: TEXT,'rank',t.member_rank)) AS rank_details
      FROM tlmemberenergy t
      WHERE member_id = $1 AND energy_id = $2
      GROUP BY DATE_TRUNC('month', energy_redeemed_date);`,
        [req?.params?.memberid, "REB"]
      );

      if (rowCount <= 0) {
        res.status(200).json({
          status: false,
          message: "No records found",
        });
        return;
      }

      res.status(200).json({
        status: true,
        message: "Rank Details have been successfully retrieved.",
        rank_results: rows,
      });
      return;
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: false,
        message: "Something went wrong. Please try again",
      });
    }
  },
};
