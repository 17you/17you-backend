const pgConnection = require("../../../config/database");
const { devImageDirectory, prodImageDirectory } = require("../../../constants");
const uploadTransactionReceipt = require("../helper/upload_images/uploadTransactionReceipt.js");
const uploadTransactionReceiptUpdate = require("../helper/upload_images/uploadTransactionReceiptUpdate.js");

var fs = require("fs");

module.exports = {
  //Merchant Side
  retrieveMerchantTransactionDetails: async (req, res, next) => {
    if (!req?.params?.merchantid || !req?.query?.status) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      var transactionStatus = [];

      switch (parseInt(req?.query?.status)) {
        case 1:
          transactionStatus.push("PAYMENT VERIFICATION");
          transactionStatus.push("PROCESSING");
          transactionStatus.push("PAYMENT REJECTED");
          break;

        case 2:
          transactionStatus.push("SHIPPED");
          break;

        case 3:
          transactionStatus.push("DELIVERED");
          break;

        case 4:
          transactionStatus.push("PAYMENT VERIFICATION");
          transactionStatus.push("PROCESSING");
          transactionStatus.push("SHIPPED");
          transactionStatus.push("DELIVERED");
          transactionStatus.push("PAYMENT REJECTED");
          break;

        default:
          res.status(400).json({
            status: false,
            message: `Invalid Status Requested`,
          });
          return;
      }

      // Processing Transactions
      const { rows: transactions } = await pgConnection.query(
        ` select tl1.live_stream_id,tl1.live_stream_title, 
        to_char(tl1.live_stream_date, 'dd-mm-yyyy') as live_stream_date,tl1.live_stream_start_time, 
        (select json_agg(json_build_object(  
         'transaction_id', tl2.transaction_id, 
        'reference_number', to_char(tl2.transaction_id, '\"MAL\"FM00000000'), 
         'delivery_fees', tl2.delivery_fees, 
        'total_product_price', tl2.total_product_price, 
        'final_price', tl2.final_price, 
        'tracking_url',tl3.tracking_url, 
        'logistic_status',tl3.logistic_status,
         'transaction_date_and_time', to_char(tl2.transaction_date_and_time, 'dd-mm-yyyy HH24:MI:SS'), 
        'transaction_receipt',tl2.transaction_receipt, 
        'courier_company',tl3.courier_company,
        'member_email',tl4.member_email, 
        'member_phone',tl4.member_phone, 
        'member_username',tl4.member_username, 
        'address_line_1', tl2.address_line_1, 
        'address_line_2', tl2.address_line_2, 
        'city',tl2.city, 
        'postcode',tl2.postcode, 
        'state',tl2.state, 
        'purchased_products', coalesce ((select json_agg(json_build_object(  
        'quantity_purchased',tl6.quantity_purchased,  
        'unit_price',tl6.unit_price,  
        'total_price',tl6.total_price, 
        'product_name',tl6.product_name, 
        'product_description',tl6.product_description, 
        'product_image',tl6.product_image, 
        'product_variation_size',tl6.product_variation_size,
        'product_variation_color',tl6.product_variation_color))  
        from tlpurchasedproducts tl6 
        where tl6.transaction_id = tl2.transaction_id 
        ORDER BY array_position 
       ($2::text[], tl3.logistic_status)
        ),'[]')))) as detailed_transaction 
         from tllivestream tl1  
         inner join tltransaction tl2 
          on tl1.live_stream_id = tl2.live_stream_id  
         inner join tllogistic tl3
         on tl2.transaction_id = tl3.transaction_id
        inner join tlmember tl4 
         on tl2.member_id = tl4.member_id 
         where tl1.merchant_id = $1
         and tl3.logistic_status = 
        ANY ($2::text[]) 
        GROUP BY tl1.live_stream_id`,
        [req?.params?.merchantid, transactionStatus]
      );

      res.status(200).json({
        status: true,
        message: `All the transaction details have been successfully retrieved`,
        transactions,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: `Error in retrieving transactions`,
      });
    }
  },

  updateTransactionStatus: async (req, res, next) => {
    if (!req?.params?.transactionid || !req?.query?.status) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    try {
      let updatedRecords;
      //1 = Shipped
      //2 = Delivered
      switch (parseInt(req?.query?.status)) {
        case 1:
          if (!req?.query?.tracking_url || !req?.query?.courier_company) {
            res.status(400).json({
              status: "false",
              message: "No tracking URL or Courier has been provided",
            });
            return;
          }

          updatedRecords = await pgConnection.query(
            "update tllogistic " +
              "set " +
              "logistic_status = $1, " +
              "courier_company = $2, " +
              "tracking_url = $3 " +
              "where transaction_id = $4 ",
            [
              "SHIPPED",
              req?.query?.courier_company,
              req?.query?.tracking_url,
              req?.params?.transactionid,
            ]
          );

          break;

        case 2:
          updatedRecords = await pgConnection.query(
            "update tllogistic " +
              "set " +
              "logistic_status = $1 " +
              "where transaction_id = $2 ",
            ["DELIVERED", req?.params?.transactionid]
          );

          break;

        default:
          res.status(400).json({
            status: false,
            message: `Invalid Status Requested`,
          });
          return;
      }

      if (updatedRecords.rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "The transaction has been successfully updated",
        });
        return;
      } else {
        res.status(400).json({
          status: false,
          message: `No record found for the specific transaction id`,
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: `Error in updating transaction `,
      });
      return;
    }
  },

  approveTransaction: async (req, res, next) => {
    if (!req?.params?.transactionid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        " with update_tltransaction AS ( " +
          "update tltransaction " +
          "set " +
          "transaction_status = $1 " +
          "where transaction_id = $2 " +
          "returning * " +
          "), " +
          "update_tllogistic AS ( " +
          "update tllogistic " +
          "set " +
          "logistic_status = $3 " +
          "where transaction_id = $2 " +
          ") " +
          "select * from update_tltransaction",
        ["PAYMENT APPROVED", req?.params?.transactionid, "PROCESSING"]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "The transaction has been successfully approved",
        });
      } else {
        res.status(400).json({
          status: false,
          message: `No record found the specific transaction id`,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: `Error in approving transaction `,
      });
    }
  },

  rejectTransaction: async (req, res, next) => {
    if (!req?.params?.transactionid || !req?.body?.remarks) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        " with update_tltransaction AS ( " +
          "update tltransaction " +
          "set " +
          "admin_remarks = $1, " +
          "transaction_status = $2 " +
          "where transaction_id = $3 " +
          "returning * " +
          "), " +
          "update_tllogistic AS ( " +
          "update tllogistic " +
          "set " +
          "logistic_status = $4 " +
          "where transaction_id = $3 " +
          ") " +
          "select * from update_tltransaction",
        [
          req?.body?.remarks,
          "PAYMENT REJECTED",
          req?.params?.transactionid,
          "PAYMENT REJECTED",
        ]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "The transaction has been successfully rejected",
        });
      } else {
        res.status(400).json({
          status: false,
          message: `No record found the specific transaction id`,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: `Error in rejecting transaction `,
      });
    }
  },

  getTransactionHistory: async (req, res, next) => {
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        " select tl1.live_stream_id,tl1.live_stream_title, " +
          "tl1.live_stream_date,tl1.live_stream_start_time, " +
          "(select json_agg(json_build_object( " +
          " 'transaction_id', tl2.transaction_id, " +
          " 'member_username',tl4.member_username, " +
          " 'member_email',tl4.member_email, " +
          " 'member_phone',tl4.member_phone, " +
          "  'final_price', tl2.final_price, " +
          "  'quantity_purchased', (select sum(tl5.quantity_purchased)  " +
          "     from tlpurchasedproducts tl5 where tl2.transaction_id = tl5.transaction_id " +
          " )))) as detailed_transaction " +
          " from tllivestream tl1 " +
          " inner join tltransaction tl2 " +
          "  on tl1.live_stream_id = tl2.live_stream_id " +
          " inner join tllogistic tl3 " +
          " on tl2.transaction_id = tl3.transaction_id " +
          " inner join tlmember tl4 " +
          " on tl2.member_id = tl4.member_id " +
          " where tl1.merchant_id = $1 " +
          " and tl3.logistic_status not in ('PAYMENT VERIFICATION','PAYMENT PENDING','PAYMENT REJECTED') " +
          " GROUP BY tl1.live_stream_id",
        [req?.params?.merchantid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Transaction history details have been retrieved",
          transaction_history_results: rows,
        });
      } else {
        res.status(200).json({
          status: false,
          message: "No records found for transaction history",
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error in retrieving transaction history",
      });
      return;
    }
  },

  //User Side
  retrieveMemberTransactionDetails: async (req, res, next) => {
    if (!req?.params?.memberid || !req?.query?.status) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    var paidTransactions = [],
      pendingTransactions = [];

    const status = parseInt(req?.query?.status);

    try {
      var logisticStatus = [];

      switch (status) {
        case 1:
          //    logisticStatus.push("PAYMENT PENDING");
          logisticStatus.push("PAYMENT VERIFICATION");
          logisticStatus.push("PROCESSING");
          logisticStatus.push("PAYMENT REJECTED");
          break;

        case 2:
          logisticStatus.push("SHIPPED");
          break;

        case 3:
          logisticStatus.push("DELIVERED");
          break;

        case 4:
          //       logisticStatus.push("PAYMENT PENDING");
          logisticStatus.push("PAYMENT VERIFICATION");
          logisticStatus.push("PROCESSING");
          logisticStatus.push("SHIPPED");
          logisticStatus.push("DELIVERED");
          logisticStatus.push("PAYMENT REJECTED");
          break;

        default:
          res.status(400).json({
            status: false,
            message: `Invalid Status Requested`,
          });
          return;
      }

      if (status === 1 || status === 4) {
        // Pending Transactions
        const { rows: pending } = await pgConnection.query(
          `select 
          tl1.tracking_url, 
          tl1.logistic_status, 
          tl2.transaction_id, 
          tl2.delivery_fees,
          tl2.admin_remarks,
          tl2.final_price, 
          tl2.transaction_status, 
          tl2.total_product_price, 
          to_char(tl2.transaction_id, '\"MAL\"FM00000000') AS reference_number, 
          to_char(tl2.transaction_date_and_time, 'dd-mm-yyyy HH24:MI:SS') as transaction_date_and_time, 
          tl2.address_line_1, 
          tl2.address_line_2,
          tl2.postcode,
          tl2.city,
          tl2.state,
          tl4.merchant_id, 
          tl4.merchant_company_name, 
          tl4.merchant_email,
          tl4.merchant_bank_number,
          tl4.merchant_bank_holder_name,
          tl4.merchant_bank_name, 
          coalesce((select json_agg(json_build_object( 
          'quantity_purchased',tl5.quantity_purchased, 
          'product_variation_id', tl5.product_variation_id, 
          'product_id', tl5.product_id, 
          'unit_price',tl5.unit_price, 
          'total_price',tl5.total_price, 
          'product_name',tl6.product_name, 
          'product_description',tl6.product_description, 
          'product_image',tl6.product_image, 
          'product_variation_size',tl7.product_variation_size, 
          'product_variation_color',tl7.product_variation_color)) 
          from tlpaymentpendingproducts tl5
          inner join tlproduct tl6
          on tl5.product_id = tl6.product_id 
          inner join tlproductvariation tl7 
          on tl5.product_variation_id = tl7.product_variation_id 
          where tl5.transaction_id = tl2.transaction_id 
          ),'[]') as purchased_products
          from tllogistic tl1 
          inner join tltransaction tl2
          on tl1.transaction_id = tl2.transaction_id
          inner join tlmerchant tl4 
          on tl2.merchant_id = tl4.merchant_id
          where tl1.logistic_status = $2
          and tl2.member_id = $1
          ORDER BY tl2.transaction_date_and_time DESC`,
          [req?.params?.memberid, "PAYMENT PENDING"]
        );

        pendingTransactions = pending;
      }

      // Paid Transactions
      const { rows: paid } = await pgConnection.query(
        ` select 
        tl1.tracking_url,
        tl1.logistic_status,
        tl2.transaction_id,
        tl2.delivery_fees,
        tl2.admin_remarks,
        tl2.final_price,
        tl2.transaction_status, 
        tl2.total_product_price, 
        to_char(tl2.transaction_id, '\"MAL\"FM00000000') AS reference_number,
        to_char(tl2.transaction_date_and_time, 'dd-mm-yyyy HH24:MI:SS') as transaction_date_and_time, 
        tl2.address_line_1, 
        tl2.address_line_2,
        tl2.postcode,
        tl2.city,
        tl2.state,
        tl4.merchant_id, 
        tl4.merchant_company_name,
        tl4.merchant_email,
        tl4.merchant_bank_number,
        tl4.merchant_bank_holder_name,
        tl4.merchant_bank_name, 
		    coalesce(
        (select json_agg(json_build_object( 
        'quantity_purchased',tl5.quantity_purchased, 
        'unit_price',tl5.unit_price,
        'total_price',tl5.total_price, 
        'product_name',tl5.product_name, 
        'product_description',tl5.product_description, 
        'product_image',tl5.product_image, 
        'product_variation_size',tl5.product_variation_size, 
        'product_variation_color',tl5.product_variation_color)) 
        from tlpurchasedproducts tl5 
        where tl5.transaction_id = tl2.transaction_id 
        ),'[]') as purchased_products 
        from tllogistic tl1 
        inner join tltransaction tl2
        on tl1.transaction_id = tl2.transaction_id 
        inner join tlmerchant tl4
        on tl2.merchant_id = tl4.merchant_id 
        where tl2.member_id = $1 
		    and tl1.logistic_status = ANY ($2::text[])  
        ORDER BY array_position 
        ($2::text[], tl1.logistic_status), 
       tl2.transaction_date_and_time DESC`,
        [req?.params?.memberid, logisticStatus]
      );

      paidTransactions = paid;

      res.status(200).json({
        status: true,
        message: `All the transaction details have been successfully retrieved`,
        transactions: [...pendingTransactions, ...paidTransactions],
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: `Error in retrieving transactions`,
      });
    }
  },

  addNewTransactionWithoutReceipt: async (req, res, next) => {
    if (
      !req?.body?.live_stream_id ||
      !req?.body?.merchant_id ||
      !req?.body?.address_id ||
      !req?.body?.member_id ||
      !req?.body?.final_price ||
      !req?.body?.total_product_price ||
      !req?.body?.delivery_fees ||
      !req?.body?.product
    ) {
      res.status(400).json({
        status: false,
        message: `Insufficient information provided.`,
      });
      return;
    }

    if (!Array.isArray(req?.body?.product) || req?.body?.product.length <= 0) {
      res.status(400).json({
        status: false,
        message: `Insufficient information provided.`,
      });
      return;
    }

    let product = req?.body?.product;

    try {
      //Get User Address
      const { rows: addressRow, rowCount: addressRowCount } =
        await pgConnection.query(
          "select * from tlmemberaddress where address_id = $1 limit 1",
          [req?.body?.address_id]
        );

      if (addressRowCount <= 0) {
        throw "Address is Not Found";
      }

      //Destructure Address Result into individual properties
      const { address_line_1, address_line_2, city, postcode, state } =
        addressRow[0];

      let productId = [],
        productVariationId = [],
        productUnitPrice = [],
        productTotalPrice = [],
        productQuantity = [];

      product.forEach((element) => {
        productId.push(element.product_id ?? "");
        productVariationId.push(element.product_variation_id ?? "");
        productQuantity.push(parseInt(element.quantity) ?? 0);
        productUnitPrice.push(parseFloat(element.unit_price).toFixed(2) ?? 0);
        productTotalPrice.push(parseFloat(element.total_price).toFixed(2) ?? 0);
      });

      //Retrieve Variation Details and Check if the Requested Quantity is Available
      const {
        rows: productVariationDetails,
        rowCount: productVariationRowCount,
      } = await pgConnection.query(
        "SELECT * FROM tlproductvariation WHERE product_variation_id = ANY ($1::integer[])",
        [productVariationId]
      );
      if (productVariationRowCount > 0) {
        var isProductAvailable = true;

        breakloop: for (var i = 0; i < product.length; i++) {
          for (var j = 0; j < productVariationDetails.length; j++) {
            if (
              product[i].product_variation_id ===
              productVariationDetails[j].product_variation_id
            ) {
              if (
                productVariationDetails[j].product_variation_quantity === 0 ||
                product[i].quantity >
                  productVariationDetails[j].product_variation_quantity
              ) {
                isProductAvailable = false;
                break breakloop;
              }
            }
          }
        }

        if (!isProductAvailable) {
          res.status(200).json({
            status: false,
            message: `Some products are unavailable.`,
          });
          return;
        }
      } else {
        res.status(200).json({
          status: "false",
          message: `No product details found.`,
        });
        return;
      }

      const { rowCount: transactionRowCount } = await pgConnection.query(
        "with insert_transaction_details AS ( " +
          "insert into tltransaction(live_stream_id,merchant_id,address_line_1,address_line_2, city, postcode, state,member_id,total_product_price,delivery_fees,final_price,transaction_status) " +
          "values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) " +
          "returning * " +
          "), " +
          "insert_logistic_details AS ( " +
          "insert into tllogistic (transaction_id) " +
          "select transaction_id from insert_transaction_details " +
          "), " +
          "insert_purchased_product_details AS ( " +
          "insert into tlpaymentpendingproducts (transaction_id, product_id, product_variation_id, quantity_purchased, unit_price, total_price) " +
          "select transaction_id, UNNEST($13::integer[]), UNNEST($14::integer[]), UNNEST($15::integer[]), UNNEST($16::decimal[]), UNNEST($17::decimal[]) " +
          "from insert_transaction_details " +
          ") " +
          "select * from insert_transaction_details;",
        [
          req?.body?.live_stream_id,
          req?.body?.merchant_id,
          address_line_1,
          address_line_2,
          city,
          postcode,
          state,
          req?.body?.member_id,
          req?.body?.total_product_price,
          req?.body?.delivery_fees,
          req?.body?.final_price,
          "PAYMENT PENDING",
          productId,
          productVariationId,
          productQuantity,
          productUnitPrice,
          productTotalPrice,
        ]
      );

      if (transactionRowCount > 0) {
        res.status(200).json({
          status: true,
          message: `Your record has been saved. Please proceed to make payment and upload receipt.`,
        });
        return;
      } else {
        res.status(200).json({
          status: false,
          message: "Please try again. No record has been updated.",
        });
        return;
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: `Error in saving transaction details. Please try again. `,
      });
      console.log(error);
    }
  },

  addNewTransactionWithReceipt: async (req, res, next) => {
    //upload transaction receipt
    try {
      await uploadTransactionReceipt(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_transaction_receipt") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }

    try {
      // construct URL for saved transaction receipt image
      const transactionReceiptUrl =
        req.protocol +
        "://" +
        req.get("host") +
        "/transaction/" +
        req?.file?.filename;

      let product;
      if (req?.body?.product) {
        product = JSON.parse(req?.body?.product);
      }

      //Get User Address
      const { rows: addressRow, rowCount: addressRowCount } =
        await pgConnection.query(
          "select * from tlmemberaddress where address_id = $1 limit 1",
          [req?.body?.address_id]
        );

      if (addressRowCount <= 0) {
        throw "Address is Not Found";
      }

      //Destructure Address Result into individual properties
      const { address_line_1, address_line_2, city, postcode, state } =
        addressRow[0];

      let customQuerytoUpdateProductVariation = "",
        productUnitPrice = [],
        productTotalPrice = [],
        productQuantity = [],
        productVariationId = [],
        productVariationSize = [],
        productVariationColor = [],
        productName = [],
        productDescription = [],
        productImage = [],
        productMainCategoryName = [],
        productSubCategoryName = [];

      product.forEach((element) => {
        productVariationId.push(element.product_variation_id ?? "");
      });

      //Get Product Details

      const {
        rows: productVariationDetails,
        rowCount: productVariationRowCount,
      } = await pgConnection.query(
        `select
        tl1.product_variation_id,tl1.product_id,
        tl1.product_variation_color,tl1.product_variation_size, tl1.product_variation_quantity,
        tl2.product_name, tl2.product_description, tl2.product_image,
        tl3.product_main_cat_name,
        tl4.product_sub_cat_name
        from tlproductvariation tl1 
        inner join tlproduct tl2 
        on tl1.product_id = tl2.product_id
        inner join tlproductmaincategory tl3
        on tl3.product_main_cat_id = tl2.product_main_cat_id
        inner join tlproductsubcategory tl4
        on tl2.product_sub_cat_id = tl4.product_sub_cat_id
        where tl1.product_variation_id = ANY ($1::integer[])
        order by tl1.product_variation_id ASC `,
        [productVariationId]
      );

      // Check if the Requested Quantity is Available

      if (productVariationRowCount > 0) {
        var isProductAvailable = true;

        breakloop: for (var i = 0; i < product.length; i++) {
          for (var j = 0; j < productVariationDetails.length; j++) {
            if (
              product[i].product_variation_id ===
              productVariationDetails[j].product_variation_id
            ) {
              if (
                productVariationDetails[j].product_variation_quantity === 0 ||
                product[i].quantity >
                  productVariationDetails[j].product_variation_quantity
              ) {
                isProductAvailable = false;
                break breakloop;
              }
            }
          }
        }
        if (!isProductAvailable) {
          // clear arrays
          productVariationId = [];

          res.status(200).json({
            status: "false",
            message: `One of the product variations is not available.`,
          });

          //delete transaction receipt
          const transactionReceipt = `${prodImageDirectory}/transaction/${req?.file?.filename}`;
          if (fs.existsSync(transactionReceipt)) {
            fs.unlinkSync(transactionReceipt);
          }

          return;
        }
      } else {
        res.status(200).json({
          status: "false",
          message: `No product details found.`,
        });
        return;
      }

      //Create Product Arrays for storing in transaction table

      productVariationId = [];
      product.forEach((element) => {
        productVariationId.push(element.product_variation_id ?? "");

        customQuerytoUpdateProductVariation =
          customQuerytoUpdateProductVariation +
          ` WHEN product_variation_id = ${element.product_variation_id} THEN ${element.quantity} `;
        productQuantity.push(parseInt(element.quantity) ?? 0);
        productUnitPrice.push(parseFloat(element.unit_price).toFixed(2) ?? 0);
        productTotalPrice.push(parseFloat(element.total_price).toFixed(2) ?? 0);

        productVariationDetails.forEach((singleProduct) => {
          if (
            element.product_variation_id === singleProduct.product_variation_id
          ) {
            productVariationColor.push(singleProduct.product_variation_color);
            productVariationSize.push(singleProduct.product_variation_size);
            productName.push(singleProduct.product_name);
            productDescription.push(singleProduct.product_description);
            productImage.push(singleProduct.product_image);
            productMainCategoryName.push(singleProduct.product_main_cat_name);
            productSubCategoryName.push(singleProduct.product_sub_cat_name);
          }
        });
      });

      const { rowCount: transactionRowCount } = await pgConnection.query(
        "with insert_transaction_details AS ( " +
          "insert into tltransaction(live_stream_id,merchant_id,address_line_1,address_line_2, city, postcode, state ,member_id,payment_method,transaction_receipt,total_product_price,delivery_fees,final_price,transaction_status) " +
          "values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) " +
          "returning * " +
          "), " +
          "insert_purchased_product_details AS ( " +
          "insert into tlpurchasedproducts (transaction_id, quantity_purchased, unit_price, total_price,product_variation_size,product_variation_color,product_name,product_description,product_main_cat_name,product_sub_cat_name,product_image) " +
          "select transaction_id, UNNEST($15::integer[]), UNNEST($16::decimal[]), UNNEST($17::decimal[]), UNNEST($18::text[]), UNNEST($19::text[]), UNNEST($20::text[]), UNNEST($21::text[]), UNNEST($22::text[]), UNNEST($23::text[]),UNNEST($26::text[]) " +
          "from insert_transaction_details " +
          "), " +
          "insert_logistic_details AS ( " +
          "insert into tllogistic (transaction_id,logistic_status) " +
          "select transaction_id, $24 from insert_transaction_details " +
          "), " +
          "update_product_variation AS ( " +
          " UPDATE tlproductvariation SET " +
          "product_variation_quantity = product_variation_quantity -  " +
          "CASE " +
          customQuerytoUpdateProductVariation +
          "   END " +
          "	WHERE product_variation_id = ANY ($25::integer[]) " +
          ") " +
          "select * from insert_transaction_details;",
        [
          req?.body?.live_stream_id,
          req?.body?.merchant_id,
          address_line_1,
          address_line_2,
          city,
          postcode,
          state,
          req?.body?.member_id,
          req?.body?.payment_method,
          transactionReceiptUrl,
          req?.body?.total_product_price,
          req?.body?.delivery_fees,
          req?.body?.final_price,
          "PAYMENT VERIFICATION",
          productQuantity,
          productUnitPrice,
          productTotalPrice,
          productVariationSize,
          productVariationColor,
          productName,
          productDescription,
          productMainCategoryName,
          productSubCategoryName,
          "PAYMENT VERIFICATION",
          productVariationId,
          productImage,
        ]
      );

      if (transactionRowCount > 0) {
        res.status(200).json({
          status: true,
          message: `You have successfully performed your transactions. Please wait for the admin to verify`,
        });
        return;
      } else {
        res.status(200).json({
          status: false,
          message: "Please try again. No record has been updated.",
        });
        return;
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message:
          error.detail ??
          `Error in saving transaction details. Please try again. `,
      });
      console.log(error);
      //delete transaction receipt
      const transactionReceipt = `${prodImageDirectory}/transaction/${req?.file?.filename}`;
      if (fs.existsSync(transactionReceipt)) {
        fs.unlinkSync(transactionReceipt);
      }
      return;
    }
  },

  updateTransactionWithReceipt: async (req, res, next) => {
    console.log(req.params);
    //Check whether transaction is provided
    if (!req?.params?.transactionid) {
      res.status(400).json({
        status: "false",
        message: "Transaction ID is not provided",
      });
      return;
    }

    let customQuerytoUpdateProductVariation = "",
      productUnitPrice = [],
      productTotalPrice = [],
      productQuantity = [],
      productVariationId = [],
      productVariationSize = [],
      productVariationColor = [],
      productName = [],
      productDescription = [],
      productMainCategoryName = [],
      productImage = [],
      productSubCategoryName = [];

    try {
      //Retrieve Variation Details and Check if the Requested Quantity is Available
      const {
        rows: productVariationDetails,
        rowCount: productVariationRowCount,
      } = await pgConnection.query(
        `select
      tl1.product_variation_id,tl1.product_id,tl1.product_variation_color,tl1.product_variation_size,
      tl2.product_name, tl2.product_description, tl2.product_image,
      tl3.product_main_cat_name,
      tl4.product_sub_cat_name,
      tl5.quantity_purchased,
      tl5.unit_price,
      tl5.total_price
      from tlpaymentpendingproducts tl5
      inner join tlproductvariation tl1 
      on tl5.product_variation_id = tl1.product_variation_id
      inner join tlproduct tl2 
      on tl1.product_id = tl2.product_id
      inner join tlproductmaincategory tl3
      on tl3.product_main_cat_id = tl2.product_main_cat_id
      inner join tlproductsubcategory tl4
      on tl2.product_sub_cat_id = tl4.product_sub_cat_id
      where tl5.transaction_id = $1
      order by tl1.product_variation_id ASC`,
        [req?.params?.transactionid]
      );

      if (productVariationRowCount > 0) {
        var isProductAvailable = true;

        breakloop: for (var j = 0; j < productVariationDetails.length; j++) {
          if (
            productVariationDetails[j].product_variation_quantity === 0 ||
            productVariationDetails[j].quantity_purchased >
              productVariationDetails[j].product_variation_quantity
          ) {
            isProductAvailable = false;
            break breakloop;
          }
          // Custom Query to Deduct the Overall Product Variation Quantity after Successful Transaction
          customQuerytoUpdateProductVariation =
            customQuerytoUpdateProductVariation +
            ` WHEN product_variation_id = ${productVariationDetails[j].product_variation_id} THEN ${productVariationDetails[j].quantity_purchased} `;
          productVariationId.push(
            productVariationDetails[j].product_variation_id
          );
        }

        if (!isProductAvailable) {
          productVariationId = [];
          res.status(200).json({
            status: "false",
            message: `One of the product variations is not available.`,
          });
          return;
        }
      } else {
        res.status(200).json({
          status: "false",
          message: `No product details found.`,
        });
        return;
      }

      productVariationId = [];
      productVariationDetails.forEach((singleProduct) => {
        productVariationId.push(singleProduct.product_variation_id ?? "");
        customQuerytoUpdateProductVariation =
          customQuerytoUpdateProductVariation +
          ` WHEN product_variation_id = ${singleProduct.product_variation_id} THEN ${singleProduct.quantity_purchased} `;
        productQuantity.push(parseInt(singleProduct.quantity_purchased) ?? 0);
        productUnitPrice.push(
          parseFloat(singleProduct.unit_price).toFixed(2) ?? 0
        );
        productTotalPrice.push(
          parseFloat(singleProduct.total_price).toFixed(2) ?? 0
        );
        productVariationColor.push(singleProduct.product_variation_color ?? "");
        productVariationSize.push(singleProduct.product_variation_size);
        productName.push(singleProduct.product_name);
        productDescription.push(singleProduct.product_description);
        productMainCategoryName.push(singleProduct.product_main_cat_name);
        productSubCategoryName.push(singleProduct.product_sub_cat_name);
        productImage.push(singleProduct.product_image);
      });
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: `Error in saving transaction details. Please try again. `,
      });
      console.log(error);
      return;
    }

    //upload transaction receipt

    try {
      await uploadTransactionReceiptUpdate(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_transaction_receipt") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }

    try {
      // construct URL for saved transaction receipt image
      const transactionReceiptUrl =
        req.protocol +
        "://" +
        req.get("host") +
        "/transaction/" +
        req?.file?.filename;

      const { rowCount: transactionRowCount } = await pgConnection.query(
        "with update_transaction_details AS ( " +
          "UPDATE TLTRANSACTION SET transaction_receipt = $2, transaction_status = $3, payment_method =$4 " +
          "WHERE transaction_id = $1 " +
          "returning * " +
          "), " +
          "update_logistic_details AS ( " +
          "UPDATE tllogistic SET logistic_status = $3 " +
          "WHERE transaction_id = $1 " +
          "), " +
          "insert_purchased_product_details AS ( " +
          "insert into tlpurchasedproducts (transaction_id, quantity_purchased, unit_price, total_price,product_variation_size,product_variation_color,product_name,product_description,product_main_cat_name,product_sub_cat_name, product_image) " +
          "select transaction_id, UNNEST($5::integer[]), UNNEST($6::decimal[]), UNNEST($7::decimal[]), UNNEST($8::text[]), UNNEST($9::text[]), UNNEST($10::text[]), UNNEST($11::text[]), UNNEST($12::text[]), UNNEST($13::text[]),UNNEST($14::text[]) " +
          "from update_transaction_details " +
          "), " +
          "delete_from_payment_pending AS ( " +
          "delete from tlpaymentpendingproducts where transaction_id = $1 " +
          "), " +
          "update_product_variation AS ( " +
          " UPDATE tlproductvariation SET " +
          "product_variation_quantity = product_variation_quantity -  " +
          "CASE " +
          customQuerytoUpdateProductVariation +
          " END " +
          "	WHERE product_variation_id = ANY ($15::integer[]) " +
          ") " +
          "select * from update_transaction_details;",

        [
          req?.params?.transactionid,
          transactionReceiptUrl,
          "PAYMENT VERIFICATION",
          req?.body?.payment_method,
          productQuantity,
          productUnitPrice,
          productTotalPrice,
          productVariationSize,
          productVariationColor,
          productName,
          productDescription,
          productMainCategoryName,
          productSubCategoryName,
          productImage,
          productVariationId,
        ]
      );

      if (transactionRowCount > 0) {
        res.status(200).json({
          status: true,
          message: `You have successfully submitted your transaction_receipt. Please wait for the merchant to reply.`,
        });
        return;
      } else {
        res.status(200).json({
          status: false,
          message: "Please try again. No record has been updated.",
        });

        //delete transaction receipt
        const transactionReceipt = `${prodImageDirectory}/transaction/${req?.file?.filename}`;
        if (fs.existsSync(transactionReceipt)) {
          fs.unlinkSync(transactionReceipt);
        }
        return;
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: `Error in saving transaction details. Please try again. `,
      });
      console.log(error);
      //delete transaction receipt
      const transactionReceipt = `${prodImageDirectory}/transaction/${req?.file?.filename}`;
      if (fs.existsSync(transactionReceipt)) {
        fs.unlinkSync(transactionReceipt);
      }
      return;
    }
  },
};
