const pgConnection = require("../../../config/database");
const { devImageDirectory, prodImageDirectory } = require("../../../constants");
const uploadLiveStreamImage = require("../helper/upload_images/uploadLiveStreamImage");
var fs = require("fs");
const { isNull } = require("lodash");

module.exports = {
  //Merchant Side

  addLiveStreamDetails: async (req, res, next) => {
    //check if merchantid exists as a param
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //upload liveStream Thumbnail
    try {
      await uploadLiveStreamImage(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_livestream_image") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }

    try {
      const liveStreamImageUrl =
        req.protocol +
        "://" +
        req.get("host") +
        "/livestream/" +
        req?.file?.filename;

      let product;
      if (req?.body?.product) {
        product = JSON.parse(req?.body?.product);
      }

      let productId = [],
        sequence = [];

      product.forEach((element) => {
        productId.push(element.product_id ?? "");
        sequence.push(element.sequence ?? "");
      });

      let insertLiveStreamDetails = await pgConnection.query(
        "WITH action_livestream_insert AS (" +
          " INSERT INTO tllivestream (live_stream_title,live_stream_description,live_stream_date,live_stream_start_time,live_stream_end_time,merchant_id,live_stream_image,live_stream_status,shipping_fee)" +
          " VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) " +
          " RETURNING *" +
          " )," +
          " action_livestreamproduct_insert AS (" +
          " INSERT INTO tllivestreamproducts (live_stream_id,product_id, sequence)" +
          " SELECT live_stream_id, UNNEST($10::integer[]), UNNEST($11::integer[])  FROM action_livestream_insert RETURNING *" +
          " )" +
          " SELECT *" +
          " FROM action_livestream_insert ",
        [
          req?.body?.title,
          req?.body?.description,
          req?.body?.date,
          req?.body?.start_time,
          req?.body?.end_time,
          req?.params?.merchantid,
          liveStreamImageUrl,
          req?.body?.status,
          req?.body?.shipping_fee,
          productId,
          sequence,
        ]
      );

      res.status(201).json({
        status: "true",
        message: "Successfully saved livestream details.",
        results: insertLiveStreamDetails?.rows[0] ?? {},
      });
    } catch (error) {
      console.log(error);
      //delete uploaded livestream file if postgres fails to save record
      const liveStreamImageFile = `${prodImageDirectory}/livestream/${req?.file?.filename}`;
      if (fs.existsSync(liveStreamImageFile)) {
        fs.unlinkSync(liveStreamImageFile);
      }
      res.status(400).json({
        status: "false",
        message: error?.detail ?? "Error in saving livestream information",
      });
    }
  },

  getMerchantLiveStreamAll: async (req, res, next) => {
    //check if merchantid exists as a param
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    try {
      //Change livestream status to expired if it is not started on time.
      await pgConnection.query(
        "UPDATE tllivestream " +
          "SET live_stream_status = 'EXPIRED' " +
          "WHERE merchant_id = $1 " +
          "AND live_stream_status " +
          "IN ('LIVE NOW','SAVED','PUBLISHED') " +
          "AND (live_stream_date + live_stream_start_time) " +
          "+ interval '1 hour' - current_timestamp < interval '1 second' ",
        [req?.params?.merchantid]
      );

      //Change 'expired' livestream status to published if the time has been modified.
      await pgConnection.query(
        "UPDATE tllivestream " +
          "SET live_stream_status = 'PUBLISHED' " +
          "WHERE merchant_id = $1 " +
          "AND live_stream_status " +
          "IN ('EXPIRED') " +
          "AND (live_stream_date + live_stream_start_time) " +
          " - current_timestamp >= interval '1 hour' ",
        [req?.params?.merchantid]
      );

      const { rows } = await pgConnection.query(
        "select * from tllivestream where merchant_id= $1 and isdelete=false " +
          "ORDER BY array_position " +
          "(array['SAVED','PUBLISHED','LIVE NOW','FINISHED','EXPIRED'], live_stream_status), " +
          "live_stream_date, live_stream_start_time",
        [req?.params?.merchantid]
      );

      res.status(200).json({
        status: "true",
        message: "All livestream details have been retrieved",
        live_streams_result: rows,
      });
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in retrieving livestream details.",
      });
    }
  },

  startLiveStream: async (req, res, next) => {
    //check if all the required deatails exist in the request
    if (!req?.params?.livestreamid || !req?.body?.bambuserid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        "with update_live_stream_status as " +
          "( UPDATE tllivestream " +
          "SET bambuser_id = $1,  " +
          "live_stream_status = 'LIVE NOW'  " +
          "WHERE live_stream_id = $2 RETURNING *), " +
          "action_increase_energy AS ( " +
          "insert into tlmerchantenergy (energy_id,merchant_id,energy_redeemed_total) " +
          "select 'MLS',merchant_id,$3 from update_live_stream_status " +
          ") " +
          "select * from update_live_stream_status",
        [req?.body?.bambuserid, req?.params?.livestreamid, 100]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "The livestream record has been successfully updated.",
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No record has been updated for the livestream id",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in updating livestream details",
      });
    }
  },

  stopLiveStream: async (req, res, next) => {
    //check if all the required deatails exist in the request
    if (
      !req?.params?.livestreamid ||
      !req?.body?.bambuserid ||
      req?.body?.remaininglivetime == null ||
      req?.body?.livestreamcount == null
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const {
        rows: updatedLiveStreamRow,
        rowCount: updatedLiveStreamRowCount,
      } = await pgConnection.query(
        "UPDATE tllivestream " +
          "SET bambuser_id = $1, " +
          "live_stream_status = 'FINISHED', " +
          "live_stream_count = $3 " +
          "WHERE live_stream_id = $2 RETURNING *;",
        [
          req?.body?.bambuserid,
          req?.params?.livestreamid,
          req?.body?.livestreamcount,
        ]
      );

      if (updatedLiveStreamRowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "No record has been updated for the livestream id",
        });
        return;
      }

      const { rowCount: updatedMerchantRowCount } = await pgConnection.query(
        "UPDATE tlmerchant SET remaining_live_time = $2 where merchant_id =$1 returning *",
        [updatedLiveStreamRow[0].merchant_id, req?.body?.remaininglivetime]
      );

      if (updatedMerchantRowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "No record has been updated for the livestream id",
        });
        return;
      }

      res.status(200).json({
        status: "true",
        message: "The livestream record has been successfully updated.",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in updating livestream details",
      });
    }
  },

  getMerchantLiveStreamProductDetails: async (req, res, next) => {
    if (!req?.params?.livestreamid || !req?.body?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows: existingProducts, rowCount } = await pgConnection.query(
        "	SELECT tl1.*,tl2.status,tl2.sequence, " +
          "	(SELECT json_agg(json_build_object( " +
          "	'product_variation_id',tl3.product_variation_id, " +
          "	   'product_variation_color',tl3.product_variation_color, " +
          "	  'product_variation_quantity',tl3.product_variation_quantity, " +
          "	  'product_variation_size',tl3.product_variation_size, " +
          "	  'product_variation_price',tl3.product_variation_price, " +
          "	  'product_variation_status',tl3.product_variation_status)) " +
          "	 AS variation " +
          "	  FROM tlproductvariation tl3 " +
          "	  WHERE tl1.product_id = tl3.product_id)  " +
          "	  FROM tlproduct tl1  " +
          "	  INNER JOIN tllivestreamproducts tl2  " +
          "	  ON tl1.product_id=tl2.product_id  " +
          "	  WHERE tl2.live_stream_id = $1 " +
          "	  AND tl1.merchant_id = $2",
        [req?.params?.livestreamid, req?.body?.merchantid]
      );

      if (rowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "There are no existing products for the livestream",
        });
        return;
      } else {
        res.status(200).json({
          status: "true",
          message: "All Products have been successfully retrieved",
          live_stream_product_results: existingProducts,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving product details of the livestream",
      });
    }
  },

  /* oldGetMerchantLiveStreamProductDetails: async (req, res, next) => {
    if (!req?.params?.livestreamid || !req?.body?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows: existingProducts, rowCount } = await pgConnection.query(
        "select tl1.*,true as is_selected from tlproduct tl1 " +
          "inner join tllivestreamproducts tl2 " +
          "on tl1.product_id=tl2.product_id " +
          "where tl2.live_stream_id = $1 " +
          "AND tl1.merchant_id = $2",
        [req?.params?.livestreamid, req?.body?.merchantid]
      );

      if (rowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "There are no existing products for the livestream",
        });
        return;
      } else {
        let existingProductsId = [];

        existingProducts.forEach((element) => {
          existingProductsId.push(element.product_id);
        });

        const { rows: nonExistingProducts, rowCount } =
          await pgConnection.query(
            "select *,false as is_selected  from tlproduct " +
              "WHERE merchant_id = $1 " +
              "AND NOT (product_id = ANY ($2::integer[]))",
            [req?.body?.merchantid, existingProductsId]
          );

        const allProducts = [...existingProducts, ...nonExistingProducts];

        res.status(200).json({
          status: "true",
          message: "All Products have been successfully retrieved",
          live_stream_product_results: allProducts ?? [],
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving product details of the livestream",
      });
    }
  },
*/

  modifyMerchantLiveStreamProduct: async (req, res, next) => {
    if (!req?.params?.livestreamid || !req?.body?.product) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    let product = req?.body?.product;

    if (!Array.isArray(product) || product <= 0) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    let productIdList = [],
      liveStreamIdList = [],
      productStatusList = [],
      productSequenceList = [];

    product.forEach((element) => {
      liveStreamIdList.push(req?.params?.livestreamid ?? "");
      productIdList.push(element.product_id ?? "");
      productStatusList.push(element.status ?? false);
      productSequenceList.push(element.sequence ?? "");
    });

    console.log(productStatusList);

    try {
      const { rowCount, rows } = await pgConnection.query(
        "INSERT INTO tllivestreamproducts(live_stream_id,product_id, sequence, status) " +
          "SELECT UNNEST($1::integer[]), UNNEST($2::integer[]), UNNEST($3::integer[]), UNNEST($4::boolean[]) " +
          "ON CONFLICT (live_stream_id, product_id) " +
          "DO UPDATE SET sequence = excluded.sequence, status = excluded.status " +
          "RETURNING * ",
        [
          liveStreamIdList,
          productIdList,
          productSequenceList,
          productStatusList,
        ]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Successfully updated the product details of livestream",
        });
      } else {
        res.status(200).json({
          status: false,
          message: "No product details have been updated.",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error while updating the product details of livestream",
      });
    }
  },

  /*oldModifyMerchantLiveStreamProduct: async (req, res, next) => {
    if (
      !req?.params?.livestreamid ||
      !req?.params?.merchantid ||
      !req?.body?.product ||
      !Array.isArray(req?.body?.product) ||
      req?.body?.product <= 0
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    let productIdList = [],
      liveStreamIdList = [],
      productSequenceList = [];

    req?.body?.product.forEach((element) => {
      liveStreamIdList.push(req?.params?.livestreamid ?? "");
      productIdList.push(element.product_id ?? "");
      productSequenceList.push(element.sequence ?? "");
    });

    try {
      await pgConnection.query("BEGIN;");
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error while updating the product details of livestream",
      });
      return;
    }

    try {
      await pgConnection.query(
        "delete from tllivestreamproducts where live_stream_id = $1;",
        [req?.params?.livestreamid]
      );
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error while updating the product details of livestream",
      });
      try {
        await pgConnection.query("ROLLBACK;");
      } catch (error) {
        console.log(error);
      }
      return;
    }

    try {
      await pgConnection.query(
        "INSERT INTO tllivestreamproducts (live_stream_id, product_id, sequence) " +
          "select UNNEST($1::integer[]),UNNEST($2::integer[]),UNNEST($3::integer[]);",
        [liveStreamIdList, productIdList, productSequenceList]
      );
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error while updating the product details of livestream",
      });
      try {
        await pgConnection.query("ROLLBACK;");
      } catch (error) {
        console.log(error);
      }
      return;
    }

    try {
      await pgConnection.query("COMMIT;");
      res.status(200).json({
        status: true,
        message: "Successfully Updated Product Details of the Live Stream",
      });
      return;
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error while updating the product details of livestream3",
      });
      try {
        await pgConnection.query("ROLLBACK;");
      } catch (error) {
        console.log(error);
      }
      return;
    }
  },

  */

  deleteLiveStream: async (req, res, next) => {
    if (!req?.params?.livestreamid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount, rows } = await pgConnection.query(
        "UPDATE tllivestream " +
          "SET isdelete = true " +
          "where live_stream_id = $1 " +
          "RETURNING * ",
        [req?.params?.livestreamid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "Successfully deleted the livestream.",
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No livestream has been deleted.",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in deleting livestream details",
      });
    }
  },

  getMerchantLiveStreamOne: async (req, res, next) => {
    if (!req?.params?.livestreamid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount, rows } = await pgConnection.query(
        `select tl1.*, 
        coalesce((select json_agg(json_build_object('product_id',tl3.product_id, 
        'product_name', tl3.product_name, 
        'product_description', tl3.product_description, 
        'product_image', tl3.product_image, 
        'product_main_cat_id', tl3.product_main_cat_id, 
        'product_sub_cat_id', tl3.product_sub_cat_id, 
        'product_status', tl3.product_status, 
        'merchant_id', tl3.merchant_id)) 
        from tlproduct tl3 
        left join tllivestreamproducts tl4 
        on tl3.product_id = tl4.product_id 
        left join tllivestream tl5 
        on tl4.live_stream_id = tl5.live_stream_id 
        where tl5.live_stream_id = $1),'[]') as products 
        from tllivestream tl1 where tl1.live_stream_id = $1 limit 1`,
        [req?.params?.livestreamid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "Successfully retrieved the livestream.",
          live_stream_result: rows[0],
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No livestream has been found.",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving livestream details",
      });
    }
  },

  updateLiveStream: async (req, res, next) => {
    //check if livestream id exists as a param
    if (!req?.params?.livestreamid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    //upload liveStream Thumbnail
    try {
      await uploadLiveStreamImage(req, res);
      if (
        !req?.body?.title ||
        !req?.body?.description ||
        !req?.body?.date ||
        !req?.body?.start_time ||
        !req?.body?.end_time ||
        !req?.body?.product ||
        !req?.body?.shipping_fee ||
        !req?.body?.status
      ) {
        var message = `Insufficient information provided`;
        throw message;
      }
    } catch (error) {
      //delete uploaded livestream file if postgres fails to save record
      const liveStreamImageFile = `${prodImageDirectory}/livestream/${req?.file?.filename}`;
      if (fs.existsSync(liveStreamImageFile)) {
        fs.unlinkSync(liveStreamImageFile);
      }

      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to save information: ${error}`,
      });
      return;
    }

    try {
      let product = JSON.parse(req?.body?.product);

      if (!Array.isArray(product) || product.length <= 0) {
        res.status(400).json({
          status: "false",
          message: "Invalid product information provided",
        });
        return;
      }

      let productId = [],
        sequence = [];

      product.forEach((element) => {
        productId.push(element?.product_id ?? "");
        sequence.push(element?.sequence ?? "");
      });

      var updateQueryParameters = [
        req?.body?.title,
        req?.body?.description,
        req?.body?.date,
        req?.body?.start_time,
        req?.body?.end_time,
        req?.body?.status,
        req?.body?.shipping_fee,
        req?.params?.livestreamid,
        productId,
        sequence,
      ];
      var additionalQueryForUploadingImage = ` `;
      if (req?.file?.filename) {
        additionalQueryForUploadingImage = ` ,live_stream_image = $11 `;
        const liveStreamImageUrl =
          req.protocol +
          "://" +
          req.get("host") +
          "/livestream/" +
          req?.file?.filename;
        updateQueryParameters.push(liveStreamImageUrl);
      }

      const { rowCount, rows } = await pgConnection.query(
        "with  action_livestream_update AS ( " +
          "update tllivestream " +
          "set live_stream_title = $1, " +
          "live_stream_description = $2, " +
          "live_stream_date = $3, " +
          "live_stream_start_time = $4, " +
          "live_stream_end_time = $5, " +
          "live_stream_status = $6, " +
          "shipping_fee = $7 " +
          `${additionalQueryForUploadingImage}` +
          "where live_stream_id = $8 " +
          "returning * " +
          "), " +
          "action_delete_current_products AS ( " +
          "delete from tllivestreamproducts where live_stream_id = $8 " +
          "), " +
          "action_livestreamproduct_insert AS ( " +
          "INSERT INTO tllivestreamproducts (live_stream_id,product_id, sequence) " +
          "SELECT $8, UNNEST($9::integer[]), UNNEST($10::integer[]) " +
          "FROM action_livestream_update " +
          "ON CONFLICT (live_stream_id, product_id) " +
          "DO UPDATE SET sequence = excluded.sequence " +
          "RETURNING * " +
          ") " +
          "select * from action_livestream_update",
        updateQueryParameters
      );

      if (rowCount > 0) {
        res.status(201).json({
          status: "true",
          message: "Successfully updated livestream details.",
        });
      } else {
        res.status(201).json({
          status: "false",
          message: "No matching records found for the specific livestream",
        });
        const liveStreamImageFile = `${prodImageDirectory}/livestream/${req?.file?.filename}`;
        if (fs.existsSync(liveStreamImageFile)) {
          fs.unlinkSync(liveStreamImageFile);
        }
      }
    } catch (error) {
      console.log(error);
      //delete uploaded livestream file if postgres fails to save record
      const liveStreamImageFile = `${prodImageDirectory}/livestream/${req?.file?.filename}`;
      if (fs.existsSync(liveStreamImageFile)) {
        fs.unlinkSync(liveStreamImageFile);
      }
      res.status(400).json({
        status: "false",
        message: error?.detail ?? "Error in saving livestream information",
      });
    }
  },

  updateLiveStreamStatus: async (req, res, next) => {
    //check if all the required deatails exist in the request
    if (!req?.params?.livestreamid || !req?.body?.status) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //Status can only be PUBLISHED
    if (req?.body?.status !== "PUBLISHED") {
      res.status(400).json({
        status: "false",
        message: "Incorrect Status Name",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        " UPDATE tllivestream " +
          "SET live_stream_status = $1  " +
          "WHERE live_stream_id = $2 RETURNING *",
        [req?.body?.status, req?.params?.livestreamid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "The livestream record has been successfully updated.",
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No record has been updated for the livestream id",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in updating livestream details",
      });
    }
  },

  getOneUpcomingLiveStreamDetails: async (req, res, next) => {
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount, rows } = await pgConnection.query(
        "select * from tllivestream where merchant_id=$1 and isdelete=false " +
          "and live_stream_status in ('UPCOMING', 'LIVE NOW', 'PUBLISHED') " +
          "and (live_stream_date + live_stream_start_time) > current_timestamp " +
          "order by live_stream_date,live_stream_start_time DESC LIMIT 1",
        [req?.params?.merchantid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "Successfully retrieved upcoming livestream details.",
          live_stream_result: rows[0],
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No upcoming livestream is found for the merchant",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving livestream details.",
      });
    }
  },

  getTopLiveStreamBasedonSales: async (req, res, next) => {
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        "select tl1.live_stream_id,tl1.live_stream_title, " +
          "tl1.live_stream_image,tl1.live_stream_count, " +
          "SUM(tl2.final_price) as total_sale, " +
          "count(tl2.live_stream_id) ::int as total_buyers " +
          "from tllivestream tl1 " +
          "inner join tltransaction tl2 " +
          "on tl1.live_stream_id = tl2.live_stream_id " +
          "inner join tllogistic tl3 " +
          "on tl2.transaction_id = tl3.transaction_id " +
          "where tl1.merchant_id = $1 " +
          "and tl3.logistic_status not in ('PENDING') " +
          "GROUP BY tl1.live_stream_id  " +
          "ORDER BY SUM(tl2.final_price) DESC " +
          "limit 3;",
        [req?.params?.merchantid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Top Live Stream Details have been retrieved",
          top_live_stream_results: rows,
        });
      } else {
        res.status(200).json({
          status: false,
          message: "No records found for top live stream",
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error in retrieving top live stream details",
      });
      return;
    }
  },

  // Member Side

  getActiveLiveStreamCount: async (req, res, next) => {
    try {
      const { rows } = await pgConnection.query(
        "SELECT a.*, (SELECT count(*) :: int " +
          "FROM tlmerchant b JOIN " +
          "tllivestream c " +
          "ON c.merchant_id = b.merchant_id " +
          "WHERE b.industry_category_id = a.industry_category_id AND c.live_stream_status = 'LIVE NOW' AND c.isdelete=false " +
          ") as count " +
          "FROM tlindustrycategory a;"
      );

      res.status(200).json({
        status: "true",
        message: "Successfully retrieved active livestream details.",
        active_live_streams_count_result: rows,
      });
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in retrieving active livestream details.",
      });
    }
  },

  getMerchantLiveStreamBasedOnCategories: async (req, res, next) => {
    //check if all the required deatails exist in the request
    if (!req?.params.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        "select tl1.*, " +
          "(SELECT COALESCE(NULLIF(json_agg(json_build_object( " +
          "'live_stream_id', tl2.live_stream_id, " +
          "'live_stream_title',tl2.live_stream_title, " +
          "'live_stream_description',tl2.live_stream_description, " +
          "'live_stream_date',tl2.live_stream_date, " +
          "'live_stream_start_time',tl2.live_stream_start_time, " +
          "'live_stream_end_time',tl2.live_stream_end_time, " +
          "'live_stream_status',tl2.live_stream_status, " +
          "'merchant_id',tl2.merchant_id, " +
          "'live_stream_image',tl2.live_stream_image, " +
          "'bambuser_id',tl2.bambuser_id, " +
          "'merchant_state', tl3.merchant_state, " +
          "'merchant_name', tl3.merchant_company_name)) " +
          "::TEXT, '[null]'), '[]')::JSON " +
          "as live_now from tllivestream tl2 " +
          "left join tlmerchant tl3 " +
          "on tl2.merchant_id = tl3.merchant_id " +
          "where tl2.live_stream_status = 'LIVE NOW' AND tl3.industry_category_id = tl1.industry_category_id " +
          "), (SELECT COALESCE(NULLIF(json_agg(json_build_object( " +
          "'live_stream_id', tl2.live_stream_id, " +
          "'live_stream_title',tl2.live_stream_title, " +
          "'live_stream_description',tl2.live_stream_description, " +
          "'live_stream_date',tl2.live_stream_date, " +
          "'live_stream_start_time',tl2.live_stream_start_time, " +
          "'live_stream_end_time',tl2.live_stream_end_time, " +
          "'live_stream_status',tl2.live_stream_status, " +
          "'merchant_id',tl2.merchant_id, " +
          "'live_stream_image',tl2.live_stream_image, " +
          "'bambuser_id',tl2.bambuser_id, " +
          "'merchant_state', tl3.merchant_state, " +
          "'merchant_name', tl3.merchant_company_name)) " +
          "::TEXT, '[null]'), '[]')::JSON " +
          "as today from tllivestream tl2 " +
          "left join tlmerchant tl3 " +
          "on tl2.merchant_id = tl3.merchant_id " +
          "where tl2.live_stream_status = 'PUBLISHED' AND tl2.live_stream_date = CURRENT_DATE AND tl3.industry_category_id = tl1.industry_category_id " +
          "),(SELECT COALESCE(NULLIF(json_agg(json_build_object( " +
          "'live_stream_id', tl2.live_stream_id, " +
          "'live_stream_title',tl2.live_stream_title, " +
          "'live_stream_description',tl2.live_stream_description, " +
          "'live_stream_date',tl2.live_stream_date, " +
          "'live_stream_start_time',tl2.live_stream_start_time, " +
          "'live_stream_end_time',tl2.live_stream_end_time, " +
          "'live_stream_status',tl2.live_stream_status, " +
          "'merchant_id',tl2.merchant_id, " +
          "'live_stream_image',tl2.live_stream_image, " +
          "'bambuser_id',tl2.bambuser_id, " +
          "'merchant_state', tl3.merchant_state, " +
          "'merchant_name', tl3.merchant_company_name)) " +
          "::TEXT, '[null]'), '[]')::JSON " +
          "as upcoming from tllivestream tl2 " +
          "left join tlmerchant tl3 " +
          "on tl2.merchant_id = tl3.merchant_id " +
          "where tl2.live_stream_status = 'PUBLISHED' AND tl2.live_stream_date > CURRENT_DATE AND tl3.industry_category_id = tl1.industry_category_id " +
          "), (SELECT COALESCE(NULLIF(json_agg(json_build_object( " +
          "'live_stream_id', tl2.live_stream_id, " +
          "'live_stream_title',tl2.live_stream_title, " +
          "'live_stream_description',tl2.live_stream_description, " +
          "'live_stream_date',tl2.live_stream_date, " +
          "'live_stream_start_time',tl2.live_stream_start_time, " +
          "'live_stream_end_time',tl2.live_stream_end_time, " +
          "'live_stream_status',tl2.live_stream_status, " +
          "'merchant_id',tl2.merchant_id, " +
          "'live_stream_image',tl2.live_stream_image, " +
          "'bambuser_id',tl2.bambuser_id, " +
          "'merchant_state', tl3.merchant_state, " +
          "'merchant_name', tl3.merchant_company_name)) " +
          "::TEXT, '[null]'), '[]')::JSON " +
          "as favourite from tllivestream tl2 " +
          "inner join tlmerchant tl3 " +
          "on tl2.merchant_id = tl3.merchant_id " +
          "inner join tlmemberfavourite tl4 " +
          "on tl4.merchant_id = tl3.merchant_id " +
          "where tl2.live_stream_status in ('LIVE NOW') AND tl3.industry_category_id = tl1.industry_category_id AND tl4.member_id = $1 " +
          ") from tlindustrycategory tl1",
        [req?.params?.memberid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "All livestream details have been retrieved",
          live_streams_category_result: rows,
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No results found for livestream",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving livestream details.",
      });
    }
  },

  getSingleLiveStreamDetailsWithProduct: async (req, res, next) => {
    if (!req?.params?.livestreamid || !req?.body?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows, rowCount } = await pgConnection.query(
        " select tl1.*, tl6.merchant_bank_name, tl6.merchant_bank_holder_name, tl6.merchant_bank_number, " +
          "(select exists(select * from tlmemberfavourite tl2 " +
          "inner join tllivestream tl3 " +
          "on tl2.merchant_id = tl3.merchant_id " +
          "where tl2.member_id = $2 AND tl3.live_stream_id = $1 ) as isFavourite), " +
          "(select json_agg(json_build_object('product_id',tl3.product_id, " +
          "'product_name', tl3.product_name, " +
          "'product_description', tl3.product_description, " +
          "'product_image', tl3.product_image, " +
          "'product_main_cat_id', tl3.product_main_cat_id, " +
          "'product_sub_cat_id', tl3.product_sub_cat_id, " +
          "'product_status', tl3.product_status, " +
          "'merchant_id', tl3.merchant_id, " +
          "'product_variation', (SELECT JSON_AGG(tl4.*) " +
          "FROM tlproductvariation tl4 " +
          "WHERE tl4.product_id = tl3.product_id))) " +
          "as products " +
          "from tlproduct tl3 " +
          "left join tllivestreamproducts tl4 " +
          "on tl3.product_id = tl4.product_id " +
          "left join tllivestream tl5 " +
          "on tl4.live_stream_id = tl5.live_stream_id " +
          "where tl5.live_stream_id = $1 and tl4.status = true) " +
          "from tllivestream tl1 " +
          "left join tlmerchant tl6 " +
          "on tl1.merchant_id = tl6.merchant_id " +
          "where tl1.live_stream_id = $1 limit 1",
        [req?.params?.livestreamid, req?.body?.memberid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: "true",
          message: "All livestream details have been retrieved",
          live_stream_detail_result: rows[0],
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "No results found for livestream",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving livestream details.",
      });
    }
  },
};
