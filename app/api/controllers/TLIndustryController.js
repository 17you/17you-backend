const pgConnection = require("../../../config/database");

module.exports = {
  getIndustryDetails: (req, res, next) => {
    pgConnection.query("SELECT * FROM tlindustrycategory", (error, results) => {

      if (error) {
        console.log(error)
        res.status(500).json({
          status: "false",
          message: "Error in retrieving industry categories",
        });
        return;
      }
      
      res.status(201).json({
        status: "true",
        message: "Successfully retrieved industry details",
        industry_list: results.rows
      });
    });
  },
};
