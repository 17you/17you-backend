const crypto = require('crypto')
var url = require('url');
const {merchantID,secretKey,algorithm} = require('../../../constants');

module.exports = {
    createManualPayment: async (req, res, next) => {
        if (!req?.body?.detail || !req?.body?.amount || !req?.body?.order_id || !req?.body?.buyer_name || !req?.body?.buyer_email || !req?.body?.buyer_phone) {
            res.status(400).json({
                status: "false",
                message: "Insufficient information provided",
            });
        } else {
            var reqHashKeys = secretKey + req.body.detail + req.body.amount + req.body.order_id;
            const hash = crypto.createHmac(algorithm, secretKey).update(reqHashKeys).digest('hex')

            try {
                url = new URL('https://sandbox.senangpay.my/payment/' + merchantID);
                url.searchParams.append('detail', req.body.detail);
                url.searchParams.append('amount', req.body.amount);
                url.searchParams.append('order_id', req.body.order_id);
                url.searchParams.append('name', req.body.buyer_name);
                url.searchParams.append('email', req.body.buyer_email);
                url.searchParams.append('phone', req.body.buyer_phone);
                url.searchParams.append('hash', hash)

                res.status(201).json({
                    status: "true",
                    message: "Order created successfully. Please click link to make payment.",
                    link: url.href,
                    order_result: {}
                });

            } catch (error) {
                console.log(error);
                res.status(400).json({
                    status: "false",
                    message: error.detail ?? "Error in creating order. Please try again later",
                });
            }
        }
    }
}