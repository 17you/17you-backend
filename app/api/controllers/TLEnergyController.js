const pgConnection = require("../../../config/database");
const updateMemberLevel = require("../helper/reusable_functions/updateMemberLevel");
const moment = require("moment");
var _ = require("lodash");

const {
  removeDuplicatesFromArray,
  countNumberofSimilarObjectsinArray,
} = require("../helper/validation");

module.exports = {
  //Member Side
  getMemberEnergyInformation: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      // Get Referral Summary Count (Total Share, Today Sign Up, Total Sign Up, Total Energy, is Today Energy Booster Redeemed)
      const { rows: referralCountResults } = await pgConnection.query(
        "select (select count(*)::int  " +
          "as total_share from tlmembershared  " +
          " where member_id = $1), " +
          "(select count(*)::int as  " +
          " total_sign_up from tlreferralmanagement  " +
          " where member_referrer_code =(select member_referral_code from  " +
          " tlreferralmanagement where member_id = $1)), " +
          " (select coalesce(sum(energy_redeemed_total),0)  " +
          " as total_energy from tlmemberenergy  " +
          " where member_id = $1) :: int,  " +
          " (select count(*)::int as " +
          " today_sign_up from tlreferralmanagement  " +
          " where member_referrer_code = (select member_referral_code from  " +
          " tlreferralmanagement where member_id = $1)  " +
          " AND member_signed_up_date_and_time :: date = CURRENT_DATE), " +
          "(select not exists (select * from tlmemberenergy  " +
          " where member_id = $1  " +
          " and energy_redeemed_date = CURRENT_DATE  " +
          " and energy_id = 'DEB' " +
          " limit 1) as is_deb_redeemable)",
        [req?.params?.memberid]
      );

      if (!referralCountResults || referralCountResults?.length <= 0) {
        res.status(201).json({
          status: "false",
          message: "No energy records have been found",
        });
        return;
      }

      //destructure the referral count results
      const {
        total_share,
        total_sign_up,
        today_sign_up,
        is_deb_redeemable,
        total_energy,
      } = referralCountResults[0];

      // If all the referral count results are 0, send the following response
      if (total_share <= 0 && today_sign_up <= 0 && total_sign_up <= 0) {
        res.status(201).json({
          status: "true",
          message: "Energy Records have been successfully retrieved",
          referral_count: {
            total_share,
            total_sign_up,
            today_sign_up,
            is_deb_redeemable,
            total_energy,
            is_ts_redeemable: false,
            is_osu_redeemable: false,
            is_tsu_redeemable: false,
            ts_redeemable_energy: 0,
            osu_redeemable_energy: 0,
            tsu_redeemable_energy: 0,
            ts_redeemable_count: 0,
            osu_redeemable_count: 0,
            tsu_redeemable_count: 0,
          },
        });
        return;
      }

      // Convert referral count results to inital energy
      const initialRedeemableTotalShareEnergy = total_share * 10;
      const initialRedeemableTodaySignUpEnergy = today_sign_up * 150;
      const initialRedeemableTotalSignUpEnergy = total_sign_up * 150;

      //These values are the final redeemable points after deducting from the already claimed points
      var redeemableTotalShareEnergy = 0;
      var redeemableTodaySignUpEnergy = 0;
      var redeemableTotalSignUpEnergy = 0;

      var isTotalShareEnergyReedemable = false,
        isTodaySignUpEnergyRedeemable = false,
        isTotalSignUpEnergyRedeemable = false;

      // get Already Redeemed Energy (Total Sign Up, Total Share, Today Sign Up)
      const { rows: redeemedEnergyCounts } = await pgConnection.query(
        "select(select coalesce((select energy_redeemed_total " +
          "from tlmemberenergy " +
          "where energy_id = $2 " +
          "and member_id = $1 " +
          "limit 1), 0) as redeemed_total_share_energy), " +
          "(select coalesce(sum(energy_redeemed_total),0) " +
          "as redeemed_today_sign_up_energy from tlmemberenergy " +
          "where member_id = $1 " +
          "and energy_redeemed_date = CURRENT_DATE " +
          "and energy_id = $3) :: int, " +
          "(select coalesce(sum(energy_redeemed_total),0) " +
          "as redeemed_total_sign_up_energy from tlmemberenergy " +
          "where member_id = $1 " +
          "and energy_id = $4) :: int",
        [req?.params?.memberid, "TS", "TSU", "TSU"]
      );

      if (!redeemedEnergyCounts || redeemedEnergyCounts?.length <= 0) {
        res.status(201).json({
          status: "false",
          message: "Something went wrong. Please try again. ",
        });
        return;
      }

      //destructure the results and convert to redeemed points
      const {
        redeemed_total_share_energy,
        redeemed_today_sign_up_energy,
        redeemed_total_sign_up_energy,
      } = redeemedEnergyCounts[0];

      //if the redeemable points are greater than already redeemed points, then user can claim those additional points
      if (initialRedeemableTodaySignUpEnergy > redeemed_today_sign_up_energy) {
        isTodaySignUpEnergyRedeemable = true;
        redeemableTodaySignUpEnergy =
          initialRedeemableTodaySignUpEnergy - redeemed_today_sign_up_energy;
      }

      if (initialRedeemableTotalShareEnergy > redeemed_total_share_energy) {
        isTotalShareEnergyReedemable = true;
        redeemableTotalShareEnergy =
          initialRedeemableTotalShareEnergy - redeemed_total_share_energy;
      }

      if (initialRedeemableTotalSignUpEnergy > redeemed_total_sign_up_energy) {
        isTotalSignUpEnergyRedeemable = true;
        redeemableTotalSignUpEnergy =
          initialRedeemableTotalSignUpEnergy - redeemed_total_sign_up_energy;
      }

      res.status(201).json({
        status: "true",
        message: "Energy Records have been successfully retrieved",
        referral_count: {
          total_share,
          total_sign_up,
          today_sign_up,
          is_deb_redeemable,
          total_energy,
          is_ts_redeemable: isTotalShareEnergyReedemable,
          is_osu_redeemable: isTotalSignUpEnergyRedeemable,
          is_tsu_redeemable: isTodaySignUpEnergyRedeemable,
          ts_redeemable_energy: redeemableTotalShareEnergy,
          osu_redeemable_energy: redeemableTotalSignUpEnergy,
          tsu_redeemable_energy: redeemableTodaySignUpEnergy,
          ts_redeemable_count: redeemableTotalShareEnergy / 10,
          osu_redeemable_count: redeemableTotalSignUpEnergy / 150,
          tsu_redeemable_count: redeemableTodaySignUpEnergy / 150,
        },
      });

      return;
    } catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving energy records.",
      });
    }
  },

  redeemDailyEnergyBooster: async (req, res, next) => {
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows: isDEBNotRedeemable } = await pgConnection.query(
        "select exists (select * from tlmemberenergy " +
          "where member_id = $1 " +
          "and energy_redeemed_date = CURRENT_DATE " +
          "and energy_id = $2 " +
          "limit 1) as is_deb_not_redeemable ",
        [req?.params?.memberid, "DEB"]
      );

      if (isDEBNotRedeemable[0]?.is_deb_not_redeemable == true) {
        res.status(200).json({
          status: false,
          message:
            "You have already redeemed Daily Energy Booster. Please try again. ",
        });
        return;
      }

      //generate a random number from 1-200 for Daily Energy Booster
      const redeemedEnergyBooster = Math.floor(Math.random() * 200) + 1;

      const { rowCount: insertedRecordRowCount } = await pgConnection.query(
        "insert into tlmemberenergy(member_id,energy_id,energy_redeemed_total) " +
          "values ($1,$2,$3) RETURNING *;",
        [req?.params?.memberid, "DEB", redeemedEnergyBooster]
      );

      if (insertedRecordRowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Daily Energy Booster has been successfully redeemed",
          deb_redeemed_energy: redeemedEnergyBooster,
        });
        updateMemberLevel(req?.params?.memberid);
      } else {
        res.status(200).json({
          status: false,
          message: "No Daily Energy Booster has been redeemed",
        });
      }

      return;
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error when redeeming Daily Energy Booster",
      });
    }
  },

  redeemOverallSignUp: async (req, res, next) => {
    //Check if All the Required Details are Available
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Get total referral count
      const { rowCount: totalReferralCountRowCount, rows: totalReferralCount } =
        await pgConnection.query(
          "select count(*)::int as  " +
            "total_sign_up_count from tlreferralmanagement  " +
            "where member_referrer_code =(select member_referral_code from  " +
            "tlreferralmanagement where member_id = $1) ",
          [req?.params?.memberid]
        );

      //convert referral count to redeemable sign up energy
      const redeemableTotalSignUpEnergy =
        totalReferralCount[0]?.total_sign_up_count * 150;

      //if redeemable total Sign Up Energy is less than 0, then user cannot redeem
      if (
        totalReferralCountRowCount <= 0 ||
        totalReferralCount[0]?.total_sign_up_count <= 0
      ) {
        res.status(200).json({
          status: false,
          message: "There are no redeemable energy for total sign up",
        });
        return;
      }

      //Get the total sign up energy points that have already been redeemed
      const {
        rowCount: currentlyRedeemedEnergyRowCount,
        rows: currentlyRedeemedEnergy,
      } = await pgConnection.query(
        "(select coalesce(sum(energy_redeemed_total),0) " +
          "as redeemed_total_sign_up_energy from tlmemberenergy " +
          "where member_id = $2 " +
          "and energy_id = $1)",
        ["TSU", req?.params?.memberid]
      );

      //If redeemable total sign up energy is more than current redeemed energy,
      // insert a new record

      if (
        redeemableTotalSignUpEnergy >
        currentlyRedeemedEnergy[0]?.redeemed_total_sign_up_energy
      ) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "insert into tlmemberenergy(member_id,energy_id,energy_redeemed_total) " +
            "values ($1,$2,$3) returning *",
          [
            req?.params?.memberid,
            "TSU",
            redeemableTotalSignUpEnergy -
              currentlyRedeemedEnergy[0]?.redeemed_total_sign_up_energy,
          ]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message:
              "You have successfully redeemed total sign up energy points",
          });
          updateMemberLevel(req?.params?.memberid);
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
      }

      //If redeemable total sign up energy is less than current redeemed energy,
      // means the user has already redeemed the redeemable energy
      else {
        res.status(200).json({
          status: false,
          message: "You have already redeemed the energy",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error when redeeming Total Sign Up",
      });
    }
  },

  redeemTodaySignUp: async (req, res, next) => {
    //Check if All the Required Details are Available
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Get today's referral count
      const { rowCount: todayReferralCountRowCount, rows: todayReferralCount } =
        await pgConnection.query(
          "select count(*)::int as  " +
            "today_sign_up_count from tlreferralmanagement  " +
            "where member_referrer_code =(select member_referral_code from  " +
            "tlreferralmanagement where member_id = $1)  " +
            "AND member_signed_up_date_and_time :: date = CURRENT_DATE",
          [req?.params?.memberid]
        );

      //convert referral count to redeemable sign up energy
      const redeemableTodaySignUpEnergy =
        todayReferralCount[0]?.today_sign_up_count * 150;
      //if redeemable Today Sign Up Energy is less than 0, then user cannot redeem
      if (
        todayReferralCountRowCount <= 0 ||
        todayReferralCount[0]?.today_sign_up_count <= 0
      ) {
        res.status(200).json({
          status: false,
          message: "There are no redeemable energy for today sign up",
        });
        return;
      }

      //Get the today sign up energy points that have already been redeemed
      const {
        rowCount: currentlyRedeemedEnergyRowCount,
        rows: currentlyRedeemedEnergy,
      } = await pgConnection.query(
        "(select coalesce(sum(energy_redeemed_total),0) " +
          "as redeemed_today_sign_up_energy from tlmemberenergy " +
          "where member_id = $2 " +
          "and energy_redeemed_date = CURRENT_DATE " +
          "and energy_id = $1)",
        ["TSU", req?.params?.memberid]
      );

      //If redeemable today sign up energy is more than current redeemed energy,
      // insert a new record with the remaining redeemable energy

      if (
        redeemableTodaySignUpEnergy >
        currentlyRedeemedEnergy[0]?.redeemed_today_sign_up_energy
      ) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "insert into tlmemberenergy(member_id,energy_id,energy_redeemed_total) " +
            "values ($1,$2,$3) returning *",
          [
            req?.params?.memberid,
            "TSU",
            redeemableTodaySignUpEnergy -
              currentlyRedeemedEnergy[0]?.redeemed_today_sign_up_energy,
          ]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message:
              "You have successfully redeemed today sign up energy points",
          });
          updateMemberLevel(req?.params?.memberid);
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
      }

      //If redeemable today sign up energy is less than current redeemed energy,
      // means the user has already redeemed the redeemable energy
      else {
        res.status(200).json({
          status: false,
          message: "You have already redeemed the energy",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error when redeeming Total Sign Up",
      });
    }
  },

  redeemTotalShare: async (req, res, next) => {
    //Check if All the Required Details are Available
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Get total share count
      const { rowCount: totalShareCountRowCount, rows: totalShareCount } =
        await pgConnection.query(
          "select count(*)::int as total_share from tlmembershared " +
            "where member_id =$1 ",
          [req?.params?.memberid]
        );

      //convert share count to redeemable share energy
      const redeemableTotalShareEnergy = totalShareCount[0]?.total_share * 10;

      //if redeemable total Share Energy is less than 0, then user cannot redeem
      if (
        totalShareCountRowCount <= 0 ||
        totalShareCount[0]?.total_share <= 0
      ) {
        res.status(200).json({
          status: false,
          message: "There are no redeemable energy for total share",
        });
        return;
      }

      //Get the total share energy points that have already been redeemed
      const {
        rowCount: currentlyRedeemedEnergyRowCount,
        rows: currentlyRedeemedEnergy,
      } = await pgConnection.query(
        "select energy_redeemed_total " +
          "from tlmemberenergy " +
          "where energy_id = $1 " +
          "and member_id = $2 " +
          "limit 1",
        ["TS", req?.params?.memberid]
      );

      //If there is no redeemed total share energy points, insert redeemable points for the user
      if (currentlyRedeemedEnergyRowCount <= 0) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "insert into tlmemberenergy(member_id,energy_id,energy_redeemed_total) " +
            "values ($1,$2,$3) returning *",
          [req?.params?.memberid, "TS", redeemableTotalShareEnergy]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message: "You have successfully redeemed total share energy points",
          });
          updateMemberLevel(req?.params?.memberid);
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
        return;
      }

      //If redeemable total share energy is more than current redeemed energy,
      // update current redeemed energy with the new value

      if (
        redeemableTotalShareEnergy >
        currentlyRedeemedEnergy[0]?.energy_redeemed_total
      ) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "update tlmemberenergy set " +
            "energy_redeemed_total = $3, " +
            "energy_redeemed_date = CURRENT_DATE " +
            "where energy_id = $2 " +
            "and member_id = $1",
          [req?.params?.memberid, "TS", redeemableTotalShareEnergy]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message: "You have successfully redeemed total share energy points",
          });
          updateMemberLevel(req?.params?.memberid);
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
      }

      //If redeemable total share energy is less than current redeemed energy,
      // means the user has already redeemed the redeemable energy
      else {
        res.status(200).json({
          status: false,
          message: "You have already redeemed the energy",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error when redeeming Total Share Energy.",
      });
    }
  },

  showSummarisedLevel: async (req, res, next) => {
    //Check if member id is provided
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }

    try{
    //get all the level details
    const { rows: memberLevel, rowCount: memberLevelRowCount } =
      await pgConnection.query(
        "select tl1.energy_level, tl1.date_completed ,tl2.energy_level_limit " +
          "from tlenergymemberlevel tl1  " +
          "inner join tlenergylevel tl2 " +
          "on tl1.energy_level = tl2.energy_level " +
          "where tl1.member_id = $1 " +
          "order by tl1.energy_level",
        [req?.params?.memberid]
      );


    if (memberLevelRowCount <= 0) {
      res.status(200).json({
        status: true,
        message: "No level found for the user.",
      });
      return;
    }

    //get member's referral code & sign up date and time
    const { rows: memberInfo, rowCount: memberInfoCount } =
      await pgConnection.query(
        "select member_signed_up_date_and_time, " +
          "member_referral_code from tlreferralmanagement where member_id = $1 limit 1",
        [req?.params?.memberid]
      );

    if (memberInfoCount <= 0) {
      res.status(200).json({
        status: true,
        message: "Required Information is not found",
      });
      return;
    }

    //construct query to get referral_email_addresses based on the level achieved
    var getReferralInfoQuery = "";
    for (var i = 0; i < memberLevel.length; i++) {
      getReferralInfoQuery =
        getReferralInfoQuery +
        `${i === 0 ? "" : "union all "} ` +
        `select member_id, member_signed_up_date_and_time :: date, ${memberLevel[i]?.energy_level} as level ` +
        `from tlreferralmanagement ` +
        `where member_signed_up_date_and_time :: date >= '${
          i === 0
            ? moment(memberInfo[0].member_signed_up_date_and_time).format(
                "YYYY-MM-DD"
              )
            : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD")
        }' :: date ` +
        `and member_signed_up_date_and_time :: date <= '${moment(
          memberLevel[i]?.date_completed
        ).format("YYYY-MM-DD")}' ::date ` +
        `and member_referrer_code = '${memberInfo[0].member_referral_code}' ` +
        ``;
    }

    //get member's referral info based on levels and date completed
    const { rows: memberReferralInfo, rowCount: memberReferralInfoCount } =
      await pgConnection.query(getReferralInfoQuery);

    //remove duplicated records based on member_id
    const newMemberReferralInfo = removeDuplicatesFromArray(
      memberReferralInfo,
      "member_id"
    );

    //count number of members based on the level achieved
    const newMemberReferralInfoCountBasedOnLevel =
      countNumberofSimilarObjectsinArray(newMemberReferralInfo);

    // construct a new array with the level details and number of referrals for the respective levels
    var finalMemberLevelResults = [];
    for (var i = 0; i < memberLevel.length; i++) {
      var finalMemberLevelObject = {};
      finalMemberLevelObject["level_points"] =
        memberLevel[i]?.energy_level_limit;
      finalMemberLevelObject["level"] = memberLevel[i]?.energy_level;
      finalMemberLevelObject["start_date"] =
        i === 0
          ? moment(memberInfo[0].member_signed_up_date_and_time).format(
              "YYYY-MM-DD"
            )
          : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD");

      finalMemberLevelObject["end_date"] = moment(
        memberLevel[i]?.date_completed
      ).format("YYYY-MM-DD");
      var hasReferralCount = false;
      for (var j = 0; j < newMemberReferralInfoCountBasedOnLevel.length; j++) {
        if (
          newMemberReferralInfoCountBasedOnLevel[j].level ===
          memberLevel[i].energy_level.toString()
        ) {
          hasReferralCount = true;
          finalMemberLevelObject["referral_count"] =
            newMemberReferralInfoCountBasedOnLevel[j]?.count;
          break;
        }
      }
      if (!hasReferralCount) {
        finalMemberLevelObject["referral_count"] = 0;
      }
      hasReferralCount = false;
      finalMemberLevelResults.push(finalMemberLevelObject);
    }

    res.status(200).json({
      status: true,
      message: "Level details have been successfully retrieved.",
      level_results: finalMemberLevelResults.reverse(),
    });
    return;
  }
  catch(error){
    res.status(400).json({
      status: true,
      message: "Error is retrieving information",
    });
  }
  },

  showDetailedLevel: async (req, res, next) => {
    //Check if member id is provided
    if (!req?.params?.memberid) {
      res.status(401).json({
        status: false,
        message: "Insufficient information provided",
      });
      return;
    }
    try {
      //get all the level details
      const { rows: memberLevel, rowCount: memberLevelRowCount } =
        await pgConnection.query(
          "select tl1.energy_level, tl1.date_completed ,tl2.energy_level_limit " +
            "from tlenergymemberlevel tl1  " +
            "inner join tlenergylevel tl2 " +
            "on tl1.energy_level = tl2.energy_level " +
            "where tl1.member_id = $1 " +
            "order by tl1.energy_level",
          [req?.params?.memberid]
        );

      if (memberLevelRowCount <= 0) {
        res.status(200).json({
          status: true,
          message: "No level found for the user.",
        });
        return;
      }

      //get member's referral code & sign up date and time
      const { rows: memberInfo, rowCount: memberInfoCount } =
        await pgConnection.query(
          "select member_signed_up_date_and_time, " +
            "member_referral_code from tlreferralmanagement where member_id = $1 limit 1",
          [req?.params?.memberid]
        );

      if (memberInfoCount <= 0) {
        res.status(200).json({
          status: true,
          message: "Required Information is not found",
        });
        return;
      }

      //construct query to get referral_email_addresses based on the level achieved
      //construct query to get total_daily_energy_booster (DEB) based on the level achieved
      var getReferralInfoQuery = "";
      var getTotalDEBQuery = "";
      var getTotalRewardsQuery = "";

      var incompleteLevelStartDate = "";
      var incompleteLevelEndDate = "";
      var incompleteLevelNumber = "";
      for (var i = 0; i < memberLevel.length; i++) {
        getTotalDEBQuery =
          getTotalDEBQuery +
          `${i === 0 ? "" : "union all "} 
          select ${memberLevel[i]?.energy_level} as level, 
          coalesce(sum(energy_redeemed_total),0) as total_daily_energy 
          from tlmemberenergy 
          where member_id = ${req?.params?.memberid} 
          and energy_id = 'DEB' 
          and energy_redeemed_date :: date ${i === 0 ? `>=` : `>`} '${
            i === 0
              ? moment(memberInfo[0].member_signed_up_date_and_time).format(
                  "YYYY-MM-DD"
                )
              : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD")
          }' :: date 
    
          and energy_redeemed_date :: date <= '${moment(
            memberLevel[i]?.date_completed
          ).format("YYYY-MM-DD")}' ::date `;


          getTotalRewardsQuery =
          getTotalRewardsQuery +
          `${i === 0 ? "" : "union all "} 
          select ${memberLevel[i]?.energy_level} as level, 
          coalesce(sum(energy_redeemed_total),0) as total_reward_energy 
          from tlmemberenergy 
          where member_id = ${req?.params?.memberid} 
          and energy_id = 'REB' 
          and energy_redeemed_date :: date ${i === 0 ? `>=` : `>`} '${
            i === 0
              ? moment(memberInfo[0].member_signed_up_date_and_time).format(
                  "YYYY-MM-DD"
                )
              : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD")
          }' :: date 
    
          and energy_redeemed_date :: date <= '${moment(
            memberLevel[i]?.date_completed
          ).format("YYYY-MM-DD")}' ::date `;



        getReferralInfoQuery =
          getReferralInfoQuery +
          `${i === 0 ? "" : "union all "} ` +
          `select 150 as energy, tl1.member_id, mask_email(tl2.member_email), tl1.member_signed_up_date_and_time :: date :: text, ${memberLevel[i]?.energy_level} as level ` +
          `from tlreferralmanagement tl1 inner join tlmember tl2 on tl1.member_id=tl2.member_id ` +
          `where tl1.member_signed_up_date_and_time :: date >= '${
            i === 0
              ? moment(memberInfo[0].member_signed_up_date_and_time).format(
                  "YYYY-MM-DD"
                )
              : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD")
          }' :: date ` +
          `and tl1.member_signed_up_date_and_time :: date <= '${moment(
            memberLevel[i]?.date_completed
          ).format("YYYY-MM-DD")}' ::date ` +
          `and tl1.member_referrer_code = '${memberInfo[0].member_referral_code}' ` +
          ``;

        //retrieve records for current level which is not completed.
        if (i === memberLevel.length - 1) {
          getTotalDEBQuery =
            getTotalDEBQuery +
            `union all 
            select ${parseInt(memberLevel[i]?.energy_level) + 1} as level, 
            coalesce(sum(energy_redeemed_total),0) as total_daily_energy 
            from tlmemberenergy 
            where member_id = ${req?.params?.memberid} 
            and energy_id = 'DEB' 
            and energy_redeemed_date :: date > '${moment(
              memberLevel[i - 1]?.date_completed
            ).format("YYYY-MM-DD")}' :: date 
            and energy_redeemed_date :: date <= current_date `;


            getTotalRewardsQuery =
            getTotalRewardsQuery +
            `union all 
            select ${parseInt(memberLevel[i]?.energy_level) + 1} as level, 
            coalesce(sum(energy_redeemed_total),0) as total_reward_energy 
            from tlmemberenergy 
            where member_id = ${req?.params?.memberid} 
            and energy_id = 'REB' 
            and energy_redeemed_date :: date > '${moment(
              memberLevel[i - 1]?.date_completed
            ).format("YYYY-MM-DD")}' :: date 
            and energy_redeemed_date :: date <= current_date `;


          getReferralInfoQuery =
            getReferralInfoQuery +
            `union all ` +
            ` select 150 as energy, tl1.member_id,mask_email(tl2.member_email), tl1.member_signed_up_date_and_time :: date :: text, ${
              parseInt(memberLevel[i]?.energy_level) + 1
            } as level ` +
            `from tlreferralmanagement tl1 inner join tlmember tl2 on tl1.member_id=tl2.member_id ` +
            `where tl1.member_signed_up_date_and_time :: date >= '${moment(
              memberLevel[i]?.date_completed
            ).format("YYYY-MM-DD")}' :: date ` +
            `and tl1.member_signed_up_date_and_time :: date <= current_date ` +
            `and tl1.member_referrer_code = '${memberInfo[0].member_referral_code}' `;

          incompleteLevelStartDate = moment(
            memberLevel[i - 1]?.date_completed
          ).format("YYYY-MM-DD");
          incompleteLevelEndDate = moment().format("YYYY-MM-DD");
          incompleteLevelNumber = parseInt(memberLevel[i]?.energy_level) + 1;
        }
      }

      //get member's referral info based on levels and date completed
      const { rows: memberReferralInfo, rowCount: memberReferralInfoCount } =
      await pgConnection.query(getReferralInfoQuery);

      //get member's total daily energy booster  based on levels and date completed
      const { rows: memberDEBInfo, rowCount: memberDEBInfoCount } =
      await pgConnection.query(getTotalDEBQuery);

      //get member's total reward energy booster  based on levels and date completed
      const { rows: memberRewardsInfo, rowCount: memberRewardsInfoCount } =
      await pgConnection.query(getTotalRewardsQuery);

      //remove duplicated records based on member_id
      const newMemberReferralInfo = removeDuplicatesFromArray(
        memberReferralInfo,
        "member_id"
      );

      const groupedReferralInfo = _.groupBy(
        newMemberReferralInfo,
        (memberReferralSingleInfo) => memberReferralSingleInfo.level
      );

      // construct a new array with the level details and number of referrals for the respective levels
      var finalMemberLevelResults = [];
      for (var i = 0; i < memberLevel.length; i++) {
        //set Values from MemberLevel
        var finalMemberLevelObject = {};
        finalMemberLevelObject["is_completed"] = true;
        finalMemberLevelObject["level_points"] =
          memberLevel[i]?.energy_level_limit;
        finalMemberLevelObject["level"] = memberLevel[i]?.energy_level;
        finalMemberLevelObject["start_date"] =
          i === 0
            ? moment(memberInfo[0].member_signed_up_date_and_time).format(
                "YYYY-MM-DD"
              )
            : moment(memberLevel[i - 1]?.date_completed).format("YYYY-MM-DD");

        finalMemberLevelObject["end_date"] = moment(
          memberLevel[i]?.date_completed
        ).format("YYYY-MM-DD");

        //set Values from memberDEBInfo
        for (var j = 0; j < memberDEBInfo.length; j++) {
          if (
            parseInt(memberDEBInfo[j].level) ===
            parseInt(memberLevel[i].energy_level)
          ) {
            finalMemberLevelObject["total_daily_reward"] =
              memberDEBInfo[j].total_daily_energy;
            break;
          }
        }

        for (var j = 0; j < memberRewardsInfo.length; j++) {
          if (
            parseInt(memberRewardsInfo[j].level) ===
            parseInt(memberLevel[i].energy_level)
          ) {
            finalMemberLevelObject["top_ranking_reward"] =
            memberRewardsInfo[j].total_reward_energy;
            break;
          }
        }

        var hasReferralValues = false;
        Object.keys(groupedReferralInfo).forEach((e) => {
          if (parseInt(memberLevel[i].energy_level) === parseInt(e)) {
            finalMemberLevelObject["referral_content"] = groupedReferralInfo[e];
            hasReferralValues = true;
          }
        });

        if (!hasReferralValues) {
          finalMemberLevelObject["referral_content"] = [];
        }
        finalMemberLevelResults.push(finalMemberLevelObject);

        //Create a New Object for Incomplete Level
        if (i === memberLevel.length - 1) {
          var inCompleteMemberLevelObject = {};
          inCompleteMemberLevelObject["is_completed"] = false;
          inCompleteMemberLevelObject["start_date"] = incompleteLevelStartDate;
          inCompleteMemberLevelObject["end_date"] = incompleteLevelEndDate;
          inCompleteMemberLevelObject["level"] = incompleteLevelNumber;

          for (var j = 0; j < memberDEBInfo.length; j++) {
            if (
              parseInt(memberDEBInfo[j].level) ===
              parseInt(memberLevel[i].energy_level) + 1
            ) {
              inCompleteMemberLevelObject["total_daily_reward"] =
                memberDEBInfo[j].total_daily_energy;
              break;
            }
          }

          for (var j = 0; j < memberRewardsInfo.length; j++) {
            if (
              parseInt(memberRewardsInfo[j].level) ===
              parseInt(memberLevel[i].energy_level) + 1
            ) {
              inCompleteMemberLevelObject["top_ranking_reward"] =
              memberRewardsInfo[j].total_reward_energy;
              break;
            }
          }


          var hasReferralValuesforIncompleteLevel = false;
          Object.keys(groupedReferralInfo).forEach((e) => {
            if (parseInt(memberLevel[i].energy_level) + 1 === parseInt(e)) {
              inCompleteMemberLevelObject["referral_content"] =
                groupedReferralInfo[e];
              hasReferralValuesforIncompleteLevel = true;
            }
          });

          if (!hasReferralValuesforIncompleteLevel) {
            inCompleteMemberLevelObject["referral_content"] = [];
          }

          const { rows, rowCount } = await pgConnection.query(
            "select energy_level_limit from tlenergylevel where energy_level = $1 limit 1",
            [parseInt(memberLevel[i].energy_level) + 1]
          );

          if (rowCount > 0) {
            inCompleteMemberLevelObject["level_points"] =
              rows[0].energy_level_limit;
          }

          finalMemberLevelResults.push(inCompleteMemberLevelObject);
        }
      }

      res.status(200).json({
        status: true,
        message: "Level details have been successfully retrieved.",
        level_results: finalMemberLevelResults.reverse(),
      });
      return;
    } catch (error) {
      res.status(400).json({
        status: true,
        message: "Error is retrieving information",
      });
    }
  },

  //Merchant Side

  /*oldGetMerchantEnergyInformation: async (req, res, next) => {
    if (!req?.params?.merchantid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows: availableLevels, rowCount: availableLevelRowCount } =
        await pgConnection.query("select * from tlenergylevel");

      if (availableLevelRowCount <= 0) {
        res.status(500).json({
          status: false,
          message: "Something went wrong. Please try again ",
        });
        return;
      }

      //Get total livestream count
      const { rowCount: totalLiveStreamRowCount, rows: totalLiveStreamCount } =
        await pgConnection.query(
          "select count(*) :: int as total_live_stream  " +
            "from tllivestream  " +
            "	where live_stream_status " +
            "in ('FINISHED','LIVE NOW')  " +
            "	and merchant_id = $1",
          [req?.params?.merchantid]
        );

      if (!totalLiveStreamCount || totalLiveStreamCount?.length <= 0) {
        res.status(200).json({
          status: true,
          message: "Merchant Energy Information has been retrieved",
          merchant_energy_info: {
            total_energy: 0,
            total_live_stream: 0,
            is_mls_redeemable: false,
            mls_redeemable_energy: 0,
            mls_redeemable_count: 0,
          },
          energy_level_results: availableLevels,
        });
        return;
      }

      //destructure the live stream count results
      const { total_live_stream } = totalLiveStreamCount[0];

      // If total live stream count is 0, send the following response
      if (total_live_stream <= 0) {
        res.status(200).json({
          status: true,
          message: "Merchant Energy Information has been retrieved",
          merchant_energy_info: {
            total_energy: 0,
            total_live_stream,
            is_mls_redeemable: false,
            mls_redeemable_energy: 0,
            mls_redeemable_count: 0,
          },
          energy_level_results: availableLevels,
        });
        return;
      }

      // Convert total livestream count results to inital energy
      const initialRedeemableLiveStreamEnergy = total_live_stream * 100;

      //This is the final redeemable energy after deducting from the already claimed energy
      var redeemableTotalLiveStreamEnergy = 0;

      var isTotalLiveStreamEnergyReedemable = false;

      // get Already Redeemed Energy (Live Stream)
      const { rows: redeemedEnergyCounts } = await pgConnection.query(
        " select coalesce((select energy_redeemed_total " +
          "  from tlmerchantenergy " +
          "  where energy_id = $2 " +
          "  and merchant_id = $1 " +
          "  limit 1), 0) as redeemed_live_stream_energy",
        [req?.params?.merchantid, "MLS"]
      );

      if (!redeemedEnergyCounts || redeemedEnergyCounts?.length <= 0) {
        res.status(201).json({
          status: "false",
          message: "Something went wrong. Please try again. ",
        });
        return;
      }

      //destructure the results and convert to redeemed points
      const { redeemed_live_stream_energy } = redeemedEnergyCounts[0];

      //if the redeemable points are greater than already redeemed points, then user can claim those additional points
      if (initialRedeemableLiveStreamEnergy > redeemed_live_stream_energy) {
        isTotalLiveStreamEnergyReedemable = true;
        redeemableTotalLiveStreamEnergy =
          initialRedeemableLiveStreamEnergy - redeemed_live_stream_energy;
      }

      res.status(200).json({
        status: true,
        message: "Merchant Energy Information has been retrieved",
        merchant_energy_info: {
          total_energy : redeemed_live_stream_energy,
          total_live_stream,
          is_mls_redeemable: isTotalLiveStreamEnergyReedemable,
          mls_redeemable_energy: redeemableTotalLiveStreamEnergy,
          mls_redeemable_count: redeemableTotalLiveStreamEnergy / 100,
        },
        energy_level_results: availableLevels,
      });

      return;
    } catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving energy records.",
      });
    }
  },
  */

  getMerchantEnergyInformation: async (req, res, next) => {
    if (!req?.params?.merchantid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rows: availableLevels, rowCount: availableLevelRowCount } =
        await pgConnection.query("select * from tlenergylevel");

      if (availableLevelRowCount <= 0) {
        res.status(500).json({
          status: false,
          message: "Something went wrong. Please try again ",
        });
        return;
      }

      //Get total livestream count and total energy
      const { rowCount: totalRowCount, rows: totalEnergyAndLive } =
        await pgConnection.query(
          "select (select count(*)::int " +
            "as total_live_stream from tllivestream  " +
            " where merchant_id = $1 AND isdelete = false), " +
            " (select coalesce(sum(energy_redeemed_total),0) " +
            " as total_energy from tlmerchantenergy " +
            " where merchant_id = $1) :: int limit 1 ",
          [req?.params?.merchantid]
        );
      var totalEnergy = 0,
        totalLiveStream = 0;
      if (totalRowCount > 0) {
        totalEnergy = totalEnergyAndLive[0]?.total_energy;
        totalLiveStream = totalEnergyAndLive[0]?.total_live_stream;
      }

      res.status(200).json({
        status: true,
        message: "Merchant Energy Information has been retrieved",
        merchant_energy_info: {
          total_energy: totalEnergy,
          total_live_stream: totalLiveStream,
        },
        energy_level_results: availableLevels,
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        status: "false",
        message: "Error in retrieving energy records.",
      });
    }
  },

  //Currently not being used because the energy is being claimed right after the merchant starts livestram
  redeemMerchantEnergy: async (req, res, next) => {
    //Check if All the Required Details are Available
    if (!req?.params?.merchantid) {
      res.status(401).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Get total livestream count
      const { rowCount: totalLiveStreamRowCount, rows: totalLiveStreamCount } =
        await pgConnection.query(
          "select count(*) :: int as total_live_stream  " +
            "from tllivestream  " +
            "	where live_stream_status " +
            "in ('FINISHED','LIVE NOW')  " +
            "	and merchant_id = $1",
          [req?.params?.merchantid]
        );

      //convert live_stream_count to redeemable merchant live_stream_energy
      const redeemableLiveStreamEnergy =
        totalLiveStreamCount[0]?.total_live_stream * 100;
      //if redeemable Live Stream Energy is less than 0, then user cannot redeem
      if (
        totalLiveStreamRowCount <= 0 ||
        totalLiveStreamCount[0]?.total_live_stream <= 0
      ) {
        res.status(200).json({
          status: false,
          message: "There are no redeemable energy for live stream",
        });
        return;
      }

      //Get the live stream energy points that have already been redeemed
      const {
        rowCount: currentlyRedeemedEnergyRowCount,
        rows: currentlyRedeemedEnergy,
      } = await pgConnection.query(
        "select energy_redeemed_total " +
          "from tlmerchantenergy " +
          "where energy_id = $1 " +
          "and merchant_id =  $2 " +
          "limit 1",
        ["MLS", req?.params?.merchantid]
      );

      //If there is no redeemed live stream energy, insert redeemable points for the merchant
      if (currentlyRedeemedEnergyRowCount <= 0) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "insert into tlmerchantenergy(merchant_id,energy_id,energy_redeemed_total) " +
            "values ($1,$2,$3) returning *",
          [req?.params?.merchantid, "MLS", redeemableLiveStreamEnergy]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message: "You have successfully redeemed live stream energy points",
          });
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
        return;
      }

      //If redeemable live stream energy is more than current redeemed energy,
      // update current redeemed energy with the new value

      if (
        redeemableLiveStreamEnergy >
        currentlyRedeemedEnergy[0]?.energy_redeemed_total
      ) {
        const { rowCount: redeemedRowCount } = await pgConnection.query(
          "update tlmerchantenergy set " +
            "energy_redeemed_total = $3 " +
            "where energy_id = $2 " +
            "and merchant_id = $1 ",
          [req?.params?.merchantid, "MLS", redeemableLiveStreamEnergy]
        );

        if (redeemedRowCount > 0) {
          res.status(200).json({
            status: true,
            message: "You have successfully redeemed live stream energy points",
          });
        } else {
          res.status(200).json({
            status: false,
            message:
              "An error occured when redeeming points. Please try again. ",
          });
        }
      }

      //If redeemable live stream energy is less than current redeemed energy,
      // means the merchant has already redeemed the redeemable energy
      else {
        res.status(200).json({
          status: false,
          message: "You have already redeemed the energy",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: false,
        message: "Error when redeeming Live Stream Energy",
      });
    }
  },
};
