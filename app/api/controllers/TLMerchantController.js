const pgConnection = require("../../../config/database");
const uploadUserIC = require("../helper/upload_images/uploadUserIC");
const uploadUserSelfie = require("../helper/upload_images/uploadUserSelfie");
const uploadLiveStreamReceipt = require("../helper/upload_images/uploadLiveSubReceiptImage");
const uploadMerchantProfileImage = require("../helper/upload_images/uploadMerchantProfilePicture");
const { prodImageDirectory, devImageDirectory } = require("../../../constants");
var fs = require("fs");

module.exports = {
  registerNewMerchantIC: async (req, res) => {
    //check if memberid presents in request parameter
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided1",
      });
      return;
    }

    // check if the member exists in the member table based on member id
    try {
      const isMemberExist = await pgConnection.query(
        "select count(*) from tlmember where member_id = $1",
        [req?.params?.memberid]
      );

      if (isMemberExist.rows[0].count <= 0) {
        throw error;
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "The member does not exist",
      });
      return;
    }

    // upload user IC front and back
    try {
      await uploadUserIC(req, res);
      if (
        !req?.files?.file_merchant_ic_back_image ||
        !req?.files?.file_merchant_ic_front_image
      ) {
        const dir = `${prodImageDirectory}/members/${req?.params?.memberid}`;
        if (fs.existsSync(dir)) {
          fs.rmdirSync(dir, { recursive: true });
        }

        throw "Insufficient information provided.";
      }
    } catch (error) {
      console.log(error);
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }
      res.status(400).json({
        status: "false",
        message: `Error when trying upload to upload files: ${error}`,
      });
      return;
    }

    //construct URL based on the uploaded ICFront file
    const icFrontUrl =
      req.protocol +
      "://" +
      req.get("host") +
      "/members/" +
      req?.params?.memberid +
      "/" +
      req?.files?.file_merchant_ic_front_image[0].filename;

    //construct URL based on the uploaded ICBack file
    const icBackUrl =
      req.protocol +
      "://" +
      req.get("host") +
      "/members/" +
      req?.params?.memberid +
      "/" +
      req?.files?.file_merchant_ic_back_image[0].filename;

    //create a new record in merchant registration table
    try {
      await pgConnection.query(
        "INSERT INTO tlmerchantapplication (merchant_ic_front_image, merchant_ic_back_image," +
          "industry_category_id, member_id, merchant_is_ic_submitted) VALUES ($1,$2,$3,$4,$5) RETURNING *",
        [
          icFrontUrl,
          icBackUrl,
          req?.body?.industry_category_id,
          req?.params?.memberid,
          true,
        ]
      );

      res.status(200).json({
        status: "true",
        message: "Your IC has been successfully saved.",
      });
    } catch (error) {
      console.log(error)
      //delete the specific member directory together with the uploaded IC files if postgres fails to save the record
      const dir = `${prodImageDirectory}/members/${req?.params?.memberid}`;
      if (fs.existsSync(dir)) {
        fs.rmdirSync(dir, { recursive: true });
      }

      res.status(400).json({
        status: "false",
        message: "Error in saving merchant details",
      });
    }
  },

  registerMerchantSelfie: async (req, res) => {
    //check if memberid presents in request parameter
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //check whether the member already completed the first merchant registration step (upload IC Files)
    try {
      const isMerchantRegistrationExist = await pgConnection.query(
        "select count(*) from tlmember a " +
          "inner join tlmerchantapplication b " +
          "on a.member_id=b.member_id " +
          "where a.member_id=$1",
        [req?.params?.memberid]
      );

      if (isMerchantRegistrationExist.rows[0].count <= 0) {
        throw error;
      }
    } catch (error) {
      
      res.status(400).json({
        status: "false",
        message: "There is no registration record in merchant table.",
      });
      return;
    }

    //upload selfie
    try {
      await uploadUserSelfie(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_merchant_selfie") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }
      res.status(400).json({
        status: "false",
        message: `Error when trying upload to upload files: ${error}`,
      });
      return;
    }

    //construct URL based on the uploaded selfie file
    const selfieUrl =
      req.protocol +
      "://" +
      req.get("host") +
      "/members/" +
      req?.params?.memberid +
      "/" +
      req?.file?.filename;

    try {
      await pgConnection.query(
        "UPDATE tlmerchantapplication " +
          "SET merchant_is_selfie_submitted = true, " +
          "merchant_selfie_image= $1 " +
          "WHERE member_id = $2" +
          "RETURNING *;",
        [selfieUrl, req?.params?.memberid]
      );

      res.status(200).json({
        status: "true",
        message: "Your selfie has been successfully saved.",
      });
    } catch (error) {
      //delete uploaded selfie file if postgres fails to save record
      const selfieFile = `${prodImageDirectory}/members/${req?.params?.memberid}/${req?.file?.filename}`;
      if (fs.existsSync(selfieFile)) {
        fs.unlinkSync(selfieFile);
      }

      res.status(400).json({
        status: "false",
        message: "Error in saving Selfie Image.",
      });
      return;
    }
  },

  registerMerchantBankDetails: async (req, res) => {
    //check if all the required elements present in request body
    if (
      !req?.params?.memberid ||
      !req?.body?.companyname ||
      !req?.body?.companytype ||
      !req?.body?.bankname ||
      !req?.body?.bankaccno ||
      !req?.body?.bankaccholdername ||
      !req?.body?.state ||
      !req?.body?.email ||
      !req?.body?.streetaddress
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //check whether the member already completed the first two steps (upload IC and Selfie)
    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select b.merchant_is_ic_submitted, b.merchant_is_selfie_submitted from tlmember a " +
            "inner join tlmerchantapplication b " +
            "on a.member_id=b.member_id " +
            "where a.member_id=$1",
          [req?.params?.memberid]
        );

      if (
        isValuePresent <= 0 ||
        !records[0]?.merchant_is_ic_submitted ||
        !records[0]?.merchant_is_selfie_submitted
      ) {
        res.status(400).json({
          status: "false",
          message: "You have not completed the previous two steps",
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: "false",
        message: "Error in updating information",
      });
      return;
    }

    let updateBusinessDetailsQuery =
      "update tlmerchantapplication set merchant_is_business_details_submitted = true, " +
      "merchant_company_name = $1, " +
      "merchant_company_type = $2, merchant_bank_name = $3, " +
      "merchant_bank_number = $4, merchant_bank_holder_name = $5, merchant_state = $6, " +
      "merchant_email = $7, merchant_street_address = $8";

    let updateBusinessDetailsParams = [
      req?.body?.companyname,
      req?.body?.companytype,
      req?.body?.bankname,
      req?.body?.bankaccno,
      req?.body?.bankaccholdername,
      req?.body?.state,
      req?.body?.email,
      req?.body?.streetaddress,
    ];

    let paramNumber = 8;

    //Add Optional Values in Query from Request Body
    if (req?.body?.companyregnum) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery +
        ", merchant_company_reg_number = $" +
        paramNumber;
      updateBusinessDetailsParams.push(req?.body?.companyregnum);
    }
    if (req?.body?.postcode) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery + ", merchant_postcode = $" + paramNumber;
      updateBusinessDetailsParams.push(req?.body?.postcode);
    }

    if (req?.body?.city) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery + ", merchant_city = $" + paramNumber;
      updateBusinessDetailsParams.push(req?.body?.city);
    }

    //add memberid condition
    paramNumber = paramNumber + 1;
    updateBusinessDetailsQuery =
      updateBusinessDetailsQuery + " WHERE member_id = $" + paramNumber;
    updateBusinessDetailsParams.push(req?.params?.memberid);

    updateBusinessDetailsQuery = updateBusinessDetailsQuery + ";";

    try {
      await pgConnection.query(
        updateBusinessDetailsQuery,
        updateBusinessDetailsParams
      );

      res.status(200).json({
        status: "true",
        message: "Your business details have been successfully saved.",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in saving business details",
      });
      return;
    }
  },

  registerMerchantLiveStreamSubscription: async (req, res) => {
    //check if memberid presents in request parameter
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //check whether the member already completed the first three steps (upload IC,Selfie and Bank Details)
    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select b.merchant_is_ic_submitted, b.merchant_is_selfie_submitted,b.merchant_is_business_details_submitted from tlmember a " +
            "inner join tlmerchantapplication b " +
            "on a.member_id=b.member_id " +
            "where a.member_id=$1",
          [req?.params?.memberid]
        );

      if (
        isValuePresent <= 0 ||
        !records[0]?.merchant_is_ic_submitted ||
        !records[0]?.merchant_is_selfie_submitted ||
        !records[0]?.merchant_is_business_details_submitted
      ) {
        res.status(400).json({
          status: "false",
          message: "You have not completed the previous three steps",
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: "false",
        message: "Error in updating information",
      });
      return;
    }

    //upload live stream receipt
    try {
      await uploadLiveStreamReceipt(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_merchant_receipt") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }
      res.status(400).json({
        status: "false",
        message: `Error when trying upload to upload files: ${error}`,
      });
      return;
    }

    //construct URL based on the uploaded receipt file
    const receiptUrl =
      req.protocol +
      "://" +
      req.get("host") +
      "/members/" +
      req?.params?.memberid +
      "/" +
      req?.file?.filename;

    try {
      await pgConnection.query(
        "UPDATE tlmerchantapplication " +
          "SET merchant_is_live_sub_receipt_submitted = true, " +
          "merchant_live_sub_receipt_image= $1 " +
          "WHERE member_id = $2" +
          "RETURNING *;",
        [receiptUrl, req?.params?.memberid]
      );

      res.status(200).json({
        status: "true",
        message: "Your subscription receipt has been successfully saved.",
      });
    } catch (error) {
      //delete uploaded receipt file if postgres fails to save record
      const receiptFile = `${prodImageDirectory}/members/${req?.params?.memberid}/${req?.file?.filename}`;
      if (fs.existsSync(receiptFile)) {
        fs.unlinkSync(receiptFile);
      }

      res.status(400).json({
        status: "false",
        message: "Error in saving Live Stream Subscription Receipt Image.",
      });
      return;
    }
  },

  registerMerchantPrivacyPolicy: async (req, res) => {
    //check if memberid presents in request parameter
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //check whether the member already completed the first four steps (upload IC,Selfie, Bank Details, LiveStream Subscription)
    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select b.merchant_is_ic_submitted, b.merchant_is_selfie_submitted, " +
            "b.merchant_is_business_details_submitted, b.merchant_is_live_sub_receipt_submitted from tlmember a " +
            "inner join tlmerchantapplication b " +
            "on a.member_id=b.member_id " +
            "where a.member_id=$1",
          [req?.params?.memberid]
        );

      if (
        isValuePresent <= 0 ||
        !records[0]?.merchant_is_ic_submitted ||
        !records[0]?.merchant_is_selfie_submitted ||
        !records[0]?.merchant_is_business_details_submitted ||
        !records[0]?.merchant_is_live_sub_receipt_submitted
      ) {
        res.status(400).json({
          status: "false",
          message: "You have not completed the previous three steps",
        });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: "false",
        message: "Error in updating information",
      });
      return;
    }

    try {
      await pgConnection.query(
        "UPDATE tlmerchantapplication " +
          "SET merchant_is_policy_submitted = $1, " +
          "merchant_application_status = $2 " +
          "WHERE member_id = $3" +
          "RETURNING *;",
        [true, "COMPLETE", req?.params?.memberid]
      );

      res.status(200).json({
        status: "true",
        message: "Your registration is completed. ",
      });
    } catch (error) {
      res.status(400).json({
        status: "false",
        message:
          "Error in Updating Merchant Application Status. Please try again. ",
      });
      return;
    }
  },

  checkMerchantRegistrationStatus: async (req, res) => {
    //Check if member id is provided as a param
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select merchant_is_ic_submitted, merchant_is_selfie_submitted, " +
            "merchant_is_business_details_submitted, merchant_is_live_sub_receipt_submitted, " +
            "merchant_is_policy_submitted, " +
            "merchant_application_status from tlmerchantapplication " +
            "where member_id=$1 LIMIT 1",
          [req?.params?.memberid]
        );

      if (
        isValuePresent <= 0 ||
        !records[0]?.merchant_is_ic_submitted ||
        !records[0]?.merchant_is_selfie_submitted ||
        !records[0]?.merchant_is_business_details_submitted ||
        !records[0]?.merchant_is_live_sub_receipt_submitted ||
        !records[0]?.merchant_is_policy_submitted ||
        !records[0]?.merchant_application_status === "COMPLETE"
      ) {
        res.status(200).json({
          status: "false",
          message: "Merchant Registration is Incomplete",
          results: records[0] ?? {
            merchant_is_ic_submitted: false,
            merchant_is_selfie_submitted: false,
            merchant_is_business_details_submitted: false,
            merchant_is_live_sub_receipt_submitted: false,
            merchant_is_policy_submitted: false,
            merchant_application_status: "INCOMPLETE",
          },
        });
      } else {
        res.status(200).json({
          status: "true",
          message: "Merchant Registration is Complete",
          results: records[0],
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in retrieving merchant application status",
      });
    }
  },

  checkAndRetrieveMerchantDetails: async (req, res, next) => {
    //Check if member id is provided as a param
    if (!req?.params?.memberid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select * from tlmerchant where member_id=$1 LIMIT 1",
          [req?.params?.memberid]
        );

      if (isValuePresent > 0) {
        const { rowCount: liveStreamCounts } = await pgConnection.query(
          "select * from tllivestream where merchant_id=$1",
          [records[0].merchant_id]
        );

        var merchantRecords = records[0];

        if (liveStreamCounts <= 0) {
          merchantRecords["is_fresh_merchant"] = true;
        } else {
          merchantRecords["is_fresh_merchant"] = false;
        }

        res.status(200).json({
          status: "true",
          message: "Merchant Details have been retrieved",
          merchant_results: merchantRecords,
        });
      } else {
        res.status(200).json({
          status: "false",
          message:
            "Please complete Merchant Application to access Merchant Dashboard",
          merchant_results: {},
        });
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in Retrieving Merchant Details",
      });
    }
  },

  approveOrRejectApplication: async (req, res, next) => {
    //check if all the necessary details are present
    if (
      !req?.params?.applicationid ||
      !req?.body?.remarks ||
      !req?.body?.status
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    if (req?.body?.status !== "APPROVED" && req?.body?.status !== "REJECTED") {
      res.status(400).json({
        status: "false",
        message: "Invalid status provided",
      });
      return;
    }

    try {
      let updatedResults;
      if (req?.body?.status === "REJECTED") {
        updatedResults = await pgConnection.query(
          "UPDATE tlmerchantapplication " +
            "SET merchant_application_status = 'REJECTED', " +
            "merchant_application_status_remarks = $1 " +
            "WHERE merchant_application_id = $2;",
          [req?.body?.remarks, req?.params?.applicationid]
        );
      } else if (req?.body?.status === "APPROVED") {
        updatedResults = await pgConnection.query(
          "WITH action_update_merchant_application_status " +
            "AS (UPDATE tlmerchantapplication " +
            "set merchant_application_status='APPROVED', " +
            "merchant_application_status_remarks = $1" +
            "WHERE merchant_application_id = $2 " +
            "RETURNING *), " +
            "action_insert_merchant AS " +
            "(INSERT INTO tlmerchant (merchant_ic_front_image,merchant_ic_back_image, " +
            "merchant_selfie_image,merchant_company_name, " +
            "merchant_bank_name,merchant_bank_number,merchant_bank_holder_name, " +
            "merchant_email,merchant_street_address, " +
            "member_id,industry_category_id,merchant_state, " +
            "merchant_company_type,merchant_company_reg_number, " +
            "merchant_city,merchant_postcode,merchant_receipt_image) " +
            "SELECT merchant_ic_front_image, merchant_ic_back_image, " +
            "merchant_selfie_image,merchant_company_name,merchant_bank_name, " +
            "merchant_bank_number,merchant_bank_holder_name,merchant_email,merchant_street_address, " +
            "member_id,industry_category_id,merchant_state,merchant_company_type, " +
            "merchant_company_reg_number, merchant_city,merchant_postcode, merchant_live_sub_receipt_image " +
            "FROM action_update_merchant_application_status " +
            "RETURNING * " +
            ") " +
            "SELECT * " +
            "FROM action_insert_merchant ",
          [req?.body?.remarks, req?.params?.applicationid]
        );
      }

      const { rowCount } = updatedResults;

      if (rowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "No record is found",
        });
      } else {
        res.status(200).json({
          status: "true",
          message: "Application has been successfully updated",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in Updating Application Status",
      });
    }
  },

  approveApplicationEmail: async (req, res, next) => {
    //check if all the necessary details are present
    if (!req?.query?.email) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    var remarks = "Your application has been successfully published";
    try {
      let updatedResults;
      updatedResults = await pgConnection.query(
        "WITH action_update_merchant_application_status " +
          "AS (UPDATE tlmerchantapplication " +
          "set merchant_application_status='APPROVED', " +
          "merchant_application_status_remarks = $1" +
          "WHERE merchant_email = $2 " +
          "RETURNING *), " +
          "action_insert_merchant AS " +
          "(INSERT INTO tlmerchant (merchant_ic_front_image,merchant_ic_back_image, " +
          "merchant_selfie_image,merchant_company_name, " +
          "merchant_bank_name,merchant_bank_number,merchant_bank_holder_name, " +
          "merchant_email,merchant_street_address, " +
          "member_id,industry_category_id,merchant_state, " +
          "merchant_company_type,merchant_company_reg_number, " +
          "merchant_city,merchant_postcode,merchant_receipt_image) " +
          "SELECT merchant_ic_front_image, merchant_ic_back_image, " +
          "merchant_selfie_image,merchant_company_name,merchant_bank_name, " +
          "merchant_bank_number,merchant_bank_holder_name,merchant_email,merchant_street_address, " +
          "member_id,industry_category_id,merchant_state,merchant_company_type, " +
          "merchant_company_reg_number, merchant_city,merchant_postcode, merchant_live_sub_receipt_image " +
          "FROM action_update_merchant_application_status " +
          "RETURNING * " +
          ") " +
          "SELECT * " +
          "FROM action_insert_merchant ",
        [remarks, req?.query?.email]
      );

      const { rowCount } = updatedResults;

      if (rowCount <= 0) {
        res.status(200).json({
          status: "false",
          message: "No record is found",
        });
      } else {
        res.status(200).json({
          status: "true",
          message: "Application has been successfully updated",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in Updating Application Status",
      });
    }
  },

  tempRegisterMerchantBankDetails: async (req, res, next) => {
    //check if all the required elements present in request body
    if (
      !req?.params?.memberid ||
      !req?.body?.companyname ||
      !req?.body?.companytype ||
      !req?.body?.bankname ||
      !req?.body?.bankaccno ||
      !req?.body?.bankaccholdername ||
      !req?.body?.state ||
      !req?.body?.email ||
      !req?.body?.streetaddress
    ) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //check whether the member already completed the first two steps (upload IC and Selfie)
    try {
      const { rowCount: isValuePresent, rows: records } =
        await pgConnection.query(
          "select b.merchant_is_ic_submitted, b.merchant_is_selfie_submitted from tlmember a " +
            "inner join tlmerchantapplication b " +
            "on a.member_id=b.member_id " +
            "where a.member_id=$1",
          [req?.params?.memberid]
        );

      if (
        isValuePresent <= 0 ||
        !records[0]?.merchant_is_ic_submitted ||
        !records[0]?.merchant_is_selfie_submitted
      ) {
        throw "Please complete the first two steps.";
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "You have not completed the previous two steps",
      });
      return;
    }

    let updateBusinessDetailsQuery =
      "update tlmerchantapplication set merchant_is_business_details_submitted = true, " +
      "merchant_application_status = 'COMPLETE', merchant_company_name = $1, " +
      "merchant_company_type = $2, merchant_bank_name = $3, " +
      "merchant_bank_number = $4, merchant_bank_holder_name = $5, merchant_state = $6, " +
      "merchant_email = $7, merchant_street_address = $8";

    let updateBusinessDetailsParams = [
      req?.body?.companyname,
      req?.body?.companytype,
      req?.body?.bankname,
      req?.body?.bankaccno,
      req?.body?.bankaccholdername,
      req?.body?.state,
      req?.body?.email,
      req?.body?.streetaddress,
    ];

    let paramNumber = 8;

    //Add Optional Values in Query from Request Body
    if (req?.body?.companyregnum) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery +
        ", merchant_company_reg_number = $" +
        paramNumber;
      updateBusinessDetailsParams.push(req?.body?.companyregnum);
    }
    if (req?.body?.postcode) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery + ", merchant_postcode = $" + paramNumber;
      updateBusinessDetailsParams.push(req?.body?.postcode);
    }

    if (req?.body?.city) {
      paramNumber = paramNumber + 1;
      updateBusinessDetailsQuery =
        updateBusinessDetailsQuery + ", merchant_city = $" + paramNumber;
      updateBusinessDetailsParams.push(req?.body?.city);
    }

    //add memberid condition
    paramNumber = paramNumber + 1;
    updateBusinessDetailsQuery =
      updateBusinessDetailsQuery + " WHERE member_id = $" + paramNumber;
    updateBusinessDetailsParams.push(req?.params?.memberid);

    updateBusinessDetailsQuery = updateBusinessDetailsQuery + ";";

    try {
      await pgConnection.query(
        updateBusinessDetailsQuery,
        updateBusinessDetailsParams
      );

      await pgConnection.query(
        "WITH action_update_merchant_application_status " +
          "AS (UPDATE tlmerchantapplication " +
          "set merchant_application_status='APPROVED', " +
          "merchant_application_status_remarks = $1" +
          "WHERE member_id = $2 " +
          "RETURNING *), " +
          "action_insert_merchant AS " +
          "(INSERT INTO tlmerchant (merchant_ic_front_image,merchant_ic_back_image, " +
          "merchant_selfie_image,merchant_company_name, " +
          "merchant_bank_name,merchant_bank_number, " +
          "merchant_email,merchant_street_address, " +
          "member_id,industry_category_id,merchant_state, " +
          "merchant_company_type,merchant_company_reg_number, " +
          "merchant_city,merchant_postcode) " +
          "SELECT merchant_ic_front_image, merchant_ic_back_image, " +
          "merchant_selfie_image,merchant_company_name,merchant_bank_name, " +
          "merchant_bank_number,merchant_email,merchant_street_address, " +
          "member_id,industry_category_id,merchant_state,merchant_company_type, " +
          "merchant_company_reg_number, merchant_city,merchant_postcode " +
          "FROM action_update_merchant_application_status " +
          "RETURNING * " +
          ") " +
          "SELECT * " +
          "FROM action_insert_merchant ",
        ["Your application is approved.", req?.params?.memberid]
      );

      res.status(200).json({
        status: "true",
        message: "Your application is approved",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in saving business details",
      });
      return;
    }
  },

  //TODO - Delete Old Image from Local Directory After Editing
  updateMerchantProfile: async (req, res, next) => {
    //check if memberId presents in request parameter
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //upload merchant image
    try {
      await uploadMerchantProfileImage(req, res);
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        console.log(error);
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }
    var paramNumber = 0;

    var updateProfileParams = [];
    var updateProfileQuery = "UPDATE TLMerchant SET ";

    //construct URL based on the uploaded image file
    if (req?.file?.filename) {
      var merchantImageURL;
      merchantImageURL =
        req.protocol +
        "://" +
        req.get("host") +
        "/merchants/" +
        req?.file?.filename;

      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_image = $" + paramNumber + ",";
      updateProfileParams.push(merchantImageURL);
    }

    if (req?.body?.companyname) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_company_name = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.companyname);
    }

    if (req?.body?.bankname) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_bank_name = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.bankname);
    }

    if (req?.body?.bankaccno) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_bank_number = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.bankaccno);
    }

    if (req?.body?.bankaccholdername) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery +
        "merchant_bank_holder_name = $" +
        paramNumber +
        ",";
      updateProfileParams.push(req?.body?.bankaccholdername);
    }

    if (req?.body?.state) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_state = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.state);
    }

    if (req?.body?.email) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_email = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.email);
    }

    if (req?.body?.streetaddress) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_street_address = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.streetaddress);
    }

    if (req?.body?.city) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_city = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.city);
    }

    if (req?.body?.postcode) {
      paramNumber = paramNumber + 1;
      updateProfileQuery =
        updateProfileQuery + "merchant_postcode = $" + paramNumber + ",";
      updateProfileParams.push(req?.body?.postcode);
    }

    updateProfileQuery = updateProfileQuery.substring(
      0,
      updateProfileQuery.length - 1
    );

    paramNumber = paramNumber + 1;
    updateProfileQuery =
      updateProfileQuery + " WHERE merchant_id = $" + paramNumber;
    updateProfileParams.push(req?.params?.merchantid);

    updateProfileQuery = updateProfileQuery + " RETURNING *";

    try {
      const { rows, rowCount } = await pgConnection.query(
        updateProfileQuery,
        updateProfileParams
      );

      if (rowCount > 0) {
        res.status(201).json({
          status: true,
          message: "Your profile has been successfully updated",
          profile_results: rows[0],
        });
      } else {
        res.status(401).json({
          status: false,
          message: "No records found for the merchant",
        });
        //delete uploaded profile image file if postgres fmails to save record
        const profileImageFile = `${prodImageDirectory}/merchants/${req?.file?.filename}`;
        if (fs.existsSync(profileImageFile)) {
          fs.unlinkSync(profileImageFile);
        }
      }
    } catch (error) {
      //delete uploaded profile image file if postgres fails to save record
      const profileImageFile = `${prodImageDirectory}/merchants/${req?.file?.filename}`;
      if (fs.existsSync(profileImageFile)) {
        fs.unlinkSync(profileImageFile);
      }

      console.log(error);
      res.status(401).json({
        status: false,
        message: "Error in Updating Merchant Profile",
      });
    }
  },

  showMerchantProfile: async (req, res, next) => {
    //check if merchantID presents in request parameter
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      const { rowCount, rows } = await pgConnection.query(
        "select * from tlmerchant where merchant_id = $1 limit 1",
        [req?.params?.merchantid]
      );

      if (rowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Successfully Retrieved Merchant Details",
          merchant_results: rows[0],
        });
      } else {
        res.status(200).json({
          status: false,
          message: "No Merchant Details Found for the Specific ID.",
        });
      }
    } catch (error) {
      res.status(401).json({
        status: false,
        message: "Error in Updating Merchant Profile",
      });
    }
  },
};
