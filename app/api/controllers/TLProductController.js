const pgConnection = require("../../../config/database");
const {
  devImageDirectory,
  prodImageDirectory,
  nexmoSecretKey,
} = require("../../../constants");
const uploadProductImage = require("../helper/upload_images/uploadProductImage");
var fs = require("fs");

module.exports = {
  addProductDetails: async (req, res, next) => {
    //check if merchantID presents in request parameter
    if (!req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //upload productImage
    try {
      await uploadProductImage(req, res);
      if (!req?.file || req?.file?.fieldname !== "file_product_image") {
        throw "No file provided";
      }
    } catch (error) {
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        console.log(error);
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to upload files: ${error}`,
      });
      return;
    }

    let variation;
    if (req?.body?.variation) {
      variation = JSON.parse(req?.body?.variation);
    }

    //construct URL based on the uploaded image file
    const productImageURL =
      req.protocol +
      "://" +
      req.get("host") +
      "/products/" +
      req?.file?.filename;

    let insertProductDetails;

    try {
      //Check if there is any variation for the specific product
      if (variation && variation.length > 0) {
        let productSize = [],
          productColor = [],
          productPrice = [],
          productQuantity = [],
          productDisplayStatus = [];

        variation.forEach((element) => {
          productSize.push(element.size ?? "");
          productPrice.push(element.price ?? "");
          productColor.push(element.color ?? "");
          productQuantity.push(element.quantity ?? "");
          productDisplayStatus.push(element.status ?? "");
        });

        insertProductDetails = await pgConnection.query(
          "WITH action_product_insert AS (" +
            "INSERT INTO tlproduct (product_name,product_description,product_image,product_main_cat_id,product_sub_cat_id,merchant_id)" +
            "VALUES ($1,$2,$3,$4,$5,$6)" +
            "RETURNING *" +
            ")," +
            "action_variant_insert AS (" +
            "INSERT INTO tlproductvariation (product_id,product_variation_size, product_variation_color, product_variation_price, product_variation_quantity,product_variation_status)" +
            "SELECT product_id, UNNEST($7::text[]),UNNEST($8::text[]),UNNEST($9::decimal[]),UNNEST($10::integer[]),UNNEST($11::boolean[]) FROM action_product_insert" +
            " RETURNING *" +
            ")" +
            "SELECT  *" +
            "FROM action_product_insert",
          [
            req?.body?.name,
            req?.body?.description,
            productImageURL,
            req?.body?.category_id,
            req?.body?.subcategory_id,
            req?.params?.merchantid,
            productSize,
            productColor,
            productPrice,
            productQuantity,
            productDisplayStatus,
          ]
        );
      } else {
        insertProductDetails = await pgConnection.query(
          "INSERT INTO tlproduct (product_name,product_description,product_image,product_main_cat_id,product_sub_cat_id,merchant_id) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *",
          [
            req?.body?.name,
            req?.body?.description,
            productImageURL,
            req?.body?.category_id,
            req?.body?.subcategory_id,
            req?.params?.merchantid,
          ]
        );
      }
      res.status(201).json({
        status: "true",
        message: "Successfully saved product details",
        results: insertProductDetails?.rows[0] ?? {},
      });
    } catch (error) {
      console.log(error);
      //delete uploaded product file if postgres fails to save record
      const productFile = `${prodImageDirectory}/products/${req?.file?.filename}`;
      if (fs.existsSync(productFile)) {
        fs.unlinkSync(productFile);
      }
      res.status(400).json({
        status: "false",
        message: error?.detail ?? "Error in saving product information",
      });
    }
  },

  deleteProductDetails: async (req, res, next) => {
    if (!req?.params?.productid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    try {
      //Check and prevent product deletion if any livestream with the status 'LIVE NOW', 'PUBLISHED' or 'SAVED'
      //depends on the product
      const { rowCount: liveStreamRowCount } = await pgConnection.query(
        `select tl1.live_stream_status, tl1.live_stream_id
        from tllivestream tl1
        inner join tllivestreamproducts tl2
        on tl1.live_stream_id = tl2.live_stream_id
        inner join tlproduct tl3
        on tl2.product_id = tl3.product_id
        where tl3.product_id = $1
        and tl1.live_stream_status IN ('PUBLISHED','SAVED','LIVE NOW')`,
        [req?.params?.productid]
      );

      if (liveStreamRowCount > 0) {
        res.status(200).json({
          status: true,
          message:
            "The product cannot be deleted because it is linked to one of your active livestreams.",
        });
        return;
      }

      const { rowCount: deletedRowCount } = await pgConnection.query(
        "WITH action_product_delete AS ( " +
          "DELETE FROM tlproduct where product_id = $1 " +
          "RETURNING * " +
          "), " +
          "action_product_variation_delete AS ( " +
          "DELETE FROM tlproductvariation where product_id = $1 " +
          "RETURNING * " +
          ") " +
          "SELECT * FROM action_product_delete;",
        [req?.params?.productid]
      );

      if (deletedRowCount > 0) {
        res.status(200).json({
          status: true,
          message: "Successfully deleted the product.",
        });
      } else {
        res.status(200).json({
          status: false,
          message: "No product has been deleted.",
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        status: "false",
        message: "Error in deleting product details.",
      });
    }
  },

  viewProductDetails: async (req, res, next) => {
    try {
      const productDetails = await pgConnection.query(
        "SELECT  q.*," +
          "(SELECT json_agg(json_build_object('product_variation_id',v.product_variation_id," +
          " 'product_variation_color',v.product_variation_color," +
          " 'product_variation_quantity',v.product_variation_quantity," +
          " 'product_variation_size',v.product_variation_size," +
          " 'product_variation_price',v.product_variation_price," +
          " 'product_variation_status',v.product_variation_status)) AS variation" +
          " FROM tlproductvariation v WHERE q.product_id = v.product_id) FROM tlproduct q"
      );
      res.status(201).json({
        status: "true",
        message: "Product Details have been successfully retrieved",
        product_results: productDetails.rows,
      });
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in Retrieving Product Details",
      });
    }
  },

  viewProductDetailsPerMerchant: async (req, res, next) => {
    let productDetails;
    try {
      if (!req?.params?.merchantid) {
        productDetails = await pgConnection.query(
          "SELECT  q.*," +
            "(SELECT json_agg(json_build_object('product_variation_id',v.product_variation_id," +
            " 'product_variation_color',v.product_variation_color," +
            " 'product_variation_quantity',v.product_variation_quantity," +
            " 'product_variation_size',v.product_variation_size," +
            " 'product_variation_price',v.product_variation_price," +
            " 'product_variation_status',v.product_variation_status)) AS variation" +
            " FROM tlproductvariation v WHERE q.product_id = v.product_id) FROM tlproduct q"
        );
        res.status(201).json({
          status: "true",
          message: "Product Details have been successfully retrieved",
          product_results: productDetails.rows,
        });
      } else {
        productDetails = await pgConnection.query(
          "SELECT  q.*," +
            "(SELECT json_agg(json_build_object('product_variation_id',v.product_variation_id," +
            " 'product_variation_color',v.product_variation_color," +
            " 'product_variation_quantity',v.product_variation_quantity," +
            " 'product_variation_size',v.product_variation_size," +
            " 'product_variation_price',v.product_variation_price," +
            " 'product_variation_status',v.product_variation_status)) AS variation" +
            " FROM tlproductvariation v WHERE q.product_id = v.product_id) FROM tlproduct q " +
            "WHERE q.merchant_id=$1",
          [req?.params?.merchantid]
        );
        res.status(201).json({
          status: "true",
          message: "Product Details have been successfully retrieved",
          product_results: productDetails.rows,
        });
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in Retrieving Product Details",
      });
    }
  },

  getCategories: async (req, res, next) => {
    try {
      const { rows } = await pgConnection.query(
        "SELECT a.*, (SELECT json_agg(json_build_object('product_sub_cat_id',b.product_sub_cat_id, " +
          "'product_sub_cat_name',b.product_sub_cat_name)) " +
          "AS sub_category from tlproductsubcategory b where a.product_main_cat_id = b.product_main_cat_id) " +
          "FROM tlproductmaincategory a"
      );

      if (rows && rows?.length > 0) {
        res.status(201).json({
          status: "true",
          message: "Product Categories have been successfully retrieved",
          product_category_results: rows,
        });
      } else {
        res.status(201).json({
          status: "false",
          message: "No categories are found",
        });
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in Retrieving Product Category Details",
      });
    }
  },

  modifyMerchantProduct: async (req, res, next) => {
    //check if productid presents in request parameter
    if (!req?.params?.productid || !req?.params?.merchantid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }

    //upload productImage
    try {
      await uploadProductImage(req, res);
      if (
        !req?.body?.name ||
        !req?.body?.description ||
        !req?.body?.category_id ||
        !req?.body?.subcategory_id ||
        !req?.body?.variation
      ) {
        throw "Insufficient information provided";
      }
    } catch (error) {
      //delete uploaded product file if postgres fails to save record
      const productImageFile = `${prodImageDirectory}/products/${req?.file?.filename}`;
      if (fs.existsSync(productImageFile)) {
        fs.unlinkSync(productImageFile);
      }
      if (error.code === "LIMIT_UNEXPECTED_FILE") {
        console.log(error);
        res.status(400).json({
          status: "false",
          message: "Too many files to upload",
        });
        return;
      }

      res.status(400).json({
        status: "false",
        message: `Error when trying to save information: ${error}`,
      });
      return;
    }

    let variation = JSON.parse(req?.body?.variation);

    if (!Array.isArray(variation) || variation.length <= 0) {
      res.status(400).json({
        status: "false",
        message: "Invalid variation information provided",
      });
      return;
    }

    try {
      let updateProductDetails;

      let productSize = [],
        productColor = [],
        productPrice = [],
        productQuantity = [],
        productDisplayStatus = [];

      variation.forEach((element) => {
        productSize.push(element.size ?? "");
        productPrice.push(element.price ?? "");
        productColor.push(element.color ?? "");
        productQuantity.push(element.quantity ?? "");
        productDisplayStatus.push(element.status ?? "");
      });

      let updateQueryParameters = [
        req?.body?.name,
        req?.body?.description,
        req?.body?.category_id,
        req?.body?.subcategory_id,
        req?.params?.productid,
        productSize,
        productColor,
        productPrice,
        productQuantity,
        productDisplayStatus,
      ];

      //if image is provided, construct a URL and sql update parameter
      var additionalQueryForUploadingImage = ` `;
      if (req?.file?.filename) {
        additionalQueryForUploadingImage = ` ,product_image = $11 `;
        const productImageURL =
          req.protocol +
          "://" +
          req.get("host") +
          "/products/" +
          req?.file?.filename;
        updateQueryParameters.push(productImageURL);
      }

      updateProductDetails = await pgConnection.query(
        " WITH action_product_update AS ( " +
          "UPDATE TLPRODUCT " +
          "SET product_name = $1, " +
          "product_description = $2, " +
          "product_main_cat_id = $3, " +
          "product_sub_cat_id = $4 " +
          ` ${additionalQueryForUploadingImage} ` +
          "WHERE product_id = $5 " +
          "RETURNING * " +
          "), " +
          "action_variant_delete AS( " +
          "DELETE FROM tlproductvariation where product_id = $5 " +
          "), " +
          "action_variant_insert AS ( " +
          "INSERT INTO tlproductvariation (product_id,product_variation_size, product_variation_color, product_variation_price, product_variation_quantity,product_variation_status) " +
          "SELECT $5, UNNEST($6::text[]),UNNEST($7::text[]),UNNEST($8::decimal[]),UNNEST($9::integer[]),UNNEST($10::boolean[]) FROM action_product_update " +
          "RETURNING * " +
          ") " +
          "SELECT  * " +
          "FROM action_product_update",
        updateQueryParameters
      );

      if (updateProductDetails?.rowCount > 0) {
        res.status(201).json({
          status: "true",
          message: "Successfully updated product details",
        });
      } else {
        res.status(200).json({
          status: "false",
          message: "Failed to update. The product id cannot be found.",
        });
        //delete uploaded product file if postgres fails to save record
        const productFile = `${prodImageDirectory}/products/${req?.file?.filename}`;
        if (fs.existsSync(productFile)) {
          fs.unlinkSync(productFile);
        }
      }
    } catch (error) {
      console.log(error);
      //delete uploaded product file if postgres fails to save record
      const productFile = `${prodImageDirectory}/products/${req?.file?.filename}`;
      if (fs.existsSync(productFile)) {
        fs.unlinkSync(productFile);
      }
      res.status(400).json({
        status: "false",
        message: error?.detail ?? "Error in saving product information",
      });
    }
  },

  getMerchantProductOne: async (req, res, next) => {
    //check if productid presents in request parameter
    if (!req?.params?.productid) {
      res.status(400).json({
        status: "false",
        message: "Insufficient information provided",
      });
      return;
    }
    try {
      const { rowCount, rows } = await pgConnection.query(
        "SELECT  q.*,pmc.product_main_cat_name, psc.product_sub_cat_name, " +
          "(SELECT json_agg(json_build_object('product_variation_id',v.product_variation_id," +
          " 'product_variation_color',v.product_variation_color," +
          " 'product_variation_quantity',v.product_variation_quantity," +
          " 'product_variation_size',v.product_variation_size," +
          " 'product_variation_price',v.product_variation_price," +
          " 'product_variation_status',v.product_variation_status)) AS variation" +
          " FROM tlproductvariation v WHERE q.product_id = v.product_id) FROM tlproduct q " +
          " inner join tlproductmaincategory pmc " +
          " on q.product_main_cat_id = pmc.product_main_cat_id " +
          " inner join tlproductsubcategory psc " +
          " on q.product_sub_cat_id = psc.product_sub_cat_id " +
          "WHERE q.product_id=$1 limit 1",
        [req?.params?.productid]
      );
      if (rowCount > 0) {
        res.status(201).json({
          status: "true",
          message: "Product Details have been successfully retrieved",
          product_result: rows[0],
        });
      } else {
        res.status(201).json({
          status: "false",
          message: "No product is found for the specific ID",
        });
      }
    } catch (error) {
      res.status(400).json({
        status: "false",
        message: "Error in Retrieving Product Details",
      });
    }
  },
};
