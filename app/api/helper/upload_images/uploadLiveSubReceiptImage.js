const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {prodImageDirectory,devImageDirectory} = require("../../../../constants") ;

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    dir = `${prodImageDirectory}/members/${req?.params?.memberid}`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {
    if (!req?.params?.memberid) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    const match = ["image/png", "image/jpeg", "application/pdf"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }

    if (!file?.fieldname || file.fieldname !== "file_merchant_receipt") {
        var message = `Insufficient information provided`;
        callback(message, null);
        return;
    }

    const filename = `receipt-${Date.now()}-${file.originalname}`;
    callback(null, filename); 
  },
});

var uploadFiles = multer({ storage: storage }).single('file_merchant_receipt');
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
