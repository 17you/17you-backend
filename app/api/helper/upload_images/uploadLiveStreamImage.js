const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {
  prodImageDirectory,
  devImageDirectory,
} = require("../../../../constants");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    dir = `${prodImageDirectory}/livestream`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {
    if (
      !req?.body?.title ||
      !req?.body?.description ||
      !req?.body?.date ||
      !req?.body?.start_time ||
      !req?.body?.end_time ||
      !req?.body?.product ||
      !req?.body?.status ||
      !req?.body?.shipping_fee
    ) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    let product;
    product = JSON.parse(req?.body?.product);
    if (!Array.isArray(product) || product.length <= 0) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    if (!file?.fieldname || file?.fieldname !== "file_livestream_image") {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    const match = ["image/png", "image/jpeg"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }

    const filename = `livestream-${req?.params?.merchantid}-${Date.now()}-${
      file.originalname
    }`;
    callback(null, filename);
  },
});

var uploadFiles = multer({ storage: storage }).single("file_livestream_image");
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
