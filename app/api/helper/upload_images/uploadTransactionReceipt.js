const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {
  prodImageDirectory,
  devImageDirectory,
} = require("../../../../constants");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    dir = `${prodImageDirectory}/transaction`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {
    if (
      !req?.body?.live_stream_id ||
      !req?.body?.merchant_id ||
      !req?.body?.address_id ||
      !req?.body?.member_id ||
      !req?.body?.final_price ||
      !req?.body?.total_product_price ||
      !req?.body?.delivery_fees ||
      !req?.body?.payment_method ||
      !req?.body?.product
    ) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    let product;
    product = JSON.parse(req?.body?.product);
    if (!Array.isArray(product) || product.length <= 0) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    if (!file?.fieldname || file?.fieldname !== "file_transaction_receipt") {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    const match = ["image/png", "image/jpeg","application/pdf"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }

    const filename = `transaction-${req?.body?.merchant_id}-${req?.body?.member_id}-${Date.now()}-${
      file.originalname
    }`;
    callback(null, filename);
  },
});

var uploadFiles = multer({ storage: storage }).single("file_transaction_receipt");
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
