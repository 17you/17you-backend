const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {
  prodImageDirectory,
  devImageDirectory,
} = require("../../../../constants");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    dir = `${prodImageDirectory}/transaction`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {
    if (
      !req?.body?.merchant_id ||
      !req?.body?.member_id ||
      !req?.body?.payment_method
    ) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    if (!file?.fieldname || file?.fieldname !== "file_transaction_receipt") {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    const match = ["image/png", "image/jpeg","application/pdf"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }

    const filename = `transaction-${req?.body?.merchant_id}-${req?.body?.member_id}-${Date.now()}-${
      file.originalname
    }`;
    callback(null, filename);
  },
});

var uploadFiles = multer({ storage: storage }).single("file_transaction_receipt");
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
