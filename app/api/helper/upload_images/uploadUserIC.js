const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {
  prodImageDirectory,
  devImageDirectory,
} = require("../../../../constants");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {

    if (!fs.existsSync(`${prodImageDirectory}/members`)) {
      fs.mkdirSync(`${prodImageDirectory}/members`);
    }

    dir = `${prodImageDirectory}/members/${req?.params?.memberid}`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {
    if (!req?.body?.industry_category_id || !req?.params?.memberid) {
      var message = `Insufficient information provided`;
      callback(message, null);
      return;
    }

    const match = ["image/png", "image/jpeg"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }

    var filename;
    if (file.fieldname === "file_merchant_ic_front_image") {
      filename = `icfront-${Date.now()}-${file.originalname}`;
    }

    if (file.fieldname === "file_merchant_ic_back_image") {
      filename = `icback-${Date.now()}-${file.originalname}`;
    }
    callback(null, filename);
  },
});

var uploadFiles = multer({ storage: storage }).fields([
  {
    name: "file_merchant_ic_front_image",
    maxCount: 1,
  },
  {
    name: "file_merchant_ic_back_image",
    maxCount: 1,
  },
]);
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
