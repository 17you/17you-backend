const util = require("util");
const path = require("path");
const multer = require("multer");
var fs = require("fs");
const {prodImageDirectory,devImageDirectory} = require("../../../../constants") ;

//TODO - Delete Old Image From Directory after Updating
var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    dir = `${prodImageDirectory}/products`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    callback(null, path.join(dir));
  },
  filename: (req, file, callback) => {

    if (!req?.body?.name || !req?.body?.description || !req?.body?.category_id || !req?.body?.subcategory_id ) {
        var message = `Insufficient information provided`;
        callback(message, null);
        return;
      }

      if (!file?.fieldname || file?.fieldname !== "file_product_image") {
        var message = `Insufficient information provided`;
        callback(message, null);
        return;
    }

    const match = ["image/png", "image/jpeg"];
    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      callback(message, null);
      return;
    }
    const filename = `product-${req?.params?.merchantid}-${Date.now()}-${file.originalname}`;
    callback(null, filename); 
  },
});

var uploadFiles = multer({ storage: storage }).single('file_product_image');
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
