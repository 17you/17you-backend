const bcrypt = require("bcrypt");

const hashValue = async (password) => {
  try {
    const results = await bcrypt.hash(password, 5);
    return [201, results];
  } catch (error) {
    return [500, error];
  }
};

const compareValue = async (firstValue, secondValue) => {
  try {
    const results = await bcrypt.compare(firstValue, secondValue);
    return [201, results];
  } catch (error) {
    return [500, error];
  }
};

module.exports = {
  hashValue,
  compareValue
};
