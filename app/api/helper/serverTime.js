const moment = require("moment");

module.exports = {
  getCurrentTime: function (time) {
    time = moment().format();
    return time;
  },
};
