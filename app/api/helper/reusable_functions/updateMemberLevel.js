const pgConnection = require("../../../../config/database");
const {
  areArraysEqual,
  getDifferenceBetweenTwoArrays,
} = require("../validation");

const updateMemberLevel = async (memberid) => {
  try {
    //Get Total Energy and All the Available Levels
    const { rows: energyLevelRows, rowCount: energyLevelRowCount } =
      await pgConnection.query(
        "select json_agg(tl1.*) as energy_level_limit, " +
          " (select coalesce(sum(energy_redeemed_total),0) " +
          " as total_energy from tlmemberenergy  " +
          " where member_id = $1 :: int ) " +
          " from tlenergylevel tl1",
        [memberid]
      );

    //Return if totalenergycount is 0
    if (energyLevelRowCount <= 0 || energyLevelRows[0]?.total_energy <= 0) {
      return;
    }

    //new Variable Declaration for better readability
    const allLevels = energyLevelRows[0]?.energy_level_limit;
    var totalEnergy = energyLevelRows[0]?.total_energy;

    //Identify Member Current Level Based on Total Energy Redeemed
    var memberCurrentLevel = 0;
    for (var i = 0; i < allLevels.length; i++) {
      totalEnergy = totalEnergy - allLevels[i].energy_level_limit;
      if (totalEnergy < 0) {
        memberCurrentLevel = allLevels[i].energy_level - 1;
        break;
      }
    }

    //Return if Member is not eligible for level 1 or more
    if (memberCurrentLevel <= 0) {
      return;
    }

    // Retrieve Already Completed Levels of the Member
    const {
      rows: memberCompletedLevelRows,
      rowCount: memberCompletedLevelRowCount,
    } = await pgConnection.query(
      "select json_agg(energy_level::int) as completed_levels from tlenergymemberlevel " +
        "where member_id = $1 " +
        "and energy_level <= $2",
      [memberid, memberCurrentLevel]
    );

    const completedLevels = memberCompletedLevelRows[0]?.completed_levels;

    //All the Levels that the member eligible
    var memberAllEligibleLevels = [];

    //new Levels which can be completed but not yet completed
    var levelsThatAreNotClaimed = [];

    
    var index = memberCurrentLevel;
    while (index > 0) {
      memberAllEligibleLevels.push(index);
      index = index - 1;
    }

    //if there is no completed levels, we need to insert levels that can be completed based on energy
    if (!completedLevels || completedLevels?.length <= 0) {
      levelsThatAreNotClaimed = memberAllEligibleLevels;
    // the member has completed all the elgible levels
    } else if (areArraysEqual([memberAllEligibleLevels], completedLevels)) {
      return;
      //there are some levels that the member needs to accomplish
    } else {
      levelsThatAreNotClaimed = getDifferenceBetweenTwoArrays(
        memberAllEligibleLevels,
        completedLevels
      );
    }

    //Certain Levels are Completed but not all

    const { rowCount: newlyCompletedLevelRowCount } = await pgConnection.query(
      "insert into tlenergymemberlevel(MEMBER_ID,energy_level) " +
        "select $1, unnest($2::int[]) returning *",
      [memberid, levelsThatAreNotClaimed]
    );


  } catch (error) {
    console.log(error);
    return;
  }
};

module.exports = updateMemberLevel;
