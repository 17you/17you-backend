
const areArraysEqual =  (_arr1,_arr2) => {
    if (
        !Array.isArray(_arr1)
        || !Array.isArray(_arr2)
        || _arr1.length !== _arr2.length
        ) {
          return false;
        }
      
      // .concat() to not mutate arguments
      const arr1 = _arr1.concat().sort();
      const arr2 = _arr2.concat().sort();
      
      for (let i = 0; i < arr1.length; i++) {
          if (arr1[i] !== arr2[i]) {
              return false;
           }
      } 
      return true;
};

const getDifferenceBetweenTwoArrays = (arr1,arr2) =>{
  return arr1.filter(x => !arr2.includes(x)); 
}

const removeDuplicatesFromArray = (myArr, prop) => {
  return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos
  })
}

const countNumberofSimilarObjectsinArray = (myArr, prop) => {
  var counts = myArr.reduce((p, c) => {
    var name = c.level;
    if (!p.hasOwnProperty(name)) {
      p[name] = 0;
    }
    p[name]++;
    return p;
  }, {});
  
  console.log(counts);
  
  var countsExtended = Object.keys(counts).map(k => {
    return {level: k, count: counts[k]}; });
    return countsExtended;
}



module.exports = {
    areArraysEqual,
    getDifferenceBetweenTwoArrays,
    removeDuplicatesFromArray,
    countNumberofSimilarObjectsinArray
};
