	-- to check the number of active connections
SELECT count(*) FROM pg_stat_activity WHERE STATE='active';

-- Creating Database

CREATE DATABASE 17You

-- Creating Tables

CREATE TABLE TLIndustryCategory(
	industry_category_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	industry_category_name TEXT NOT NULL,
	industry_category_image TEXT NOT NULL,
	PRIMARY KEY(industry_category_id)
	);
  
CREATE TABLE TLState(
		state_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
		state_name TEXT NOT NULL,
		PRIMARY KEY(state_id)
	);
   
CREATE TABLE TLMember (
	member_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	member_username TEXT NOT NULL,
	member_email TEXT NOT NULL UNIQUE,
	member_phone TEXT NOT NULL UNIQUE,
	member_password TEXT NOT NULL,
	member_is_merchant BOOLEAN NOT NULL DEFAULT FALSE,
	member_account_status BOOLEAN NOT NULL DEFAULT TRUE,
	member_expertise TEXT,
	member_image TEXT,
	state_id INTEGER NOT NULL,
	member_privacy_protection BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY(member_id),
	CONSTRAINT fk_state_id
      FOREIGN KEY(state_id) 
	  REFERENCES TLState(state_id)
	);
	
CREATE TABLE TLMemberAddress (
	address_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	address_line_1 TEXT NOT NULL, 
	address_line_2 TEXT NOT NULL, 
	city TEXT NOT NULL,
	postcode NUMERIC(5) NOT NULL,
	state TEXT NOT NULL,
	isDefault BOOLEAN NOT NULL DEFAULT FALSE,
	isDelete BOOLEAN NOT NULL DEFAULT FALSE,
	member_id INTEGER NOT NULL,
	PRIMARY KEY(address_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember(member_id)
	);
	
CREATE TABLE TLMerchantApplication (
	merchant_application_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	merchant_is_ic_submitted BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_ic_front_image TEXT,
	merchant_ic_back_image TEXT,
	merchant_is_selfie_submitted BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_selfie_image TEXT,
	merchant_is_business_details_submitted BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_company_name TEXT,
	merchant_company_reg_number TEXT,
	merchant_company_type TEXT,
	merchant_bank_name TEXT,
	merchant_bank_number BIGINT,
	merchant_bank_holder_name TEXT,
	merchant_email TEXT ,
	merchant_street_address TEXT ,
	merchant_city TEXT,
	merchant_state TEXT,
	merchant_postcode NUMERIC(5) NULL,
	merchant_live_sub_receipt_image TEXT,
	merchant_is_live_sub_receipt_submitted BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_is_policy_submitted BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_application_status TEXT NOT NULL DEFAULT 'INCOMPLETE',
	merchant_application_status_remarks TEXT,
	member_id INTEGER NOT NULL,
	industry_category_id INTEGER NOT NULL,
	PRIMARY KEY(merchant_application_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember(member_id),
		CONSTRAINT fk_industry_category_id
      FOREIGN KEY(industry_category_id) 
	  REFERENCES TLIndustryCategory (industry_category_id)
	);
	
CREATE TABLE TLMerchant (
	merchant_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	merchant_ic_front_image TEXT NOT NULL,
	merchant_ic_back_image TEXT NOT NULL,
	merchant_selfie_image TEXT NOT NULL,
	merchant_receipt_image TEXT NOT NULL,
	merchant_company_name TEXT NOT NULL,
	merchant_company_reg_number TEXT NULL,
	merchant_bank_name TEXT NOT NULL,
	merchant_company_type TEXT NOT NULL,
	merchant_bank_number BIGINT NOT NULL,
	merchant_bank_holder_name TEXT NOT NULL,
	merchant_email TEXT NOT NULL,
	merchant_signed_up_date_and_time TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	merchant_street_address TEXT NOT NULL,
	merchant_city TEXT,
	merchant_image TEXT,
	merchant_state TEXT NOT NULL,
	merchant_postcode NUMERIC(5),
	merchant_account_status BOOLEAN NOT NULL DEFAULT TRUE,
	member_id INTEGER NOT NULL,
	industry_category_id INTEGER NOT NULL,
	remaining_live_time BIGINT NOT NULL DEFAULT 86400000,
	PRIMARY KEY(merchant_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember(member_id),
	  CONSTRAINT fk_industry_category_id
      FOREIGN KEY(industry_category_id) 
	  REFERENCES TLIndustryCategory (industry_category_id)
	);


CREATE TABLE TLMemberIndustry(
	member_id INTEGER NOT NULL,
	industry_category_id INTEGER NOT NULL,
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember(member_id),
		CONSTRAINT fk_industry_category_id
      FOREIGN KEY(industry_category_id) 
	  REFERENCES TLIndustryCategory (industry_category_id),
	PRIMARY KEY(industry_category_id,member_id)
	);

CREATE TABLE TLSupport(
	support_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	contact TEXT,
	email TEXT,
	PRIMARY KEY(support_id)
	);

CREATE TABLE TLSystemProfile(
	system_profile_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	is_maintenance BOOLEAN NOT NULL,
	is_new_version BOOLEAN NOT NULL,
	new_version TEXT,
	PRIMARY KEY(system_profile_id)
	);
	
CREATE TABLE TLProductMainCategory(
	product_main_cat_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	product_main_cat_name TEXT NOT NULL,
	PRIMARY KEY(product_main_cat_id)
	);

CREATE TABLE TLProductSubCategory(
	product_sub_cat_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	product_sub_cat_name TEXT NOT NULL,
	product_main_cat_id INTEGER NOT NULL,
	PRIMARY KEY(product_sub_cat_id),
	CONSTRAINT fk_product_main_cat_id
      FOREIGN KEY(product_main_cat_id) 
	  REFERENCES TLProductMainCategory(product_main_cat_id)
	);

CREATE TABLE TLProduct(
	product_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	product_name TEXT NOT NULL,
	product_description TEXT NOT NULL,
	product_image TEXT NOT NULL,
	product_thumbnail TEXT,
	product_main_cat_id INTEGER NOT NULL,
	product_status BOOLEAN NOT NULL DEFAULT TRUE,
	product_sub_cat_id INTEGER NOT NULL,
	isDelete BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_id INTEGER NOT NULL,
	PRIMARY KEY(product_id),
	CONSTRAINT fk_product_main_cat_id
      FOREIGN KEY(product_main_cat_id) 
	  REFERENCES TLProductMainCategory(product_main_cat_id),
	CONSTRAINT fk_product_sub_cat_id
      FOREIGN KEY(product_sub_cat_id) 
	  REFERENCES TLProductSubCategory(product_sub_cat_id),
	  CONSTRAINT fk_merchant_id
      FOREIGN KEY(merchant_id) 
	  REFERENCES TLMerchant(merchant_id)
	);



CREATE TABLE TLProductVariation(
	product_variation_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	product_variation_size TEXT NOT NULL,
	product_variation_color TEXT NOT NULL,
	product_variation_price DECIMAL (12,2) NOT NULL,
	product_variation_quantity INTEGER NOT NULL,
	product_variation_status BOOLEAN NOT NULL DEFAULT TRUE,
	isDelete BOOLEAN NOT NULL DEFAULT FALSE,
	product_id INTEGER NOT NULL,
	PRIMARY KEY(product_variation_id),
	CONSTRAINT fk_product_id
      FOREIGN KEY(product_id) 
	  REFERENCES TLProduct(product_id)
	);

CREATE TABLE TLLiveStream(
	live_stream_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	live_stream_title TEXT NOT NULL,
	live_stream_description TEXT NOT NULL,
	live_stream_image TEXT NOT NULL,
	live_stream_date DATE NOT NULL,
	live_stream_start_time TIME NOT NULL,
	live_stream_end_time TIME NOT NULL,
	shipping_fee DECIMAL (12,2) NOT NULL DEFAULT 0.00,
	bambuser_id TEXT,
	live_stream_count INTEGER NOT NULL DEFAULT 0,
	live_stream_status TEXT NOT NULL,
	isDelete BOOLEAN NOT NULL DEFAULT FALSE,
	merchant_id INTEGER NOT NULL,
	PRIMARY KEY(live_stream_id),
	CONSTRAINT fk_merchant_id
      FOREIGN KEY(merchant_id) 
	  REFERENCES TLMerchant(merchant_id)
	);

CREATE TABLE TLLiveStreamProducts(
	live_stream_id INTEGER NOT NULL,
	product_id INTEGER NOT NULL,
	status BOOLEAN NOT NULL DEFAULT FALSE,
	sequence INTEGER NOT NULL,
	CONSTRAINT fk_live_stream_id
      FOREIGN KEY(live_stream_id) 
	  REFERENCES TLLiveStream(live_stream_id),
	CONSTRAINT fk_product_id
      FOREIGN KEY(product_id) 
	  REFERENCES TLProduct (product_id)
	  ON DELETE CASCADE,
	PRIMARY KEY(live_stream_id,product_id)
	);



CREATE TABLE TLReferralManagement(
	referral_management_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	member_id INT NOT NULL,
	member_referrer_code TEXT,
	member_referral_code TEXT UNIQUE NOT NULL DEFAULT ('17' || generate_referral_code(6)),
	member_signed_up_date_and_time TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	total_energy INT NOT NULL DEFAULT 0,
	total_pending INT NOT NULL DEFAULT 0,
	total_redeemed_at INT NOT NULL DEFAULT 0,
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TlMember (member_id),
	PRIMARY KEY(referral_management_id)
	);

CREATE TABLE TLEnergy(
	energy_id char(5) NOT NULL,
	energy_name TEXT NOT NULL,
	energy_description TEXT NOT NULL,
	energy_limit_per_day INTEGER NOT NULL,
	is_check_limit BOOLEAN NOT NULL,
	default_energy_value INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY(energy_id)
	);

CREATE TABLE TLMemberEnergy(
	member_energy_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	member_id INTEGER NOT NULL,
	energy_id char(5) NOT NULL,
	energy_redeemed_total INTEGER NOT NULL DEFAULT 0,
	member_rank INTEGER,
	energy_redeemed_date DATE NOT NULL DEFAULT CURRENT_DATE,
	PRIMARY KEY(member_energy_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TlMember (member_id),
	CONSTRAINT fk_energy_id
      FOREIGN KEY(energy_id) 
	  REFERENCES TLEnergy (energy_id)
	);
CREATE TABLE TLMerchantEnergy(
	merchant_energy_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	merchant_id INTEGER NOT NULL,
	energy_id char(5) NOT NULL,
	energy_redeemed_total INTEGER NOT NULL DEFAULT 0,
	energy_redeemed_date DATE NOT NULL DEFAULT CURRENT_DATE,
	PRIMARY KEY(merchant_energy_id),
	CONSTRAINT fk_merchant_id
      FOREIGN KEY(merchant_id) 
	  REFERENCES TLMerchant (merchant_id),
	CONSTRAINT fk_energy_id
      FOREIGN KEY(energy_id) 
	  REFERENCES TLEnergy (energy_id)
	);

CREATE TABLE TLMemberFavourite(
	member_id INTEGER NOT NULL,
	merchant_id INTEGER NOT NULL,
	CONSTRAINT fk_merchant_id
      FOREIGN KEY(merchant_id) 
	  REFERENCES TLMerchant(merchant_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember (member_id),
	PRIMARY KEY(merchant_id,member_id));
	
CREATE TABLE TLEnergyLevel(
	energy_level INT NOT NULL UNIQUE,
	energy_level_limit INT NOT NULL,
	PRIMARY KEY(energy_level)
	);

CREATE TABLE TLMemberShared(
	member_shared_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	member_id INT NOT NULL,
	member_shared_date DATE NOT NULL DEFAULT CURRENT_DATE,
	PRIMARY KEY(member_shared_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TLMember(member_id)
	);
CREATE TABLE TLEnergyMemberLevel(
	energy_member_level_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	member_id INTEGER NOT NULL,
	energy_level INTEGER NOT NULL,
	date_completed DATE NOT NULL DEFAULT current_date,
	PRIMARY KEY(energy_member_level_id),
	CONSTRAINT fk_member_id
      FOREIGN KEY(member_id) 
	  REFERENCES TlMember (member_id),
	CONSTRAINT fk_energy_level
      FOREIGN KEY(energy_level) 
	  REFERENCES TLEnergyLevel (energy_level)
	);

CREATE TABLE TLTransaction (
	transaction_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	live_stream_id INTEGER NOT NULL,
	merchant_id INTEGER NOT NULL,
	address_line_1 TEXT NOT NULL DEFAULT '', 
	address_line_2 TEXT NOT NULL DEFAULT '', 
	city TEXT NOT NULL DEFAULT '',
	postcode NUMERIC(5) NOT NULL DEFAULT 0,
	state TEXT NOT NULL DEFAULT '',
	transaction_date_and_time TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	transaction_status TEXT NOT NULL DEFAULT 'PAYMENT PENDING',
	member_id INTEGER NOT NULL,
	payment_method TEXT,
	total_product_price DECIMAL (12,2) NOT NULL,
	delivery_fees DECIMAL (12,2) NOT NULL,
	final_price DECIMAL (12,2) NOT NULL,
	transaction_receipt TEXT,
	admin_remarks TEXT NULL,
	PRIMARY KEY(transaction_id),
	CONSTRAINT fk_live_stream_id
    FOREIGN KEY(live_stream_id) 
	REFERENCES TLLiveStream(live_stream_id),
	CONSTRAINT fk_merchant_id
    FOREIGN KEY(merchant_id) 
	REFERENCES TLMerchant(merchant_id),
	CONSTRAINT fk_address_id
    FOREIGN KEY(address_id) 
	REFERENCES TLMemberAddress(address_id),
	CONSTRAINT fk_member_id
    FOREIGN KEY(member_id) 
	REFERENCES TLMember(member_id)
	);
		
CREATE TABLE TLLogistic (
	logistic_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	transaction_id INTEGER NOT NULL,
	logistic_status TEXT NOT NULL DEFAULT 'PAYMENT PENDING',
	courier_company TEXT,
	tracking_url TEXT,
	PRIMARY KEY(logistic_id),
	CONSTRAINT fk_transaction_id
    FOREIGN KEY(transaction_id) 
	REFERENCES TLTransaction(transaction_id)
	);

CREATE TABLE TLPaymentPendingProducts(
	transaction_id INTEGER NOT NULL,
	product_id INTEGER NOT NULL,
	product_variation_id INTEGER NOT NULL,
	quantity_purchased INTEGER NOT NULL,
	unit_price DECIMAL (12,2) NOT NULL,
	total_price DECIMAL (12,2) NOT NULL,
	PRIMARY KEY(transaction_id,product_id,product_variation_id),
	CONSTRAINT fk_transaction_id
    FOREIGN KEY(transaction_id) 
	REFERENCES TLTransaction(transaction_id),
	CONSTRAINT fk_product_id
    FOREIGN KEY(product_id) 
	REFERENCES TLProduct(product_id)
	ON DELETE CASCADE,
	CONSTRAINT fk_product_variation_id
    FOREIGN KEY(product_variation_id) 
	REFERENCES TLProductVariation(product_variation_id)
	);

CREATE TABLE TLRankingInfo(
		ranking_id INT NOT NULL UNIQUE,
		ranking_image TEXT NOT NULL,
		reward_points INT NOT NULL,
		PRIMARY KEY(ranking_id)
	);
	
CREATE TABLE TLPurchasedProducts(
	purchased_product_id INTEGER GENERATED ALWAYS AS IDENTITY NOT NULL,
	transaction_id INTEGER NOT NULL,
	quantity_purchased INTEGER NOT NULL,
	unit_price DECIMAL (12,2) NOT NULL,
	total_price DECIMAL (12,2) NOT NULL,
	product_variation_size TEXT NOT NULL,
	product_variation_color TEXT NOT NULL,
	product_name TEXT NOT NULL,
	product_description TEXT NOT NULL,
	product_image TEXT NOT NULL,
	product_main_cat_name TEXT NOT NULL,
	product_sub_cat_name TEXT NOT NULL,
	PRIMARY KEY(purchased_product_id),
	CONSTRAINT fk_transaction_id
    FOREIGN KEY(transaction_id) 
	REFERENCES TLTransaction(transaction_id)
	);
	
	
-- Adding records into tlmemberindustry


 INSERT INTO tlindustrycategory (industry_category_id,industry_category_name, industry_category_image)
OVERRIDING SYSTEM VALUE
	VALUES 
	(1,'Accomodation & Hotel', 'https://obor.17app.io/default/industry/accomodation.png'),
   	(2,'Amusement','https://obor.17app.io/default/industry/amusement.png'),
    (3,'Charity', 'https://obor.17app.io/default/industry/charity.png'),
	(4,'Computer / Accessories', 'https://obor.17app.io/default/industry/computer.png'),
	(5,'Education', 'https://obor.17app.io/default/industry/education.png'),
   	(6,'Fashion', 'https://obor.17app.io/default/industry/fashion.png'),
    (7,'Food', 'https://obor.17app.io/default/industry/food.png'),
	(8,'Grocery', 'https://obor.17app.io/default/industry/grocery.png'),
   	(9,'Health & Beauty', 'https://obor.17app.io/default/industry/healthbeauty.png'),
    (10,'Home & Living', 'https://obor.17app.io/default/industry/home.png'),
	(11,'Medical', 'https://obor.17app.io/default/industry/medical.png'),
   	(12,'Toy & Kids', 'https://obor.17app.io/default/industry/accomodation.png');
   


-- Adding records into tlproductmaincategory
INSERT INTO tlproductmaincategory (product_main_cat_id, product_main_cat_name)
OVERRIDING SYSTEM VALUE
	VALUES 
	(1,'Services'),
   	(2,'Products');


-- Adding records into tlproductsubcategory
	INSERT INTO tlproductsubcategory (product_sub_cat_id,product_sub_cat_name,product_main_cat_id)
	OVERRIDING SYSTEM VALUE
	VALUES 
	(1,'Computer',1),
   	(2,'Groceries',1),
	(3,'Charity',1),
   	(4,'Others',1),
	(5,'Fashion',2),
   	(6,'Food',2),
	(7,'Education',2),
   	(8,'Medical',2),
	(9,'Donation',2),
	(10,'Others',2)

	   

--Adding sample records for merchant
INSERT INTO tlmerchant (merchant_ic_front_image,merchant_ic_back_image,merchant_selfie_image,merchant_company_name,merchant_bank_name,merchant_bank_number,merchant_email,merchant_street_address,member_id,industry_category_id) 
VALUES ('merchant_ic_front_image','merchant_ic_back_image','merchant_selfie_image','merchant_company_name','merchant_bank_name',1234,'merchant_email','merchant_street_address',113,36) ;


--Insert States
INSERT INTO TLState (state_id,state_name)
OVERRIDING SYSTEM VALUE
VALUES
(1,'SELANGOR'),
(2,'PAHANG'),
(3,'SABAH'),
(4,'SARAWAK'),
(5,'PERLIS'),
(6,'PENANG'),
(7,'MALACCA'),
(8,'JOHOR'),
(9,'TERENGGANU'),
(10,'PERAK'),
(11,'KELANTAN'),
(12,'KUALA LUMPUR'),
(13,'PUTRAJAYA'),
(14,'LABUAN'),
(15,'KEDAH'),
(16'NEGERI SEMBILAN');

--Insert Energy Level and Max
INSERT INTO TLEnergyLevel (energy_level,energy_level_limit)
VALUES
(1,1000),
(2,2000),
(3,3000),
(4,4000),
(5,5000),
(6,6000),
(7,7000),
(8,8000),
(9,9000),
(10,10000);

--Insert Energy Types and Limits
	insert into TLEnergy(energy_id,energy_name,energy_description,energy_limit_per_day, is_check_limit,default_energy_value)
	VALUES
	('TS','Total Share','This is Total Share',1500,true,0),
	('TSU','Sign Up Energy','Sign Up Energy',1500,false,150),
	('DEB','Daily Energy Booster','This is Daily Energy Booster',200,false,0),
	('NEB','New Energy Booster','This is New Sign Up Energy',100,false,100,100),
	('REB','Reward Energy Booster', 'This is energy received from rewards',1000,false,0),
	('MLS','Merchant Live Stream','This is Merchant Live Stream',1500,false,100);


--Insert Data for Ranking

INSERT INTO TLRankingInfo (ranking_id,ranking_image,reward_points)
VALUES 
(1,'https://obor.17app.io/default/ranking/image1.jpeg',1000),
 (2,'https://obor.17app.io/default/ranking/image2.jpeg',900),
 (3,'https://obor.17app.io/default/ranking/image3.jpeg',800),
 (4,'https://obor.17app.io/default/ranking/image4.jpeg',700),
 (5,'https://obor.17app.io/default/ranking/image5.jpeg',600),
 (6,'https://obor.17app.io/default/ranking/image6.jpeg',500),
 (7,'https://obor.17app.io/default/ranking/image7.jpeg',400),
 (8,'https://obor.17app.io/default/ranking/image8.jpeg',300),
 (9,'https://obor.17app.io/default/ranking/image9.jpeg',200),
 (10,'https://obor.17app.io/default/ranking/image10.jpeg',100);


-- Change TimeZone
ALTER DATABASE onesevenyoudb SET timezone TO 'ASIA/Kuala_Lumpur';



-- Delete All Records From All Tables
	BEGIN;
	delete from tlpurchasedproducts;
	delete from tlmemberenergy;
	delete from tltransaction;
	delete from tllogistic;
	delete from tlmerchantenergy;
	delete from tlreferralmanagement;
	delete from tlmemberfavourite;
	delete from tlmembershared;
	delete from tlenergymemberlevel;
	delete from tllivestreamproducts;
	delete from tllivestream;
	delete from tlproductvariation;
	delete from tlproduct;
	delete from tlmerchant;
	delete from tlmemberaddress;
	delete from tlmerchantapplication;
	delete from tlmemberindustry;
	delete from tlmember;
	COMMIT;


--Delete Records of a Specific User

delete from tlmember where member_phone = '0107621735';
delete from tlmerchant where member_id = 143;
delete from tllivestream where merchant_id = 49;
delete from tllivestreamproducts where live_stream_id = 74;
delete from tltransaction where live_stream_id = 75;
delete from tlpurchasedproducts where transaction_id = 35;
delete from tllogistic where transaction_id = 35;
delete from tlproduct where merchant_id = 49;
delete from tlproductvariation where product_id = 58;
delete from tlmemberindustry where member_id = 143;
delete from tlmerchantapplication where member_id = 143;
delete from tlmembershared where member_id = 143;
delete from tlmemberenergy where member_id = 143;
