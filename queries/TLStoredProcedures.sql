
/* To generate a random alphanumeric characters for referral code */

CREATE OR REPLACE FUNCTION generate_referral_code(size INT) RETURNS TEXT AS $$
DECLARE
  characters TEXT := 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  bytes BYTEA := gen_random_bytes(size);
  l INT := length(characters);
  i INT := 0;
  output TEXT := '';
BEGIN
  WHILE i < size LOOP
    output := output || substr(characters, get_byte(bytes, i) % l + 1, 1);
    i := i + 1;
  END LOOP;
  RETURN output;
END;
$$ LANGUAGE plpgsql VOLATILE;


/*  Mask Email Address */
CREATE FUNCTION mask_email(text) RETURNS text
   LANGUAGE SQL IMMUTABLE AS
$$SELECT overlay($1 placing repeat('*', position('@' in $1) - 3) from 2 )$$;