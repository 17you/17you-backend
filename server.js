const express = require("express");
const logger = require("morgan");
const cors = require("cors");
var multer = require('multer');
var upload = multer();
const formidableMiddleware = require('express-formidable');
const serverTime = require("./app/api/helper/serverTime.js");
const pgConnection = require("./config/database");
const memberRouter = require("./routes/TLMemberRoutes");
const industryRouter = require("./routes/TLIndustryRoutes");
const productRouter = require("./routes/TLProductRoutes");
const merchantRouter = require("./routes/TLMerchantRoutes");
const liveStreamRouter = require("./routes/TLLiveStreamRoutes");
const paymentRouter = require("./routes/TLPaymentRoutes");
const referralRouter = require("./routes/TLReferralRoutes");
const statesRouter = require("./routes/TLStatesRoutes");
const energyRouter = require("./routes/TLEnergyRoutes");
const transactionsRouter = require("./routes/TLTransactionRoutes");
const rankingRouter = require("./routes/TLRankingRoutes");



const verifytoken = require("./config/jwtauth");
const socketio = require('socket.io')

//create express application
const app = express();
//Server Configurations
app.use(logger("dev"));
app.use(cors())
app.use(express.urlencoded({
  extended: true
}));
app.use(express.json());

// for serving static contents
app.use(express.static('/home/demgradmin/upload_images/'))
app.use(express.static('/Users/upload_images/'))

//Database
pgConnection.on("error", (err, client) => {
  console.error("Error:", err);
});

//Entry Point
app.get("/", function (req, res) {
  res.status(201).json({
    status: true,
    message: "Welcome to 17You",
  });
});

app.get("/log/", function (req, res) {
  console.log(req.query.platform + ": -->>>> " + req.query.crashLog);
  res.sendStatus(201);
});

app.get("/favicon.ico", function (req, res) {
  res.sendStatus(204);
});

//public and private routes
app.use("/member", memberRouter);


//public routes
app.use("/industry", industryRouter);


//private routes
app.use("/merchant", merchantRouter);
app.use("/product", productRouter);
app.use("/live", liveStreamRouter);
app.use("/referral", referralRouter);
app.use("/energy", energyRouter);
app.use("/states", statesRouter);
app.use("/order", paymentRouter);
app.use("/transaction", transactionsRouter);
app.use("/ranking", rankingRouter);
app.use("/reward", rankingRouter);






// Handle Errors
app.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  console.log(err);

  if (err.status === 404)
    res.status(404).json({
      status: false,
      message: "Not found",
    });
  else
    res.status(500).json({
      status: false,
      message: "Something looks wrong :( !!!",
    });
});

// handle "error" event so nodejs will not crash
app.use("error", function (err) {
  console.log(err);
});

//Server Port Configurations
app.set("port", process.env.PORT || 5000);

app.listen(app.get("port"), function () {
  console.log("Node server listening on port " + app.get("port"));
  console.log(serverTime.getCurrentTime());
});



// //Live Chat Server
// var liveChatServer = app.listen(3000,()=>{
//   console.log('Live Chat Server is running on port number 3000')
// })

// var io = socketio.listen(liveChatServer)
// io.on('connection', function (socket) {
//   console.log(`Connection : SocketId = ${socket.id}`)
//   var userName = '';

//   socket.on('subscribe', function (data) {
//     console.log('subscribe triggered')
//     const roomData = JSON.parse(data)
//     userName = roomData.userName;
//     const roomName = roomData.roomName;

//     socket.join(`${roomName}`)
//     console.log(`Username : ${userName} joined Room Name : ${roomName}`)

//     socket.broadcast.to(`${roomName}`).emit('newUserToLiveRoom',userName);
//     //io.to(`${roomName}`).emit('newUserToLiveRoom', userName);

//   })

//   socket.on('unsubscribe', function (data) {
//     console.log('unsubscribe trigged')
//     const roomData = JSON.parse(data)
//     const userName = roomData.userName;
//     const roomName = roomData.roomName;

//     console.log(`Username : ${userName} leaved Room Name : ${roomName}`);
//     socket.broadcast.to(`${roomName}`).emit('userLeftLiveRoom', userName);
//     socket.leave(`${roomName}`)
//   })

//   socket.on('newMessage', function (data) {

//     const messageData = JSON.parse(data)
//     const userName = messageData.userName
//     const messageContent = messageData.messageContent
//     const roomName = messageData.roomName

//     console.log(`[Room Number ${roomName}] ${userName} : ${messageContent}`)
//     const chatData = {
//       userName: userName,
//       messageContent: messageContent,
//       roomName: roomName
//     }
//     socket.broadcast.to(`${roomName}`).emit('updateLiveChat', JSON.stringify(chatData))
//   })

//   // socket.on('typing',function(roomNumber){
//   //     console.log('typing triggered')
//   //     socket.broadcast.to(`${roomNumber}`).emit('typing')
//   // })

//   // socket.on('stopTyping',function(roomNumber){ 
//   //     console.log('stopTyping triggered')
//   //     socket.broadcast.to(`${roomNumber}`).emit('stopTyping')
//   // })

//   socket.on('disconnect', function () {
//     console.log("One of sockets disconnected from our server.")
//   });
// })