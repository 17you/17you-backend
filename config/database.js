const Pool = require('pg').Pool
const {user,host,database,password,port} = require('../constants')

const pool = new Pool({
  user: user,
  host: host,
  database: database,
  password: password,
  port: port,
  max: 15,
  connectionTimeoutMillis: 30000,
  idleTimeoutMillis: 2000
})
module.exports = pool;