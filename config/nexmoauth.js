const Nexmo = require('nexmo');
const {nexmoApiKey,nexmoSecretKey} = require('../constants')

const nexmo = new Nexmo({
    apiKey: nexmoApiKey,
    apiSecret: nexmoSecretKey,
  });

  module.exports =nexmo;