const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const socketio = require('socket.io')
//create express application
const app = express();

//Server Configurations
app.use(logger("dev"));
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

//Live Chat Server
var liveChatServer = app.listen(3000,()=>{
  console.log('Live Chat Server is running on port number 3000')
})

var io = socketio.listen(liveChatServer)
io.on('connection', function (socket) {
  console.log(`Connection : SocketId = ${socket.id}`)
  var userName = '';

  socket.on('subscribe', function (data) {
    console.log('subscribe triggered')
    const roomData = JSON.parse(data)
    userName = roomData.userName;
    const roomName = roomData.roomName;

    console.log(`Username : ${userName} joined Room Name : ${roomName}`)
    socket.join(`${roomName}`)
    socket.to(`${roomName}`).emit('newUserToLiveRoom',userName);

  })

  socket.on('unsubscribe', function (data) {
    console.log('unsubscribe trigged')
    const roomData = JSON.parse(data)
    const userName = roomData.userName;
    const roomName = roomData.roomName;

    console.log(`Username : ${userName} leaved Room Name : ${roomName}`);
    socket.broadcast.to(`${roomName}`).emit('userLeftLiveRoom', userName);
    socket.leave(`${roomName}`)
  })

  socket.on('newMessage', function (data) {

    const messageData = JSON.parse(data)
    const userName = messageData.userName
    const messageContent = messageData.messageContent
    const roomName = messageData.roomName

    console.log(`[Room Number ${roomName}] ${userName} : ${messageContent}`)
    const chatData = {
      userName: userName,
      messageContent: messageContent,
      roomName: roomName
    }
    io.to(`${roomName}`).emit('updateLiveChat', JSON.stringify(chatData))
  })

  socket.on('like',function(data){
    const roomData = JSON.parse(data)
    const roomName = roomData.roomName;
    console.log('like triggered')
    io.to(`${roomName}`).emit('like')
  })

  socket.on('update',function(data){
    const roomData = JSON.parse(data)
    const roomName = roomData.roomName;
    console.log('update triggered')
    socket.broadcast.to(`${roomName}`).emit('update')
  })


  // socket.on('typing',function(roomNumber){
  //     console.log('typing triggered')
  //     socket.broadcast.to(`${roomNumber}`).emit('typing')
  // })

  // socket.on('stopTyping',function(roomNumber){ 
  //     console.log('stopTyping triggered')
  //     socket.broadcast.to(`${roomNumber}`).emit('stopTyping')
  // })

  socket.on('disconnect', function () {
    console.log("One of sockets disconnected from our server.")
  });
})