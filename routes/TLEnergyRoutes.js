const express = require("express");
const energyController = require("../app/api/controllers/TLEnergyController.js");
const router = express.Router();

//Member Side
router.post("/redeem/daily/:memberid", energyController.redeemDailyEnergyBooster);
router.post("/redeem/todaysignup/:memberid", energyController.redeemTodaySignUp);
router.post("/redeem/overallsignup/:memberid", energyController.redeemOverallSignUp);
router.post("/redeem/totalshare/:memberid", energyController.redeemTotalShare);
router.get("/summary/:memberid", energyController.getMemberEnergyInformation);
router.get("/level/summary/:memberid",energyController.showSummarisedLevel);
router.get("/level/detailed/:memberid",energyController.showDetailedLevel);


//Merchant Side
router.get("/merchant/summary/:merchantid",energyController.getMerchantEnergyInformation);
router.post("/merchant/redeem/:merchantid",energyController.redeemMerchantEnergy);


router.use(express.urlencoded({extended: true}));
router.use(express.json());
 
module.exports = router;