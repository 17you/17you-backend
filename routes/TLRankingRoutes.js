const express = require("express");
const rankingController = require("../app/api/controllers/TLRankingController.js");
const router = express.Router();


//Get Ranking Detals
router.get("/get", rankingController.getRankingDetails);

//Get Reward Details
router.get("/check/:memberid", rankingController.checkRewards);
router.get("/get/:memberid", rankingController.getRewardDetails);
router.post("/claim/:memberid", rankingController.claimRewards);



router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;