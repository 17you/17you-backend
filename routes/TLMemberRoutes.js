const express = require("express");
const memberController = require("../app/api/controllers/TLMemberController");
const router = express.Router();

//Member Login
router.post("/login", memberController.loginwithEmail);
router.post("/register", memberController.register);
router.post("/login/mobile", memberController.loginWithPhone)
router.post("/login/mobile/check", memberController.loginWithMobileCheck);
router.post("/register/check", memberController.mobileAndEmailCheckBeforeRegister);
router.post("/update/password", memberController.updatePassword);

//Member Profile
router.post("/edit/:memberid", memberController.updateProfile);

//Member Favourites
router.post("/fav/:memberid",memberController.addOrRemoveFavourite);

//Member Address 
router.post("/address/:memberid",memberController.addressActions);


router.use(express.urlencoded({extended: true}));
router.use(express.json());
module.exports = router;