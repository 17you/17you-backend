const express = require("express");
const merchantController = require("../app/api/controllers/TLMerchantController.js");
const router = express.Router();
const path = require("path");

//Merchant Application Details 
router.post("/register/ic/:memberid",  merchantController.registerNewMerchantIC);
router.post("/register/selfie/:memberid", merchantController.registerMerchantSelfie);
router.post("/register/bank/:memberid", merchantController.registerMerchantBankDetails);
router.post("/register/subcription/:memberid", merchantController.registerMerchantLiveStreamSubscription);
router.post("/register/policy/:memberid", merchantController.registerMerchantPrivacyPolicy);
router.get("/register/status/:memberid", merchantController.checkMerchantRegistrationStatus);
router.post("/register/bank/temp/:memberid", merchantController.tempRegisterMerchantBankDetails);


//Merchant Details (Merchant)
router.get("/:memberid", merchantController.checkAndRetrieveMerchantDetails);
router.get("/profile/view/:merchantid", merchantController.showMerchantProfile);
router.post("/profile/edit/:merchantid", merchantController.updateMerchantProfile);



//Merchant Application Details (Admin Side)
router.post("/application/update/:applicationid",merchantController.approveOrRejectApplication);
router.get("/application/update/manual",merchantController.approveApplicationEmail);



router.use(express.urlencoded({extended: true}));
router.use(express.json());
module.exports = router;