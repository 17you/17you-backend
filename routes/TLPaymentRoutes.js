const express = require("express");
const paymentController = require("../app/api/controllers/TLPaymentController");
const router = express.Router();

//Manual Payment
router.post("/payment", paymentController.createManualPayment);

router.use(express.urlencoded({extended: true}));
router.use(express.json());
module.exports = router;