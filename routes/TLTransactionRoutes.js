const express = require("express");
const transactionsController = require("../app/api/controllers/TLTransactionController.js");
const router = express.Router();

//User Side
router.post("/add/receipt", transactionsController.addNewTransactionWithReceipt);
router.post("/add", transactionsController.addNewTransactionWithoutReceipt);
router.post("/update/receipt/:transactionid", transactionsController.updateTransactionWithReceipt);
router.get("/member/get/:memberid", transactionsController.retrieveMemberTransactionDetails);


//Merchant Side
router.get("/merchant/get/:merchantid", transactionsController.retrieveMerchantTransactionDetails);
router.post("/merchant/update/:transactionid", transactionsController.updateTransactionStatus);
router.get("/merchant/history/:merchantid", transactionsController.getTransactionHistory);
router.post("/reject/:transactionid",transactionsController.rejectTransaction);
router.post("/approve/:transactionid",transactionsController.approveTransaction);


router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;