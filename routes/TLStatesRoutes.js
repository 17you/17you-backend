const express = require("express");
const statesController = require("../app/api/controllers/TLStatesController.js");
const router = express.Router();

//Get All States
router.get("/all", statesController.getStates);

router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;