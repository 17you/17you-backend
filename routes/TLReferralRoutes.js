const express = require("express");
const referralController = require("../app/api/controllers/TLReferralController.js");
const router = express.Router();

//Add Total Share
router.post("/add/share/:memberid", referralController.addMemberShared);

//Get Referral Counts of a Single User
router.get("/get/counts/:memberid", referralController.getReferralCounts);

//Get Referral Results Based on State
router.get("/get/results/total/:memberid", referralController.getTotalReferralCountBasedOnStates);
router.get("/get/results/today/:memberid", referralController.getTodayReferralCountBasedOnStates);
router.get("/get/results/week/:memberid", referralController.getCurrentWeekReferralCountBasedOnStates);
router.get("/get/results/month/:memberid", referralController.getCurrentMonthReferralCountBasedOnStates);

//Get Referral Email Addresses
router.get("/get/emails/total/:memberid", referralController.getTotalReferralEmailAddress);
router.get("/get/emails/today/:memberid", referralController.getTodayReferralEmailAddress);
router.get("/get/emails/week/:memberid", referralController.getCurrentWeekReferralEmailAddress);
router.get("/get/emails/month/:memberid", referralController.getCurrentMonthReferralEmailAddress);




router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;