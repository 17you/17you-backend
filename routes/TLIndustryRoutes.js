const express = require("express");
const industryController = require("../app/api/controllers/TLIndustryController.js");
const router = express.Router();

//Get Industries
router.get("/get", industryController.getIndustryDetails);
router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;