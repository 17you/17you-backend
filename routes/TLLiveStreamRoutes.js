const express = require("express");
const liveStreamController = require("../app/api/controllers/TLLiveStreamController");

const router = express.Router();

//LiveStream (Merchant Side)
router.post("/add/:merchantid", liveStreamController.addLiveStreamDetails);
router.get("/all/:merchantid", liveStreamController.getMerchantLiveStreamAll);
router.get("/one/:livestreamid", liveStreamController.getMerchantLiveStreamOne);
router.post("/products/:livestreamid",liveStreamController.getMerchantLiveStreamProductDetails);
router.post("/products/update/:livestreamid",liveStreamController.modifyMerchantLiveStreamProduct);
router.post("/update/status/:livestreamid",liveStreamController.updateLiveStreamStatus);
router.post("/start/:livestreamid",liveStreamController.startLiveStream);
router.post("/stop/:livestreamid",liveStreamController.stopLiveStream);
router.post("/delete/:livestreamid",liveStreamController.deleteLiveStream);
router.post("/update/:livestreamid/:merchantid",liveStreamController.updateLiveStream);
router.get("/upcoming/one/:merchantid",liveStreamController.getOneUpcomingLiveStreamDetails);
router.get("/top/:merchantid",liveStreamController.getTopLiveStreamBasedonSales);


//LiveStream (User Side)
router.get("/active", liveStreamController.getActiveLiveStreamCount);
router.get("/categories/all/:memberid", liveStreamController.getMerchantLiveStreamBasedOnCategories);
router.post("/detailed/:livestreamid", liveStreamController.getSingleLiveStreamDetailsWithProduct);

router.use(express.urlencoded({extended: true}));
router.use(express.json());
module.exports = router;