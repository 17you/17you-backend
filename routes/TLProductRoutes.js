const express = require("express");
const productController = require("../app/api/controllers/TLProductController");

const router = express.Router();

//Merchant Side
router.post("/add/:merchantid", productController.addProductDetails);
router.get("/categories",productController.getCategories);
router.get("/all/:merchantid", productController.viewProductDetailsPerMerchant);
router.post("/update/:productid/:merchantid", productController.modifyMerchantProduct);
router.get("/one/:productid", productController.getMerchantProductOne);
router.post("/delete/:productid",productController.deleteProductDetails);



//Member Side


//Admin
router.get("/all", productController.viewProductDetails);


router.use(express.urlencoded({extended: true}));
router.use(express.json());
module.exports = router;